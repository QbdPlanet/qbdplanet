if($args.length -eq 1) {
  findstr /m /S $args[0] .\*
}
elseif($args.length -eq 2) {
  findstr /m /S $args[0] $args[1]
}
else {
  echo "This script requires atleast 1 argument.";
}
