<?php

include php("common/utils.php");
include php("common/databaseconnection.php");
include php("folder/folderfunctions.php");

$query = "DELETE FROM Folder";
$result = mysqli_query($con,$query);

$query =  "SELECT q.QuestionId,dl.DifficultyLevel,st.Subject,st.Topic"
          . " FROM Question q JOIN SubjectTopic st ON q.SubjectTopicId=st.SubjectTopicId"
          . " JOIN DifficultyLevel dl ON q.DifficultyLevelId=dl.DifficultyLevelId"
          . " WHERE q.UserId=1";
//echo $query;
//exit();
$result = mysqli_query($con,$query);

while($row = mysqli_fetch_array($result) ) {
  $folderId = getFolderId($con,$row[1],$row[2],$row[3],1);
  $queryTwo = "UPDATE Question SET FolderId=" . $folderId . " WHERE QuestionId=" . $row[0];
  $resultTwo = mysqli_query($con,$queryTwo);
}

$query =  "SELECT q.QuestionId,f1.FolderName,f2.FolderName,f3.FolderName"
          . " FROM Question q JOIN Folder f3 ON q.FolderId=f3.FolderId"
          . " JOIN Folder f2 ON f3.ParentId=f2.FolderId"
          . " JOIN Folder f1 ON f2.ParentId=f1.FolderId WHERE q.UserId=1";
$result = mysqli_query($con,$query);

while($row = mysqli_fetch_array($result) ) {
  echo "QuestionId: " . $row[0] . ", Directory: " . $row[1] . " -> " . $row[2] . " -> " . $row[3] . "</br>";
}

mysqli_close($con);

exit();

?>
