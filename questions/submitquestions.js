var imageFileSelected = false;
var questionId;

function initSubmitQuestions() {
  getRequest("categories/getallcategories.php",populateCategory);
  questionId = getCookie("questionIdToCopy");
  if(!varEmptyOrNull(questionId)) {
    deleteCookie("questionIdToCopy");
    postRequest("questions/getquestionfromid.php","questionId="+questionId,initQuestionsField);
  }
  //initGetCategories(document.getElementById("subject"),document.getElementById("topic"),document.getElementById("level"),"Select");
  //initSubmitCategories(document.getElementById("valueSpace"),refreshSubjectList,refreshLevel);
  //populateAllLists();
}

function handleSubmitQuestions(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
}

function initQuestionsField(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  $("#question").html(response.question);
  populateQuestionCard("#questionStatementFinalCut","#question","Question");

  $("#inputSubject").val(response.subject);
  $("#inputTopic").val(response.topic);
  $("#inputLevel").val(response.level);
  populateTopicDataList();

  //populateSubjectListWithSelectedValues(response.subject,response.topic);
  //populateLevelsWithSelectedValue(response.level);
}

function submitQuestions(){

  initValidForm();

  checkQuestionAndCategories();

  var uploadImageButton = document.getElementById("uploadImageButton");
  if(uploadImageButton.innerHTML == "Cancel Image Upload") {
    var imgPath = $("#chooseImageFile")[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if(!imageFileSelected) {
      appendValidFormAlert("Either a image file should be chosen or Image Upload should be cancelled");
      uploadImageButton.style.color = "red";
      validForm = false;
    }
  }

  if(!isFormValid()) {
    return;
  }

  var formData = getFormData();
  if(uploadImageButton.innerHTML == "Cancel Image Upload") {
    formData.append("imageFile",document.getElementById("chooseImageFile").files[0]);
    console.log(document.getElementById("chooseImageFile").files[0]);
    return;
  }

  /*var formData = new FormData();
  formData.append("question",$('#question').html());
  formData.append("topic",$('#topic').val());
  formData.append("level",$('#level').val());
  if(uploadImageButton.innerHTML == "Cancel Image Upload") {
    formData.append("imageFile",document.getElementById("chooseImageFile").files[0]);
  }
  formRequest("questions/submitquestions.php",formData,handleSubmitQuestions);*/
  formRequest("questions/submitquestionandcategory.php",formData,handleSubmitQuestions,100);
}

function uploadImage() {
  var uploadImageDiv = document.getElementById("uploadImageDiv");
  var uploadImageButton = document.getElementById("uploadImageButton");
  var inputFileChild = document.createElement("INPUT");
  inputFileChild.id = "chooseImageFile";
  inputFileChild.name = "imageFile";
  inputFileChild.setAttribute("type","file");
  inputFileChild.setAttribute("accept"," .png, .gif, .jpg, .jpeg");
  uploadImageDiv.appendChild(inputFileChild);
  $("#chooseImageFile").on('change',onImageChange);
  uploadImageButton.innerHTML = "Cancel Image Upload";
  uploadImageButton.setAttribute("onclick","cancelImageUpload()");
}

function cancelImageUpload() {
  var uploadImageDiv = document.getElementById("uploadImageDiv");
  var uploadImageButton = document.getElementById("uploadImageButton");
  var imageHolder = document.getElementById("imageHolder");
  var actualImage = document.getElementById("actualImage");
  imageHolder.style.display = "none";
  actualImage.src = undefined;
  removeChildFromParent("chooseImageFile");
  uploadImageButton.innerHTML = "Upload Image";
  uploadImageButton.setAttribute("onclick","uploadImage()");
}

function onImageChange() {

  imageFileSelected = false;

  var imageHolder = document.getElementById("imageHolder");
  var imageSelector = $("#chooseImageFile");
  var actualImage = document.getElementById("actualImage");

  if(imageSelector[0].files.length == 0) {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    return;
  }

  if(imageSelector[0].files[0].size > 150000) {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    alert("Image File Size: " + imageSelector[0].files[0].size + " bytes should be less than 150,000 bytes");
    return;
  }

  var imgPath = imageSelector[0].value;
  var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
  if( extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" ) {
    if( typeof(FileReader) != "undefined" ) {
      var reader = new FileReader();
      reader.onload = function(e) {
        imageHolder.style.display = "block";
        actualImage.src = e.target.result;
        imageFileSelected = true;
      };
      reader.readAsDataURL(imageSelector[0].files[0]);
    }
    else {
      imageHolder.style.display = "block";
      actualImage.src = "../images/questions/4.jpg";
      imageFileSelected = true;
      alert("Preview unavailable as browser does not support FileReader, but the image is selected.");
    }
  }
  else {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    alert("Only .gif, .png, .jpg, .jpeg are recognized as Image File");
  }

}
