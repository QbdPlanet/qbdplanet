var REFRESH_QUESTION_LIST = "refreshQuestionList";
var REFRESH_QUESTION_LIST_IN_PAPER = "refreshQuestionListInPaper";

function handleOptionContainer(statement) {
  for(var j=0; j<statement.getElementsByClassName("optionContainer").length; ++j) {
    var element = statement.getElementsByClassName("optionContainer")[j];
    var maxChar = 0;
    for(var k=0; k<element.getElementsByTagName("td").length; ++k) {
      var cellElement = element.getElementsByTagName("td")[k];
      if(maxChar < $(cellElement).text().length) { maxChar = $(cellElement).text().length; }
    }
    if(maxChar > 30) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
    else if(maxChar > 15) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
  }
}

function invertHandleOptionContainer(statement) {
  for(var j=0; j<statement.getElementsByClassName("optionContainer").length; ++j) {
    var element = statement.getElementsByClassName("optionContainer")[j];
    var newElement = document.createElement("table");
    newElement.className = "optionContainer";
    $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td><td>"
                         + element.getElementsByTagName("td")[1].innerHTML + "</td><td>"
                         + element.getElementsByTagName("td")[2].innerHTML + "</td><td>"
                         + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
    element.parentNode.replaceChild(newElement,element);
  }
}

function createQuestionElement(questionData,type) {

  var question = document.createElement(type);
  question.className = "questionCard prettybox";

  var floatingDiv = document.createElement("div");
  floatingDiv.className = "floatingDiv";
  question.appendChild(floatingDiv);

  var hiddenInput = document.createElement("INPUT");
  hiddenInput.type = "hidden";
  hiddenInput.value = questionData.questionId;
  question.appendChild(hiddenInput);

  var statement = document.createElement("span");
  statement.className = "questionStatement";
  statement.innerHTML = questionData.question;
  handleOptionContainer(statement);
  question.appendChild(statement);

  // Handle Image
  var image = document.createElement("IMG");
  image.className = "questionImage";
  question.appendChild(image);
  if( !varEmptyOrNull(questionData.imageUrl) ) {
    image.src = rootPath+questionData.imageUrl;
  }
  else {
    image.src = "";
    image.style.display = "none";
  }

  //if(questionData.questionId == 0) {
    var hiddenSubject = document.createElement("INPUT");
    hiddenSubject.type = "hidden";
    hiddenSubject.value = questionData.subject;
    question.appendChild(hiddenSubject);

    var hiddenTopic = document.createElement("INPUT");
    hiddenTopic.type = "hidden";
    hiddenTopic.value = questionData.topic;
    question.appendChild(hiddenTopic);

    var hiddenLevel = document.createElement("INPUT");
    hiddenLevel.type = "hidden";
    hiddenLevel.value = questionData.level;
    question.appendChild(hiddenLevel);
  //}

  return question;
}

function addToFinalCut(card) {
  card.getElementsByClassName("questionStatement")[0].id = "questionStatementFinalCut";
}

function getQuestionIdFromCard(card) { return card.getElementsByTagName("INPUT")[0].value; }
function getQuestionStatementFromCard(card) { return card.getElementsByClassName("questionStatement")[0].innerHTML; }
function getQuestionImageUrlFromCard(card) { return card.getElementsByTagName("IMG")[0].src; }

function addTick(question) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var image = document.createElement("IMG");
  image.src = rootPath+"images/tick.png";
  image.style.width = "3.9675mm";
  floatingDiv.appendChild(image);
}

/*function addDeleteMark(question) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var image = document.createElement("IMG");
  image.src = rootPath+"images/remove.png";
  image.style.width = "3.9675mm";
  $(image).click( function(event) { deleteQuestion($(this)[0]); event.stopPropagation(); });
  floatingDiv.appendChild(image);
}*/

function addToolImage(question) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolImage = document.createElement("IMG");
  toolImage.src = rootPath+"images/spanner.png";
  toolImage.style.width = "6.6125mm";
  var timeoutId;
  $(toolImage).mouseenter(
    function() { 
      timeoutId = window.setTimeout(
        function() {
          timeoutId = null;
          addToolDiv(question); 
        }, 150);
    });
  $(toolImage).mouseleave(function() { window.clearTimeout(timeoutId); });
  floatingDiv.appendChild(toolImage);
}

function createToolButton(text,customFunction) {
  var toolButton = document.createElement("button");
  toolButton.type = "button";
  toolButton.className = "floatingToolButton";
  $(toolButton).click( function(event) { customFunction(getGreatGrandParent($(this)[0])); event.stopPropagation(); });
  toolButton.innerHTML = text;
  return toolButton;
}

function addToolDiv(question) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolDiv = document.createElement("div");
  $(toolDiv).css("background-color","lightgrey");
  $(toolDiv).append(createToolButton("Copy",copyQuestion));
  $(toolDiv).append(createToolButton("Edit",editQuestion));
  $(toolDiv).append(createToolButton("Delete",deleteQuestion));
  $(toolDiv).mouseleave(function() { addToolImage(question); });
  floatingDiv.appendChild(toolDiv);
}

function createFunctionButtons(text,customFunction,question,functionDiv) {
  var toolButton = document.createElement("button");
  toolButton.type = "button";
  toolButton.className = "floatingToolButton";
  $(toolButton).click( function(event) { $(functionDiv).remove(); customFunction(question); event.stopPropagation(); });
  toolButton.innerHTML = text;
  return toolButton;
}

function showQuestionFunctions(question,event,functionList) {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  var functionDiv = document.createElement("div");
  functionDiv.id = "functionDiv";
  var x = event.clientX;
  var y = event.clientY;
  $(functionDiv).css("background-color","lightgrey");
  $(functionDiv).css({
    position: 'fixed',
    top: y,
    left: x,
    zIndex: '1000'
  });
  if(functionList.indexOf("Copy") != -1) {
    $(functionDiv).append(createFunctionButtons("Copy",copyQuestion,question,functionDiv));
  }
  if(functionList.indexOf("Edit") != -1) {
    $(functionDiv).append(createFunctionButtons("Edit",editQuestion,question,functionDiv));
  }
  if(functionList.indexOf("Delete") != -1) {
    $(functionDiv).append(createFunctionButtons("Delete",deleteQuestion,question,functionDiv));
  }
  $('body').append(functionDiv);
}

$(window).click(function() {
  if($('#functionDiv').length > 0) {
    $('#functionDiv').remove();
  }
});

$(window).scroll(function() {
  if($('#functionDiv').length > 0) {
    $('#functionDiv').remove();
  }
});

function removeQuestionFromQuestionList(questionId,questionList) {
  for(var i=0;i<questionList.children.length;++i) {
    if($(questionList.children[i]).hasClass("questionCard")) {
      var tempId = getQuestionIdFromCard(questionList.children[i]);
      if(tempId==questionId) {
        questionList.removeChild(questionList.children[i]);
        return;
      }
    }
  }
}

function handleDeleteQuestion(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  if(response.result = "success") {
    alert(response.message);
    $("."+REFRESH_QUESTION_LIST).each(function(index,element) {
      removeQuestionFromQuestionList(response.questionId,element);
    });
    $("."+REFRESH_QUESTION_LIST_IN_PAPER).each(function(index,element) {
      removeQuestionFromQuestionList(response.questionId,element);
    });
  }
  else if(response.result = "failure") {
    alert(response.message);
  }
  else {
    alert("Unexpected Response Result: " + response.result);
  }
}

function deleteQuestion(question) {
  if(confirm("Are you sure you want to permanantly delete this question from database!")) {
    var id = getQuestionIdFromCard(question);
    var postData = "questionId="+id;
    postRequest("questions/deletequestion.php",postData,handleDeleteQuestion,100);
  }
}

function refreshQuestionInList(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  $("."+REFRESH_QUESTION_LIST).each(function(index,questionList) {
    for(var i=0;i<questionList.children.length;++i) {
      var tempId = getQuestionIdFromCard(questionList.children[i]);
      if(tempId==response.questionId) {
        var question = createQuestionElement(response,"li");
        var functionList = ["Copy","Edit","Delete"];
        $(question).click( function(event) { showQuestionFunctions($(this)[0],event,functionList); event.stopPropagation(); });
        //addToolImage(question);
        questionList.replaceChild(question,questionList.children[i]);
        return;
      }
    }
  });
  $("."+REFRESH_QUESTION_LIST_IN_PAPER).each(function(index,questionList) {
    for(var i=0;i<questionList.children.length;++i) {
      if($(questionList.children[i]).hasClass("questionCard")) {
        var tempId = getQuestionIdFromCard(questionList.children[i]);
        if(tempId==response.questionId) {
          var question = createQuestionElement(response,"li");
          inAddQuestionState(question);
          questionList.replaceChild(question,questionList.children[i]);
          return;
        }
      }
    }
  });
}

function handleEditQuestion(questionId) {
  postRequest("questions/getquestionfromid.php","questionId="+questionId,refreshQuestionInList,100);
}

function editQuestion(question) {
  var id = getQuestionIdFromCard(question);
  setCookie("questionIdToModify",id,1);
  window.open(rootPath+"questions/modifyquestions.html");
}

function copyQuestion(question) {
  var id = getQuestionIdFromCard(question);
  setCookie("questionIdToCopy",id,1);
  window.open(rootPath+"questions/submitquestions.html");
}
