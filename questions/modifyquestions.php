<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("questionId;question;subject;topic;level");

$questionId = $_POST["questionId"];
$question = $_POST["question"];
$subject = $_POST["subject"];
$topic = $_POST["topic"];
$level = $_POST["level"];

$userId = $_SESSION["userId"];

$imageUrl=NULL;
if(isset($_POST["imageUrl"])) {
  $imageUrl=$_POST["imageUrl"];
}
elseif(isset($_FILES["imageFile"]) ) {
  $imageUrl = handleImage($_FILES["imageFile"]);
}

$subject = mysqli_real_escape_string($con,$subject);
$topic = mysqli_real_escape_string($con,$topic);
$query = "SELECT SubjectTopicId FROM SubjectTopic WHERE Subject='$subject' AND Topic='$topic'";
$result = mysqli_query($con,$query);

$subjectTopicId;
if($row = mysqli_fetch_array($result)) {
  $subjectTopicId = $row[0];
}
else {
  $query = "INSERT INTO SubjectTopic (Subject,Topic,UserId) VALUES ('$subject','$topic','$userId')";
  $result = mysqli_query($con,$query);
  $subjectTopicId = $con->insert_id;
}

$level = mysqli_real_escape_string($con,$level);
$query = "SELECT DifficultyLevelId FROM DifficultyLevel WHERE DifficultyLevel='$level'";
$result = mysqli_query($con,$query);

$difficultyLevelId;
if($row = mysqli_fetch_array($result)) {
  $difficultyLevelId = $row[0];
}
else {
  $query = "INSERT INTO DifficultyLevel (DifficultyLevel,UserId) VALUES ('$level','$userId')";
  $result = mysqli_query($con,$query);
  $difficultyLevelId = $con->insert_id;
}

$question = mysqli_real_escape_string($con,$question);
$query = "UPDATE Question SET Question='$question', SubjectTopicId='$subjectTopicId', DifficultyLevelId='$difficultyLevelId',"
          ." ImageUrl='$imageUrl' WHERE QuestionId='$questionId'";
$result = mysqli_query($con,$query);

$response = array();
$response["result"] = "success";
$response["message"] = "Question Modified Successfully";

echo json_encode($response);

?>
