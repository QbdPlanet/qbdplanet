var imageFileSelected = false;
var questionId;
var response;
var rightWing;

function initModifyQuestions() {
  getRequest("categories/getallcategories.php",populateCategory);
  rightWing = document.getElementById("rightWing");
  questionId = getCookie("questionIdToModify");
  if(varEmptyOrNull(questionId)) {
    alert("No Question is selected for modification");
    questionId = 19;
    window.close();
  }
  else {
    deleteCookie("questionIdToModify");
  }
  //initGetCategories(document.getElementById("subject"),document.getElementById("topic"),document.getElementById("level"),"Select");
  //initSubmitCategories(document.getElementById("valueSpace"),refreshSubjectList,refreshLevel);
  postRequest("questions/getquestionfromid.php","questionId="+questionId,initQuestionsField);
}

function initImageStateDiv() {
  if(varEmptyOrNull(response.imageUrl)) {
    $('#imageState').append($("<option></option>").attr("value","1").text("Uploaded"));
    $('#imageState').append($("<option></option>").attr("value","2").text("Left Blank").attr('selected', 'selected'));
  }
  else {
    $('#imageState').append($("<option></option>").attr("value","1").text("Replaced"));
    $('#imageState').append($("<option></option>").attr("value","2").text("No Image"));
    $('#imageState').append($("<option></option>").attr("value","3").text("Initial Image").attr('selected', 'selected'));
  }
}

function initQuestionCard() {

  var initialQuestionState = document.createElement("div");

  var question = createQuestionElement(response,"div");
  question.style.width = "200mm";
  rightWing.insertBefore(question,rightWing.firstChild);

  var subject = document.createElement("span");
  subject.innerHTML = "Subject: "+response.subject+", ";
  question.appendChild(subject);

  var topic = document.createElement("span");
  topic.innerHTML = "Topic: "+response.topic+", ";
  question.appendChild(topic);

  var level = document.createElement("span");
  level.innerHTML = "Level: "+response.level;
  question.appendChild(level);

  //populateSubjectListWithSelectedValues(response.subject,response.topic);
  //populateLevelsWithSelectedValue(response.level);

  initialQuestionState.innerHTML = "Initial Question State: ";
  rightWing.insertBefore(initialQuestionState,rightWing.firstChild);
}

function setSelectTag(selectId,optionText) {
  $("#"+selectId+" option").filter(function() {
      return this.text == optionText; 
  }).attr('selected', true);
}

function initQuestionsField(jsonResponse) {
  response = JSON.parse(jsonResponse);
  initQuestionCard(response);
  initImageStateDiv();

  $("#question").html(response.question);
  populateQuestionCard("#questionStatementFinalCut","#question","Question");

  if(!varEmptyOrNull(response.imageUrl)) {
    var image = document.getElementById("actualImage");
    image.src = rootPath+response.imageUrl;
    $(image).show();
  }

  $("#inputSubject").val(response.subject);
  $("#inputTopic").val(response.topic);
  $("#inputLevel").val(response.level);
  populateTopicDataList();
}

function handleModifyQuestions(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
  if(response.result == "success") {
    window.opener.handleEditQuestion(questionId);
    window.close();
  }
}

function modifyQuestions(){

  initValidForm();

  checkQuestionAndCategories();

  if($("#imageState option:selected").val() == "1") {
    var imgPath = $("#chooseImageFile")[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if(!imageFileSelected) {
      appendValidFormAlert("Either a image file should be chosen or Image State should be changed");
      document.getElementById("imageStateLabel").style.color = "red";
      validForm = false;
    }
  }

  if(!isFormValid()) {
    return;
  }

  /*var formData = new FormData();
  formData.append("questionId",questionId);
  formData.append("question",$('#question').html());
  formData.append("topic",$('#topic').val());
  formData.append("level",$('#level').val());*/
  var formData = getFormData();
  formData.append("questionId",questionId);
  switch($("#imageState option:selected").val()) {
    case "1":
      formData.append("imageFile",document.getElementById("chooseImageFile").files[0]);
    break;
    case "2":
      //Nothing. If we don't set anything, in php we have coded to accept it as null value.
    break;
    case "3":
      formData.append("imageUrl",response.imageUrl);
    break;
    default:
      alert("Code is broken. Contact Admin");
      return;
    break;
  }
  formRequest("questions/modifyquestions.php",formData,handleModifyQuestions,100);
}

function onImageStateChange() {
  switch($("#imageState option:selected").val()) {
    case "1":
      document.getElementById("imageHolder").style.display = "none";
      $("#chooseImageFile").show();
    break;
    case "2":
      document.getElementById("imageHolder").style.display = "none";
      document.getElementById("actualImage").src = undefined;
      $("#chooseImageFile").hide();
    break;
    case "3":
      document.getElementById("imageHolder").style.display = "none";
      if(!varEmptyOrNull(response.imageUrl)) {
        var image = document.getElementById("actualImage");
        image.src = rootPath+response.imageUrl;
        document.getElementById("imageHolder").style.display = "block";
      }
      $("#chooseImageFile").hide();
    break;
    default:
      alert("Code is broken. Contact Admin");
    break;
  }
}

function onImageChange() {

  imageFileSelected = false;

  var imageHolder = document.getElementById("imageHolder");
  var imageSelector = $("#chooseImageFile");
  var actualImage = document.getElementById("actualImage");

  if(imageSelector[0].files.length == 0) {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    return;
  }

  if(imageSelector[0].files[0].size > 150000) {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    alert("Image File Size: " + imageSelector[0].files[0].size + " bytes should be less than 150,000 bytes");
    return;
  }

  var imgPath = imageSelector[0].value;
  var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
  if( extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" ) {
    if( typeof(FileReader) != "undefined" ) {
      var reader = new FileReader();
      reader.onload = function(e) {
        imageHolder.style.display = "block";
        actualImage.src = e.target.result;
        imageFileSelected = true;
      };
      reader.readAsDataURL(imageSelector[0].files[0]);
    }
    else {
      imageHolder.style.display = "block";
      actualImage.src = "../images/questions/4.jpg";
      imageFileSelected = true;
      alert("Preview unavailable as browser does not support FileReader, but the image is selected.");
    }
  }
  else {
    imageHolder.style.display = "none";
    actualImage.src = undefined;
    alert("Only .gif, .png, .jpg, .jpeg are recognized as Image File");
  }

}

