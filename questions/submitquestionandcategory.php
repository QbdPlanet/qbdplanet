<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("question;subject;topic;level");

$question = $_POST["question"];
$subject = $_POST["subject"];
$topic = $_POST["topic"];
$level = $_POST["level"];

$responseQuestion = array();
$responseQuestion["question"] = $question;

$userId = $_SESSION["userId"];

$imageUrl=NULL;
if(isset($_FILES["imageFile"]) ) {
  $imageUrl = handleImage($_FILES["imageFile"]);
}

$subject = mysqli_real_escape_string($con,$subject);
$topic = mysqli_real_escape_string($con,$topic);
$query = "SELECT SubjectTopicId FROM SubjectTopic WHERE Subject='$subject' AND Topic='$topic'";
$result = mysqli_query($con,$query);

$subjectTopicId;
if($row = mysqli_fetch_array($result)) {
  $subjectTopicId = $row[0];
}
else {
  $query = "INSERT INTO SubjectTopic (Subject,Topic,UserId) VALUES ('$subject','$topic','$userId')";
  $result = mysqli_query($con,$query);
  $subjectTopicId = $con->insert_id;
}

$level = mysqli_real_escape_string($con,$level);
$query = "SELECT DifficultyLevelId FROM DifficultyLevel WHERE DifficultyLevel='$level'";
$result = mysqli_query($con,$query);

$difficultyLevelId;
if($row = mysqli_fetch_array($result)) {
  $difficultyLevelId = $row[0];
}
else {
  $query = "INSERT INTO DifficultyLevel (DifficultyLevel,UserId) VALUES ('$level','$userId')";
  $result = mysqli_query($con,$query);
  $difficultyLevelId = $con->insert_id;
}

$question = mysqli_real_escape_string($con,$question);
$query = "INSERT INTO Question (Question,SubjectTopicId,DifficultyLevelId,ImageUrl,UserId) VALUES ('$question','$subjectTopicId','$difficultyLevelId','$imageUrl','$userId')";
if( $result = mysqli_query($con,$query) ) {
  $response["result"] = "success";
  $response["message"] = "Question submitted Successfully";
  $responseQuestion["questionId"] = $con->insert_id;
  $responseQuestion["imageUrl"] = $imageUrl;
  $response["question"] = $responseQuestion;
}
else {
  $response["result"] = "failure";
  $response["message"] = "Question submission failed";
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
