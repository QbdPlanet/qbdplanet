<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("questionId");

$questionId = $_POST["questionId"];

/*
  Check if the question exist in some paper.
  If it does we will not delete it.
*/

$response = array();

$query = "SELECT PaperId FROM PaperQuestion WHERE QuestionId='$questionId' LIMIT 1";
$result = mysqli_query($con,$query);

if($row=mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to delete question. It is used in one or more papers.";
  echo json_encode($response);
  exit();
}

$query="SELECT ImageUrl FROM Question WHERE QuestionId='$questionId' AND ImageUrl!=''";
$result = mysqli_query($con,$query);
if($row = mysqli_fetch_array($result)) {
  if(!unlink($_SERVER['DOCUMENT_ROOT'] . "/" . $row[0])) {
    $response["result"] = "failure";
    $response["message"] = "Not able to delete Image";
    echo json_encode($response);
    exit();
  }
}

$query="DELETE FROM Question WHERE QuestionId='$questionId'";
$result = mysqli_query($con,$query);

$response["result"] = "success";
$response["message"] = "Question Deleted Successfully.";
$response["questionId"] = $questionId;

echo json_encode($response);
exit();

?>
