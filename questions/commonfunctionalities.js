var subjectTopicCategory;
var levelCategory;

function checkQuestionAndCategories() {
  checkTextDiv("#question","Question must be filled out");

  checkText("inputSubject","Subject should be filled");
  checkText("inputLevel","Class should be filled");

}

function populateQuestionCard(cardElement,editElement,placeHolder) {
  if(varEmptyOrNull($(editElement).html())) {
    $(cardElement).html(placeHolder);
  }
  else {
    //$(editElement).text($(editElement).text().replace(/\r?\n/g, '<br />'));
    $(cardElement).html($(editElement).html());
    //$(cardElement).text($(editElement).html());
  }
}

function populateAllLists() {
  refreshLevel();
  refreshSubjectList();
}

function onSubjectChange() {
  populateTopicList();
}

function refreshSubjectList() {
  populateSubjectList();
}

function refreshLevel() {
  populateLevels();
}

function populateSubjectDataList() {
  var prevSubject="";
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#subjectDataList").append("<option value='" + subjectTopicCategory[i].subject + "'/>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

function populateTopicDataList() {
  if(varEmptyOrNull(subjectTopicCategory)) {
    return;
  }
  $("#topicDataList").empty();
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject == $("#inputSubject").val()) {
      $("#topicDataList").append("<option value='" + subjectTopicCategory[i].topic + "'/>");
    }
  }
}

function populateLevelDataList() {
  for(var i=0; i<levelCategory.length; ++i) {
    $("#levelDataList").append("<option value='" + levelCategory[i].level + "'/>");
  }
}

function populateCategory(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  subjectTopicCategory = response.subjectTopic;
  levelCategory = response.level;
  populateSubjectDataList();
  populateLevelDataList();
}

function getFormData() {
  var formData = new FormData();
  formData.append("question",$('#question').html());
  formData.append("subject",$("#inputSubject").val());
  if(varEmptyOrNull($("#inputTopic").val())) {
    formData.append("topic","General");
  }
  else {
    formData.append("topic",$("#inputTopic").val());
  }
  formData.append("level",$("#inputLevel").val());

  return formData;
}

function insertTextAtCursor(text) {
  var sel, range, html;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.getRangeAt && sel.rangeCount) {
      range = sel.getRangeAt(0);
      range.deleteContents();
      var newTextNode = document.createTextNode(text);
      range.insertNode(newTextNode);
      range.setStartAfter(newTextNode);
      range.setEndAfter(newTextNode); 
      sel.removeAllRanges();
      sel.addRange(range);
      //range.insertNode( document.createTextNode(text) );
    }
  }
  else if (document.selection && document.selection.createRange) {
    document.selection.createRange().text = text;
  }
}

function questionKeyDown(evt) {
  var e = event;// || evt;
  var charCode = e.which || e.keyCode;
  if (charCode == 65) {
    insertTextAtCursor("g");
    e.preventDefault();
    return false;
  }
}
