<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("questionId");

$questionId = $_POST["questionId"];

$response = array();

$query="SELECT Question.Question,SubjectTopic.Subject,SubjectTopic.Topic,DifficultyLevel.DifficultyLevel,Question.ImageUrl"
        ." FROM Question JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
        ." JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId WHERE Question.QuestionId='$questionId'";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["question"] = $row[0];
  $response["subject"] = $row[1];
  $response["topic"] = $row[2];
  $response["level"] = $row[3];
  $response["imageUrl"] = $row[4];
}

$response["questionId"] = $questionId;

echo json_encode($response);

mysqli_close($con);

exit();

?>
