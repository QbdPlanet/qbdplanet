var simpleQuestionList;

var elementToView;
var limit=10;
var startPos=0;
var prevStartPos=0;
var stopDownloading=false;

var subjectTopicCategory;
var levelCategory;

var searchStr = "";

function initListQuestions() {
  simpleQuestionList = document.getElementById("questionListDiv");
  $(simpleQuestionList).addClass(REFRESH_QUESTION_LIST);
}

function refreshQuestionList() {
  if( $("#subject > option").length == 0 || $("#topic > option").length == 0 || $("#level > option").length == 0) {
    alert("One or more category fields has zero length");
    return;
  }
  $("#searchQuestionListRow").show();
  $("#questionListDiv").empty();
  selectedSubject = $('#subject option:selected').text();
  selectedTopic = $('#topic option:selected').text();
  selectedLevel = $('#level').val();
  searchStr = $('#searchStr').val();
  stopDownloading = false;
  startPos = 0;
  $("#noResultsFound").hide();
  getQuestionList();
}

function getQuestionList() {
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  //$("#hideLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+startPos;
  postData += "&limit="+limit;
  $("#questionListDiv").empty();
  if(!varEmptyOrNull(searchStr)) { postData += "&searchStr="+searchStr; }
  postRequest("questions/getquestionlist.php",postData,handleGetQuestionList,100);
}

function handleGetQuestionList(jsonResponse) {
  $("#questionListDiv").show();
  var response = JSON.parse(jsonResponse);

  startPos = response.nextIndex;
  prevStartPos = response.prevIndex;

  for(var i=0;i < response.question.length; ++i) {

    var question = createQuestionElement(response.question[i],"li");
    simpleQuestionList.appendChild(question);
    if(i==0) { question.value = startPos-limit+1; }

    question.style.backgroundColor = "White";

    $(question).click( function(event) { 
      var functionList = ["Copy","Edit","Delete"];
      showQuestionFunctions($(this)[0],event,functionList);
      event.stopPropagation(); 
    });
    //addToolImage(question);

  }
  populatePageTrack(startPos,response.totalQuestions);
  if(simpleQuestionList.children.length == 0) { $("#noResultsFound").show(); }
}

function populatePageTrack(questionNum,totalQuestions) {
  if(questionNum>limit) {
    $("#prevListLink").show();
  }
  if(questionNum<totalQuestions) {
    $("#nextListLink").show();
  }
  //$("#hideLink").show();
}

function previousQuestionList() {
  $('html, body').animate({
    scrollTop: $("#searchStrDiv").offset().top
  },10);
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  //$("#hideLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+prevStartPos;
  postData += "&limit="+limit;
  //$("#questionListDiv").html("");
  $("#questionListDiv").empty();
  if(!varEmptyOrNull(searchStr)) { postData += "&searchStr="+searchStr; }
  postRequest("questions/getquestionlist.php",postData,handleGetQuestionList,100);
}

function populateSelectSubject() {
  var prevSubject="";
  $("#subject").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#subject").append("<option>" + subjectTopicCategory[i].subject + "</option>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

function populateSelectTopic() {
  $("#topic").empty();
  $("#topic").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject == $("#subject option:selected").text()) {
      $("#topic").append("<option>" + subjectTopicCategory[i].topic + "</option>");
    }
  }
}

function populateSelectLevel() {
  $("#level").append("<option>ALL</option");
  for(var i=0; i<levelCategory.length; ++i) {
    $("#level").append("<option>" + levelCategory[i].level + "</option>");
  }
}

function populateCategoryInListQuestions(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  subjectTopicCategory = response.subjectTopic;
  levelCategory = response.level;
  populateSelectSubject();
  populateSelectTopic();
  populateSelectLevel();
}

$(function() {
  $("#filter").click(function() {
    $('.selectDiv').toggle();
    if(this.innerHTML == "Show Filter") {
      this.innerHTML = "Hide Filter";
    }
    else if(this.innerHTML == "Hide Filter") {
      this.innerHTML = "Show Filter";
      $("#subject")[0].selectedIndex = 0;
      $("#topic")[0].selectedIndex = 0;
      $("#level")[0].selectedIndex = 0;
    }
  });
  getRequest("categories/getallcategories.php",populateCategoryInListQuestions);
});
