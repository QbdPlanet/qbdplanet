function QuestionList(questionList) {
  this.questions = [];
  for(var i=0; i<questionList.length; ++i) {
    this.questions[i] = new Question(questionList[i]);
  }
}

function QuestionListDiv(questionList) {
  this.list = document.createElement("ol");
  this.questionCard = [];
  for(var i=0; i<questionList.length; ++i) {
    this.questionCard[i] = new QuestionCard(questionList[i]);
    var questionCardWrapper = document.createElement("li");
    questionCardWrapper.appendChild(this.questionCard[i].card);
    this.list.appendChild(questionCardWrapper);
  }
}

QuestionListDiv.prototype.getQuestionCardElement(questionId) {
  for(var i=0; i<this.questionCard.length; ++i) {
    if(this.questionCard[i].questionId. == questionId) {
      return this.questionCard[i].card;
    }
  }
  return null;
}

QuestionListDiv.prototype.addList(questionList) {
}
