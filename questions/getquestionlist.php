<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("subject;topic;level;startPos;limit");

$subject = $_POST["subject"];
$topic = $_POST["topic"];
$level = $_POST["level"];
$startPos = $_POST["startPos"];
$limit = $_POST["limit"];

$searchStr = "";
if(isset($_POST["searchStr"])) {
  $searchStr = $_POST["searchStr"];
}

$userId = $_SESSION["userId"];

/*$subject = "ALL";
$topic = "ALL";
$level = "ALL";*/

$defaultValue = "ALL";

$query = "SELECT Question.QuestionId,Question.Question,Question.ImageUrl," 
                   . "SubjectTopic.Subject,SubjectTopic.Topic,DifficultyLevel.DifficultyLevel" 
                   . " FROM Question JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
                   . " JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId"
                   . " WHERE Question.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (SubjectTopic.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (SubjectTopic.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (DifficultyLevel.DifficultyLevel='$level') )"
                   . " AND Question.Question LIKE '%$searchStr%'"
                   . " ORDER BY Question.QuestionId"
                   . " LIMIT $startPos, $limit";
$result = mysqli_query($con,$query);

$response = array();
$questionResponse = array();
/*$response["questionId"] = array();
$response["question"] = array();
$response["imageUrl"] = array();*/

while($row = mysqli_fetch_array($result) ) {
  $question = array();
  $question["questionId"] = $row[0];
  $question["question"] = $row[1];
  $question["imageUrl"] = $row[2];
  $question["subject"] = $row[3];
  $question["topic"] = $row[4];
  $question["level"] = $row[5];
  array_push($questionResponse,$question);
}
$response["question"] = $questionResponse;

$query = "SELECT COUNT(Question.QuestionId) AS TotalQuestions"
                   . " FROM Question JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
                   . " JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId"
                   . " WHERE Question.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (SubjectTopic.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (SubjectTopic.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (DifficultyLevel.DifficultyLevel='$level') )"
                   . " AND Question.Question LIKE '%$searchStr%'";
$result = mysqli_query($con,$query);

if($row = mysqli_fetch_array($result) ) {
  $response["totalQuestions"] = $row['TotalQuestions'];
}

$response["nextIndex"] = $startPos+$limit;
$response["prevIndex"] = $startPos-$limit;

echo json_encode($response);

mysqli_close($con);

exit();

?>
