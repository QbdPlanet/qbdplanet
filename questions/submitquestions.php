<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("question;topic;level");

$question = $_POST["question"];
$subjectTopicId = $_POST["topic"];
$difficultyLevelId = $_POST["level"];
$answer;

$userId = $_SESSION["userId"];

$imageName=NULL;
if(isset($_FILES["imageFile"]) ) {
  $imageName = handleImage($_FILES["imageFile"]);
}

$question = mysqli_real_escape_string($con,$question);
$query = "INSERT INTO Question (Question,SubjectTopicId,DifficultyLevelId,ImageUrl,UserId) VALUES ('$question','$subjectTopicId','$difficultyLevelId','$imageName','$userId')";
if( $result = mysqli_query($con,$query) ) {
  $response["result"] = "success";
  $response["message"] = "Question submitted Successfully";
}
else {
  $response["result"] = "failure";
  $response["message"] = "Question submission failed";
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
