<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

$response = array();
$response["subjectTopic"] = array();
$response["level"] = array();

$userId = $_SESSION["userId"];

$query="SELECT Subject,Topic,SubjectTopicId FROM SubjectTopic WHERE UserId='$userId' ORDER BY Subject, Topic";
$result=mysqli_query($con,$query);

while($row = mysqli_fetch_array($result)) {
  $temp = array();
  $temp["subject"] = $row[0];
  $temp["topic"] = $row[1];
  $temp["id"] = $row[2];
  array_push($response["subjectTopic"],$temp);
}

$query="SELECT DifficultyLevel,DifficultyLevelId FROM DifficultyLevel WHERE UserId='$userId'";
$result=mysqli_query($con,$query);

while($row = mysqli_fetch_array($result)) {
  $temp = array();
  $temp["level"] = $row[0];
  $temp["id"] = $row[1];
  array_push($response["level"],$temp);
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
