<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("subjectTopicId");

$subjectTopicId = $_POST["subjectTopicId"];

$response = array();

$query = "SELECT QuestionId FROM Question WHERE SubjectTopicId='$subjectTopicId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Question/s exist for this subject and topic";
  echo json_encode($response);
  exit();
}

$query = "SELECT PaperId FROM Paper WHERE PaperSubjectId='$subjectTopicId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Paper/s exist for this subject and topic";
  echo json_encode($response);
  exit();
}

$query = "DELETE FROM SubjectTopic WHERE SubjectTopicId='$subjectTopicId' LIMIT 1";
$result = mysqli_query($con,$query);

$response["result"] = "success";
$response["message"] = "Subject, Topic deleted successfully";
$response["subjectTopicId"] = $subjectTopicId;

echo json_encode($response);

mysqli_close($con);

exit();

?>
