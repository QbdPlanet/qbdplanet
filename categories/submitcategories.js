var parent;
var onSubmitSubject;
var onSubmitLevel;
var subjectElem = "inputSubject";
var topicElem = "inputTopic";
var levelElem = "inputLevel";
var subjectDataList = "subjectDataList";
var submitButtonElem = "submitButtonCategory";
var responseElem = "responseCategory";

$(function() {
  onSubmitSubject = refreshSubjectList;
  onSubmitLevel = refreshLevel;

  var createCategory = document.getElementById("createCategory");

  $(createCategory).append('<button type="button" style="margin: 0mm 2.645mm 2.645mm 0mm;"' 
    + 'onclick="showSubjectAndTopic();">Create new Subject and Topic</button>');
  $(createCategory).append('<button type="button" style="margin: 0mm 2.645mm 2.645mm 0mm;"'
    + 'onclick="showLevel();">Create new Level</button>');
  //createCategory.append('<br>');

  var inputSubjectAndTopicDiv = document.createElement("div");
  inputSubjectAndTopicDiv.id = "inputSubjectAndTopicDiv";
  $(inputSubjectAndTopicDiv).hide();

  var inputSubject = document.createElement("input");
  inputSubject.type = "text";
  inputSubject.id = "inputSubject";
  inputSubject.placeholder = "Subject";
  inputSubject.className = "multiLanguage";
  $(inputSubject).css("margin-right","2.645mm");
  inputSubject.setAttribute("list","subjectDataList");
  inputSubjectAndTopicDiv.appendChild(inputSubject);

  var dataList = document.createElement("DATALIST");
  dataList.id = "subjectDataList";
  inputSubjectAndTopicDiv.appendChild(dataList);

  getRequest("categories/getsubjectlist.php",handleSubjectDataListResponse);

  var inputTopic = document.createElement("input");
  inputTopic.type = "text";
  inputTopic.id = "inputTopic";
  inputTopic.placeholder = "Topic";
  inputTopic.className = "multiLanguage";
  $(inputTopic).css("margin-right","2.645mm");
  inputSubjectAndTopicDiv.appendChild(inputTopic);

  var submitSubjectAndTopicButton = document.createElement("button");
  submitSubjectAndTopicButton.type="button";
  submitSubjectAndTopicButton.innerHTML = "Submit";
  submitSubjectAndTopicButton.addEventListener("click",submitSubjectAndTopic);
  inputSubjectAndTopicDiv.appendChild(submitSubjectAndTopicButton);

  createCategory.appendChild(inputSubjectAndTopicDiv);

  var inputLevelDiv = document.createElement("div");
  inputLevelDiv.id = "inputLevelDiv";
  $(inputLevelDiv).hide();

  var inputLevel = document.createElement("input");
  inputLevel.type = "text";
  inputLevel.id = "inputLevel";
  inputLevel.placeholder = "Level";
  inputLevel.className = "multiLanguage";
  $(inputLevel).css("margin-right","2.645mm");
  inputLevelDiv.appendChild(inputLevel);

  var submitLevelButton = document.createElement("button");
  submitLevelButton.type = "button";
  submitLevelButton.innerHTML = "Submit";
  submitLevelButton.addEventListener("click",submitLevel);
  inputLevelDiv.appendChild(submitLevelButton);

  createCategory.appendChild(inputLevelDiv);

});

function showSubjectAndTopic() {
  $("#inputLevelDiv").hide();
  $("#inputSubjectAndTopicDiv").show();
}

function showLevel() {
  $("#inputSubjectAndTopicDiv").hide();
  $("#inputLevelDiv").show();
}

function initSubmitCategories(a,b,c) {
  parent = a;
  onSubmitSubject = b;
  onSubmitLevel = c;
}

function removeAllElements() {
  removeChildFromParent(subjectElem);
  removeChildFromParent(topicElem);
  removeChildFromParent(levelElem);
  removeChildFromParent(submitButtonElem);
  removeChildFromParent(responseElem);
  removeChildFromParent(subjectDataList);
}

function appendInputTextWithListToParent(childId,listId,childPlaceHolder,filename,customFunction) {
  var child = document.createElement("input");
  child.type = "text";
  child.id = childId;
  child.name = childId;
  child.placeholder = childPlaceHolder;
  child.className = "multiLanguage";
  child.setAttribute("list",listId);
  dataList = document.createElement("DATALIST");
  dataList.id = listId;

  parent.appendChild(child);
  parent.appendChild(dataList);
  getRequest(filename,customFunction);
}

function appendInputTextToParent( childId, childPlaceHolder) {
  var child = document.createElement("input");
  child.type = "text";
  child.id = childId;
  child.name = childId;
  child.placeholder = childPlaceHolder;
  child.className = "multiLanguage";
  parent.appendChild(child);
}

function appendSubmitButtonToParent( childId, submitFunction) {
  var child = document.createElement("button");
  child.type="button";
  child.id = childId;
  child.innerHTML = "Submit";
  child.addEventListener("click",submitFunction);
  parent.appendChild(child);
}

function appendTextToParent(childId,childText) {
  var child = document.createElement("div");
  child.id = childId;
  child.style.display = "inline-block";
  child.innerHTML = childText;
  parent.appendChild(child);
}

function handleSubjectDataListResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  var dataList = document.getElementById(subjectDataList);
  for(var i=0; i<response.length; ++i) {
    var option = document.createElement('option');
    option.value = response[i];
    dataList.appendChild(option);
  }
}

function createSubjectAndTopic() {
  removeAllElements();
  appendInputTextWithListToParent(subjectElem,subjectDataList,"Subject","categories/getsubjectlist.php",handleSubjectDataListResponse);
  appendInputTextToParent(topicElem,"Topic");
  appendSubmitButtonToParent(submitButtonElem, submitSubjectAndTopic);
}

function createLevel() {
  removeAllElements();
  appendInputTextToParent(levelElem,"Level");
  appendSubmitButtonToParent(submitButtonElem, submitLevel);
}

function handleSubmitLevelResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
  if(onSubmitLevel) {
    onSubmitLevel();
  }
}

function submitLevel() {

  var level = document.getElementById(levelElem).value;

  if(varEmptyOrNull(level)) {
    alert("Level Field should not be empty");
    return;
  }

  var postData = "level="+level;
  postRequest("categories/submitlevel.php",postData,handleSubmitLevelResponse);

}

function handleSubmitSubjectAndTopicResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
  if(onSubmitSubject) {
    onSubmitSubject();
  }
}

function submitSubjectAndTopic() {

  var subject = document.getElementById(subjectElem).value;
  var topic = document.getElementById(topicElem).value;

  if(varEmptyOrNull(subject)) {
    alert("Subject Field should not be empty");
    return;
  }

  if(varEmptyOrNull(topic)) {
    alert("Topic Field should not be empty");
    return;
  }

  var postData = "subject="+subject+"&topic="+topic;
  postRequest("categories/submitsubjectandtopic.php",postData,handleSubmitSubjectAndTopicResponse);
}

function showResponse(response) {
  removeAllElements();
  appendTextToParent(responseElem,response);
}
