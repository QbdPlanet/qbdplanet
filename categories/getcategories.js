var subject;
var topic;
var level;
var defaultValue;
var subjectSelectedValue;
var topicSelectedValue;
var levelSelectedValue;

function initGetCategories(a,b,c,d) {
  subject = a;
  topic = b;
  level = c;
  defaultValue = d;
}

function addDefaultValue(parent) {
  if(defaultValue) {
    var option = document.createElement('option');
    option.innerHTML = defaultValue;
    option.value = 0;
    parent.appendChild(option);
  }
}

function handleTopicListResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  addDefaultValue(topic);
  for(var i=0; i<response.topic.length; ++i) {
    var option = document.createElement('option');
    option.innerHTML = response.topic[i];
    option.value = response.subjectTopicId[i];
    topic.appendChild(option);
  }
  if(!varEmptyOrNull(topicSelectedValue)) {
    var selectId=topic.id;
    $("#"+selectId+" option").filter(function() {
        return this.text == topicSelectedValue; 
    }).attr('selected', true);
  }
}

function populateTopicList() {
  topic.options.length = 0;
  var postData = "subject="+subject.options[subject.selectedIndex].text;
  postRequest("categories/gettopiclist.php", postData, handleTopicListResponse);
}

function handleSubjectListResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  addDefaultValue(subject);
  for(var i=0; i<response.length; ++i) {
    var option = document.createElement('option');
    option.innerHTML = response[i];
    option.value = response[i];
    subject.appendChild(option);
  }
  if(!varEmptyOrNull(subjectSelectedValue)) {
    var selectId=subject.id;
    $("#"+selectId+" option").filter(function() {
        return this.text == subjectSelectedValue; 
    }).attr('selected', true);
  }
  if(topic) {
    populateTopicList();
  }
}

function populateSubjectListWithSelectedValues(subjectValue,topicValue) {
  subjectSelectedValue = subjectValue;
  topicSelectedValue = topicValue;
  subject.options.length = 0;
  getRequest("categories/getsubjectlist.php", handleSubjectListResponse);
}

function populateSubjectList() {
  subjectSelectedValue = null;
  topicSelectedValue = null;
  subject.options.length = 0;
  getRequest("categories/getsubjectlist.php", handleSubjectListResponse);
}

function handleLevelResponse(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  addDefaultValue(level);
  for(var i=0; i<response.level.length; ++i) {
    var option = document.createElement('option');
    option.innerHTML = response.level[i];
    option.value = response.levelId[i];
    level.appendChild(option);
  }
  if(!varEmptyOrNull(levelSelectedValue)) {
    var selectId=level.id;
    $("#"+selectId+" option").filter(function() {
        return this.text == levelSelectedValue; 
    }).attr('selected', true);
  }
}

function populateLevelsWithSelectedValue(levelValue) {
  levelSelectedValue = levelValue;
  level.options.length = 0;
  getRequest("categories/getlevels.php", handleLevelResponse);
}

function populateLevels() {
  levelSelectedValue = null;
  level.options.length = 0;
  getRequest("categories/getlevels.php", handleLevelResponse);
}
