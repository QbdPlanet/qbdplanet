<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("levelId");

$levelId = $_POST["levelId"];

$response = array();

$query = "SELECT QuestionId FROM Question WHERE DifficultyLevelId='$levelId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Question/s exist for this level";
  echo json_encode($response);
  exit();
}

$query = "SELECT PaperId FROM Paper WHERE PaperLevelId='$levelId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Paper/s exist for this level";
  echo json_encode($response);
  exit();
}

$query = "DELETE FROM DifficultyLevel WHERE DifficultyLevelId='$levelId' LIMIT 1";
$result = mysqli_query($con,$query);

$response["result"] = "success";
$response["message"] = "Level deleted successfully";
$response["levelId"] = $levelId;

echo json_encode($response);

mysqli_close($con);

exit();

?>
