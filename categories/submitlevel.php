<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("level");

$level = $_POST['level'];

$userId = $_SESSION["userId"];

$query = "SELECT * FROM DifficultyLevel WHERE DifficultyLevel='$level' AND UserId='$userId' LIMIT 1";
$result = mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response = array();
  $response["result"] = "failure";
  $response["message"] = "Level: " . $level  . " already exists.";
  echo json_encode($response);
  exit();
}

$query = "INSERT INTO DifficultyLevel (DifficultyLevel,UserId) VALUES ('$level','$userId')";
$result = mysqli_query($con,$query);

$response = array();
$response["result"] = "success";
$response["message"] = "Level: " . $level . " category created successfully";

echo json_encode($response);

mysqli_close($con);

exit();

?>
