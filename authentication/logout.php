<?php

session_start();
unset($SESSION);
session_destroy();

header("Location: http://" . $_SERVER['SERVER_NAME']);

?>
