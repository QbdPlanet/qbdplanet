<?php

include php("common/utils.php");
include php("common/databaseconnection2.php");

checkPostVariables("displayName;username;password");

$displayName = $_POST["displayName"];
$username = $_POST["username"];
$password = sha1($_POST["password"]);

$response = array();

$query = "SELECT * FROM User WHERE Username='$username'";
$result = mysqli_query($con,$query);
if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "'" . $username . "' username already exists.";
  echo json_encode($response);
  exit();
}

$query = "INSERT INTO User (Name,Username,Password) VALUES ('$displayName','$username','$password')";
$result = mysqli_query($con,$query);
$userId = $con->insert_id;

if($result) {
  $response["result"] = "success";
  $response["message"] = "User created successfully";

  session_start();

  $_SESSION["displayName"] = $displayName;
  $_SESSION["username"] = $username;
  $_SESSION["userId"] = $userId;
  //$_SESSION["LAST_ACTIVITY"] = time();
}
else {
  $response["result"] = "failure";
  $response["message"] = "Not able to Sign up. Please contact site administrator.";
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
