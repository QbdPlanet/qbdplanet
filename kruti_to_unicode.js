var inputTerm = new Array(//"kZsa", 
// "(",")", 
"ñ","Q+Z","sas","aa",")Z","ZZ","‘","’","“","”",

"å","ƒ","„","…","†","‡","ˆ","‰","Š","‹", 

"¶+","d+","[+k","[+","x+","T+","t+","M+","<+","Q+",";+","j+","u+",
"Ùk","Ù","ä","–","—","é","™","=kk","f=k",  

"à","á","â","ã","ºz","º","í","{k","{","=","«",   
"Nî","Vî","Bî","Mî","<î","|","K","}",
"J","Vª","Mª","<ªª","Nª","Ø","Ý","nzZ","æ","ç","Á","xz","#",":",

"v‚","vks","vkS","vk","v","b±","Ã","bZ","b","m","Å",",s",",","_",

"ô","d","Dk","D","£","[k","[","x","Xk","X","Ä","?k","?","³", 
"p","Pk","P","N","t","Tk","T",">","÷","¥",

"ê","ë","V","B","ì","ï","M+","<+","M","<",".k",".",    
"r","Rk","R","Fk","F",")","n","/k","èk","/","Ë","è","u","Uk","U",   

"i","Ik","I","Q","¶","c","Ck","C","Hk","H","e","Ek","E",
";","¸","j","y","Yk","Y","G","o","Ok","O",
"'k","'","\"k","\"","l","Lk","L","g", 

"È","z", 
"Ì","Í","Î","Ï","Ñ","Ò","Ó","Ô","Ö","Ø","Ù","Ük","Ü",

"‚","¨","ks","©","kS","k","h","q","w","`","s","¢","S",
"a","¡","%","W","•","·","∙","·","~j","~","\\","+"," ः",
"^","*","Þ","ß","(","¼","½","¿","À","¾","A","-","&","&","Œ","]","~ ","@",
"ाे","ाॅ","ंै","े्र","अौ","अो","आॅ");

var outputTerm = new Array(//"ksaZ",
//"¼","½", 
"॰","QZ+","sa","a","र्द्ध","Z","\"","\"","'","'",

"०","१","२","३","४","५","६","७","८","९",   

"फ़्","क़","ख़","ख़्","ग़","ज़्","ज़","ड़","ढ़","फ़","य़","ऱ","ऩ",    // one-byte nukta varNas
"त्त","त्त्","क्त","दृ","कृ","न्न","न्न्","=k","f=",

"ह्न","ह्य","हृ","ह्म","ह्र","ह्","द्द","क्ष","क्ष्","त्र","त्र्", 
"छ्य","ट्य","ठ्य","ड्य","ढ्य","द्य","ज्ञ","द्व",
"श्र","ट्र","ड्र","ढ्र","छ्र","क्र","फ्र","र्द्र","द्र","प्र","प्र","ग्र","रु","रू",

"ऑ","ओ","औ","आ","अ","ईं","ई","ई","इ","उ","ऊ","ऐ","ए","ऋ",

"क्क","क","क","क्","ख","ख","ख्","ग","ग","ग्","घ","घ","घ्","ङ",
"च","च","च्","छ","ज","ज","ज्","झ","झ्","ञ",

"ट्ट","ट्ठ","ट","ठ","ड्ड","ड्ढ","ड़","ढ़","ड","ढ","ण","ण्",   
"त","त","त्","थ","थ्","द्ध","द","ध","ध","ध्","ध्","ध्","न","न","न्",    

"प","प","प्","फ","फ्","ब","ब","ब्","भ","भ्","म","म","म्",  
"य","य्","र","ल","ल","ल्","ळ","व","व","व्",   
"श","श्","ष","ष्","स","स","स्","ह", 

"ीं","्र",    
"द्द","ट्ट","ट्ठ","ड्ड","कृ","भ","्य","ड्ढ","झ्","क्र","त्त्","श","श्",

"ॉ","ो","ो","ौ","ौ","ा","ी","ु","ू","ृ","े","े","ै",
"ं","ँ","ः","ॅ","ऽ","ऽ","ऽ","ऽ","्र","्","?","़",":",
"‘","’","“","”",";","(",")","{","}","=","।",".","-","µ","॰",",","् ","/",
"ो","ॉ","ैं","्रे","औ","ओ","ऑ");

var tempTerm="";
var lastCaretPosition=0;
var expectedCaretPosition=0;
var expectedContainer;

function getCaretAtInnerHTML(container) {
  var target = document.createTextNode("\u0001");
  document.getSelection().getRangeAt(0).insertNode(target);
  var position = container.innerHTML.indexOf("\u0001");
  target.parentNode.removeChild(target);
  return position;
}

function isAtExpectedCaretPosition(container) {
  if(expectedContainer == container && expectedCaretPosition == getCaretPosition(container)) {
    return true;
  }
  return false;
}

$(function() {
  $(".typeKruti").keypress(function (event) {
    var keynum;
    var container=this;
    var e = event;

    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }

    var inputChar = String.fromCharCode(keynum);

    if(inputChar==" ") {
      lastCaretPosition = 9999;
      tempTerm="";
      return true;
    }

    container = determineContainer(container);

    var strPos = getCaretAtInnerHTML(container);
    console.log("StrPos: "+strPos);
    var index;

    if(isAtExpectedCaretPosition(container)) {
      tempTerm += inputChar;
      index = getIndexFromInputTerm(tempTerm);
    }
    else {
      index = -1;
    }
    if(index == -1) {
      tempTerm = inputChar;
      index = getIndexFromInputTerm(inputChar);
      if(index==-1) { return false; }

      var caretPositionText = getCaretPosition(container);
      console.log("Initial Caret Position: "+caretPositionText);
      var front = (container.innerHTML).substring(0, strPos);
      var back = (container.innerHTML).substring(strPos, container.innerHTML.length);

      var text = getOutputTermFromIndex(index);
      container.innerHTML = front + text + back;
      caretPositionText += text.length;
      lastCaretPosition = front.length;
      console.log("Final Caret Position: "+caretPositionText);

      var range = document.createRange();
      range.setStart(container.firstChild, caretPositionText);
      range.setEnd(container.firstChild, caretPositionText);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
    else {
      var caretPositionText = getCaretPosition(container);
      console.log("Initial Caret Position: "+caretPositionText);
      var front = (container.innerHTML).substring(0, lastCaretPosition);
      var back = (container.innerHTML).substring(strPos, container.innerHTML.length);

      var text = getOutputTermFromIndex(index);
      container.innerHTML = front + text + back;
      caretPositionText = front.length + text.length;
      lastCaretPosition = front.length;
      console.log("Final Caret Position: "+caretPositionText);

      var range = document.createRange();
      range.setStart(container.firstChild, caretPositionText);
      range.setEnd(container.firstChild, caretPositionText);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
    
    return false;
  });
});

function determineContainer(topMostContainer) {
  var container = window.getSelection().focusNode;
  while(container!=topMostContainer){
    if(container.nodeName != "#text") {
      return container;
    }
    container = container.parentNode;
  }
  return topMostContainer;
}

function getIndexFromInputTerm(inputChar) {
  for(i=0;i<inputTerm.length;++i) {
    if(inputTerm[i]==inputChar) {
      return i;
    }
  }
  return -1;
}

function getOutputTermFromIndex(index) {
  return outputTerm[index];
}
