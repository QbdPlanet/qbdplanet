var selectedInstructionList;
var simpleInstructionList;

function initHandleInstruction(selectedInstructionListId,simpleInstructionListId) {
  selectedInstructionList = document.getElementById(selectedInstructionListId);
  simpleInstructionList = document.getElementById(simpleInstructionListId);
  populateSimpleInstructionList();
}

function getInstructionsArray() {
  /*var instructionsArray = [];
  for(var i=0;i<selectedInstructionList.children.length;++i) {
    instructionsArray.push(selectedInstructionList.children[i].getElementsByTagName("INPUT")[0].value);
  }*/
  var instructionsArray = {};
  for(var i=0;i<selectedInstructionList.children.length;++i) {
    var instruction = {};
    instruction.id = selectedInstructionList.children[i].getElementsByTagName("INPUT")[0].value;
    instruction.instruction = selectedInstructionList.children[i].getElementsByTagName("div")[0].innerHTML;
    instructionsArray[i] = instruction;
    //var tempValue = selectedInstructionList.children[i].getElementsByTagName("div")[0].innerHTML;
    //instructionsArray.push(selectedInstructionList.children[i].getElementsByTagName("INPUT")[0].value);
  }
  return JSON.stringify(instructionsArray);
}

function updateTotalInstructions() {
  document.getElementById("totalInstructions").value = selectedInstructionList.children.length;
  refreshInstructionListBackgroundColor();
}

function createInstructionElement() {
  var instruction = document.createElement("li");
  //instruction.className = "w3-card-4";
  return instruction;
}

function addInstructionToSelectedList(id,value) {
  var instruction = createInstructionElement();

  var hiddenInput = document.createElement("INPUT");
  hiddenInput.type = "hidden";
  hiddenInput.value = id;
  instruction.appendChild(hiddenInput);

  var statement = document.createElement("div");
  statement.style.display = "inline-block";
  statement.innerHTML = value;
  instruction.appendChild(statement);

  addButtonsInInstructionElement(instruction);

  selectedInstructionList.appendChild(instruction);

  updateTotalInstructions();
}

function removeInstructionFromSelectedList(id) {
  for(var i=0; i<selectedInstructionList.children.length; ++i) {
    var instructionId  = selectedInstructionList.children[i].getElementsByTagName("INPUT")[0].value;
    if(id == instructionId) {
      var value = selectedInstructionList.children[i].getElementsByTagName("div")[0].innerHTML;
      addInstructionToSimpleList(id,value);
      selectedInstructionList.removeChild(selectedInstructionList.children[i]);
      updateTotalInstructions();
      return;
    }
  }
}

function closeInstruction(button) {
  var instruction = button.parentNode.parentNode;
  var id = instruction.getElementsByTagName("INPUT")[0].value;
  if(id == 0) {
    selectedInstructionList.removeChild(button.parentNode.parentNode);
  }
  else {
    removeInstructionFromSelectedList(id);
  }
}

function addButtonsInInstructionElement(instruction) {

  var floatingDiv = document.createElement("div");
  floatingDiv.className = "floatingDiv";
  floatingDiv.innerHTML = "";

  var upButton = document.createElement("IMG");
  upButton.height = "7";
  upButton.width = "10";
  upButton.src = rootPath+"images/up.png"
  upButton.style.marginRight = "1.3225mm";
  $(upButton).css("vertical-align","middle");
  $(upButton).click( function(event) { moveUp($(this)[0]); refreshInstructionListBackgroundColor(); event.stopPropagation(); });
  floatingDiv.appendChild(upButton);

  var downButton = document.createElement("IMG");
  downButton.height = "7";
  downButton.width = "10";
  downButton.src = rootPath+"images/down.png";
  downButton.style.marginRight = "1.3225mm";
  $(downButton).css("vertical-align","middle");
  $(downButton).click( function(event) { moveDown($(this)[0]); refreshInstructionListBackgroundColor(); event.stopPropagation(); });
  floatingDiv.appendChild(downButton);

  var closeButton = document.createElement("div");
  closeButton.style.display = "inline-block";
  closeButton.innerHTML = "&times;";
  $(closeButton).click( function(event) { closeInstruction($(this)[0]); event.stopPropagation(); });
  floatingDiv.appendChild(closeButton);

  floatingDiv.style.display = "inline-block";

  instruction.appendChild(floatingDiv);
}

function addInstructionToSimpleList(id,value) {
    var option = document.createElement('option');
    option.value = value;

    var hiddenInput = document.createElement("INPUT");
    hiddenInput.type = "hidden";
    hiddenInput.value = id;
    option.appendChild(hiddenInput);

    simpleInstructionList.appendChild(option);
}

function removeInstructionFromSimpleList(id) {
  for(var i=0; i<simpleInstructionList.options.length; ++i) {
    var instructionId  = simpleInstructionList.options[i].getElementsByTagName("INPUT")[0].value;
    if(id == instructionId) {
      simpleInstructionList.removeChild(simpleInstructionList.options[i]);
      return;
    }
  }
}

function handleSubmitInstruction(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  addInstructionToSelectedList(response.instructionId,response.instruction);
}

function submitInstruction() {
  var value = document.getElementById("instruction").value;
  if(varEmptyOrNull(value)) { alert("Instruction field is empty"); return; }
  if(checkIfInstructionSelected(value)) {
    alert("Instruction: " + value + ", already submitted.");
    return;
  }
  var instructionId = checkInstructionInSimpleList(value);
  if(instructionId != 0) {
    addInstructionToSelectedList(instructionId,value);
    removeInstructionFromSimpleList(instructionId);
  }
  else {
    /*var postData = "instruction="+value;
    postRequest("instructions/submitinstruction.php",postData,handleSubmitInstruction);*/
    addInstructionToSelectedList(0,value);
  }
  document.getElementById("instruction").value = "";
}

function checkIfInstructionSelected(value) {
  if(selectedInstructionList.children.length==0){
    return false;
  }
  for(var i=0;i<selectedInstructionList.children.length;++i) {
    var tempValue = selectedInstructionList.children[i].getElementsByTagName("div")[0].innerHTML;
    if(tempValue==value) {
      return true;
    }
  }
  return false;
}

function checkInstructionInSimpleList(value) {
  if(simpleInstructionList.children.length==0){
    return 0;
  }
  for(var i=0;i<simpleInstructionList.children.length;++i) {
    var tempValue = simpleInstructionList.children[i].value;
    if(tempValue==value) {
      return simpleInstructionList.children[i].getElementsByTagName("INPUT")[0].value;
    }
  }
  return 0;
}

function handlePopulateSimpleInstructionList(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  for(var i=0; i<response.instruction.length; ++i) {
    if(!checkIfInstructionSelected(response.instructionId[i])) {
      addInstructionToSimpleList(response.instructionId[i],response.instruction[i]);
    }
  }
}

function populateSimpleInstructionList() {
  getRequest("instructions/getinstructionlist.php",handlePopulateSimpleInstructionList);
}

function refreshInstructionListBackgroundColor() {
  for(var i=0; i<selectedInstructionList.children.length; ++i) {
    /*if(i%2 == 0) {
      selectedInstructionList.children[i].style.backgroundColor = "lightgrey";
    }
    else {*/
      selectedInstructionList.children[i].style.backgroundColor = "transparent";
    //}
  }
}

$(function() {
  $("#instruction").keydown(function(evt) {
    var e = event || evt;
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) {
      submitInstruction();
      e.preventDefault();
      return false;
    }
  });
});
