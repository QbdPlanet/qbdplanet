if($args.length -eq 2) {
  $configFiles = Get-ChildItem . *.* -rec
  foreach ($file in $configFiles)
  {
    try {
      (Get-Content $file.PSPath -ErrorAction SilentlyContinue).replace($args[0],$args[1]) | Set-Content $file.PSPath 
    } catch { }
  }
}
else {
  echo "Script needs to have two arguments"
}
