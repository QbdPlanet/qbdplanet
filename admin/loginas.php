<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

$userId = $_SESSION["userId"];

if($userId != 1) {
  mysqli_close($con);
  exit();
}

$username = $_GET["u"];

$query = "SELECT Name,UserId,Username FROM User WHERE Username='" . $username . "'";
$result = mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  //session_start();

  $_SESSION["displayName"] = $row[0];
  $_SESSION["userId"] = $row[1];
  $_SESSION["username"] = $row[2];
  //$_SESSION["LAST_ACTIVITY"] = time();
  if(isset($_SESSION["lastPreferredLocation"])) {
    $response["lastPreferredLocation"] = $_SESSION["lastPreferredLocation"];
    unset($_SESSION["lastPreferredLocation"]);
  }
  $response["result"] = "success";
  header("Location: http://" . $_SERVER['SERVER_NAME']);
}
else {
  echo "Invalid Username: " . $username ;
}

mysqli_close($con);

exit();

?>
