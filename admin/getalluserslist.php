<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

$userId = $_SESSION["userId"];

if($userId != 1) {
  mysqli_close($con);
  exit();
}

$query = "SELECT Name,UserName FROM User";
$result = mysqli_query($con,$query);

while($row = mysqli_fetch_array($result)) {
  echo "<a href='loginas.php?u=" . $row[1] . "'>" . $row[0] . "</a><br>";
}

mysqli_close($con);

exit();

?>
