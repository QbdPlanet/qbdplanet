<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("paperId");

$paperId = $_POST['paperId'];

#Step 3 - Prepare response variable.

$response = array();

$response["paperId"] = $paperId;
$response["heading"] = "";
$response["time"] = "";
$response["language"] = "";
$response["pageSize"] = "";
$response["totalMarks"] = 0;
$response["paperSet"] = "";

$response["instructions"] = array();
$response["instructionsId"] = array();

$response["question"] = array();

$query = "SELECT Instructions.Instruction,Instructions.InstructionId FROM PaperInstruction JOIN Instructions"
                 ." ON PaperInstruction.InstructionId=Instructions.InstructionId WHERE PaperInstruction.PaperId = '$paperId'"
                 ." ORDER BY PaperInstruction.InstructionNum";
$result = mysqli_query($con,$query);
while($row = mysqli_fetch_array($result)) {
  array_push($response["instructions"],$row[0]);
  array_push($response["instructionsId"],$row[1]);
}

$query = "SELECT p.Heading,p.Time,p.Language,p.PageSize,i.InstituteName,e.ExamName,s.Subject,d.DifficultyLevel,p.PaperSet" 
         . " FROM Paper p LEFT JOIN Institute i ON p.InstituteId=i.InstituteId" 
         . " LEFT JOIN Exam e ON p.ExamId=e.ExamId"
         . " LEFT JOIN SubjectTopic s ON p.PaperSubjectId=s.SubjectTopicId"
         . " LEFT JOIN DifficultyLevel d ON p.PaperLevelId=d.DifficultyLevelId"
         . " WHERE PaperId='$paperId'";

$result = mysqli_query($con,$query);
while($row = mysqli_fetch_array($result)) {
  $response["heading"] = $row[0];
  $response["time"] = $row[1];
  $response["language"] = $row[2];
  $response["pageSize"] = $row[3];
  $response["instituteName"] = $row[4];
  $response["examType"] = $row[5];
  $response["paperSubject"] = $row[6];
  $response["paperLevel"] = $row[7];
  $response["paperSet"] = $row[8];
}

$query = "SELECT PaperQuestion.Type,PaperQuestion.QuestionNum,PaperQuestion.QuestionId," 
           . "Question.Question,PaperQuestion.PositiveMarks,Question.ImageUrl,PaperQuestion.Content,"
           . "SubjectTopic.Subject,SubjectTopic.Topic,DifficultyLevel.DifficultyLevel"
           . " FROM PaperQuestion LEFT JOIN Question ON PaperQuestion.QuestionId=Question.QuestionId" 
           . " LEFT JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
           . " LEFT JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId"
           . " WHERE PaperQuestion.PaperId='$paperId' ORDER BY PaperQuestion.QuestionNum ASC";

$result = mysqli_query($con,$query);
while($row = mysqli_fetch_array($result)) {
  $question = array();
  $question["type"] = $row[0];
  $question["questionNum"] = $row[1];
  $question["questionId"] = $row[2];
  $question["question"] = $row[3];
  $question["positiveMarks"] = $row[4];
  $question["imageUrl"] = $row[5];
  $question["content"] = $row[6];
  $question["subject"] = $row[7];
  $question["topic"] = $row[8];
  $question["level"] = $row[9];
  array_push($response["question"],$question);
  if($question["type"] == "question") {
    $response["totalMarks"] += $row[4];
  }
  else if($question["type"] == "or") {
    $response["totalMarks"] -= $row[4];
  }
}

#Step 6 - Send all information related to paperId to the client except correct answers.

echo json_encode($response);

mysqli_close($con);

exit();

?>
