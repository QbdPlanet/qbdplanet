<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("paperId");

$paperId = $_POST['paperId'];

#Step 3 - Prepare response variable.

$response = array();

$response["paperId"] = $paperId;
$response["heading"] = "";
$response["time"] = "";
$response["language"] = "";
$response["pageSize"] = "";
$response["totalMarks"] = 0;
$response["totalQuestions"] = 0;
$response["totalInstructions"] = 0;
$response["paperSet"] = "";

//$query = "SELECT p.Heading,p.Time,p.Language,p.PageSize,"
$query = "SELECT p.Heading,p.Time,p.Language,p.PageSize,i.InstituteName,e.ExamName,s.Subject,d.DifficultyLevel,p.paperSet," 
         . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalQuestions,"
         . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOr,"
         . "(SELECT COUNT(pi.PaperId) FROM PaperInstruction pi WHERE pi.PaperId=p.PaperId) AS TotalInstructions,"
         . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalMarks,"
         . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOrMarks"
         . " FROM Paper p LEFT JOIN Institute i ON p.InstituteId=i.InstituteId" 
         . " LEFT JOIN Exam e ON p.ExamId=e.ExamId"
         . " LEFT JOIN SubjectTopic s ON p.PaperSubjectId=s.SubjectTopicId"
         . " LEFT JOIN DifficultyLevel d ON p.PaperLevelId=d.DifficultyLevelId"
         . " WHERE p.PaperId='$paperId'";
$result = mysqli_query($con,$query);
if($row = mysqli_fetch_array($result)) {
  $response["heading"] = $row[0];
  $response["time"] = $row[1];
  $response["language"] = $row[2];
  $response["pageSize"] = $row[3];
  $response["instituteName"] = $row[4];
  $response["examType"] = $row[5];
  $response["paperSubject"] = $row[6];
  $response["paperLevel"] = $row[7];
  $response["paperSet"] = $row[8];
  $response["totalMarks"] = $row['TotalMarks'] - $row['TotalOrMarks'];
  $response["totalQuestions"] = $row['TotalQuestions'] - $row['TotalOr'];
  $response["totalInstructions"] = $row['TotalInstructions'];
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
