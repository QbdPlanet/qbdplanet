function populatePaperLevel() {
  document.getElementById("paperLevel").innerHTML = this.innerHTML;
  document.getElementById("paperLevelDataList").style.display = "none";
}

function blockEnter(evt,focusElement) {
  var charCode = evt.which || evt.keyCode;
  if(charCode == 13) {
    return false;
  }
  return true;
}

function showResult(evt,focusElement) {
  var charCode = evt.which || evt.keyCode;
  if(charCode == 40 || charCode == 38 || charCode == 13) {
    console.log(charCode);
    return false;
  }
  document.getElementById("paperLevelDataList").style.display = "block";
  document.getElementById("paperLevelDataList").innerHTML = "";
  var tabindex = 0;
  for(var i=0; i<levelCategory.length; ++i) {
    var n = levelCategory[i].level.indexOf(document.getElementById("paperLevel").innerHTML);
    if(n != -1 && document.getElementById("paperLevel").innerHTML.length !=0 ) {
      var item = document.createElement("div");
      item.id = tabindex;
      item.className = "suggestionItem";
      item.tabIndex = ++tabindex;
      //item.innerHTML = levelCategory[i];
      item.innerHTML = levelCategory[i].level;
      item.onclick = populatePaperLevel;
      item.onkeydown = function (event) { return handlekeydown(event,this) };
      document.getElementById("paperLevelDataList").appendChild(item);
    }
  }
  if(tabindex == 0) {
    document.getElementById("paperLevelDataList").style.display = "none";
  }
}

function handlekeydown(evt, focusElement) {
  var charCode = evt.which || evt.keyCode;
  if(charCode == 40) {
    ondownkeypress(focusElement);
    return false;
  }
  else if(charCode == 38) {
    onupkeypress(focusElement);
    return false;
  }
  else if(charCode == 13 && focusElement.id != "paperLevel") {
    document.getElementById("paperLevel").innerHTML = focusElement.innerHTML;
    document.getElementById("paperLevelDataList").style.display = "none";
    return false;
  }
  return true;
}

function ondownkeypress(focusElement) {
  if(focusElement.id=="paperLevel") {
    if(document.getElementById("paperLevelDataList").getElementsByTagName("div")[0]) {
      document.getElementById("paperLevelDataList").getElementsByTagName("div")[0].focus();
      document.getElementById("paperLevel").blur();
    }
  }
  else {
    if(focusElement.nextSibling) {
      focusElement.nextSibling.focus();
    }
    else {
      focusElement.parentNode.getElementsByTagName("div")[0].focus();
    }
  }
}

function onupkeypress(focusElement) {
  if(focusElement.id=="paperLevel") {
    if(document.getElementById("paperLevelDataList").getElementsByTagName("div")[0]) {
      document.getElementById("paperLevelDataList").lastChild.focus();
      document.getElementById("paperLevel").blur();
    }
  }
  else {
    if(focusElement.previousSibling) {
      focusElement.previousSibling.focus();
    }
    else {
      document.getElementById("paperLevelDataList").lastChild.focus();
    }
  }
}

$(window).click(function(event) {
  if($("#paperLevelDataList").length) {
    document.getElementById("paperLevelDataList").style.display = "none";
  }
});
