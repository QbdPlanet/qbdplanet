var paperDraftList;

//var elementToView;
var limit=10;
var startPos=0;
var prevStartPos=0;
//var stopDownloading=false;

/*var subjectTopicCategory;
var levelCategory;*/

function initListPaperDrafts() {
  paperDraftList = document.getElementById("paperDraftListDiv");
  getPaperDraftList();
  //paperDraftList.className = REFRESH_PAPER_LIST;
}

/*$(window).scroll(function (event) {
  if(!varEmptyOrNull(elementToView) && !stopDownloading) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elementToView).offset().top;
    var elemBottom = elemTop + $(elementToView).height();

    if(((elemBottom <= docViewBottom) && (elemTop >= docViewTop)))
    {
      elementToView = null;
      getPaperList();
    }
  }
});*/

function refreshPaperDraftList() {
  //if( $("#subject > option").length == 0 || $("#topic > option").length == 0 || $("#level > option").length == 0) {
  /*if( $("#subject > option").length == 0 || $("#topic > option").length == 0) {
    alert("One or more category fields has zero length");
    return;
  }*/
  $("#paperDraftListDiv").empty();
  //selectedSubject = $('#subject option:selected').text();
  //selectedTopic = $('#topic option:selected').text();
  //selectedLevel = $('#level').val();
  //selectedLevel = $('#okay').html();
  //stopDownloading = false;
  startPos = 0;
  $("#noResultsFound").hide();
  getPaperDraftList();
}

function getPaperDraftList() {
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  /*var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+startPos;
  postData += "&limit="+limit;*/
  $("#paperDraftListDiv").empty();

  getPaperDraftListFromLocalStorage(startPos,limit);

  //postRequest("paper/getpaperlist.php",postData,handleGetPaperList,100);
}

function getPaperDraftListFromLocalStorage(startPos,limit) {
  var response = {};
  var paperDraftList = [];
  var i=startPos;
  for( ; i<startPos+limit; ++i) {
    var localPaperDraft = localStorage.getItem("qbdPaperDraft"+i);
    if(!(localPaperDraft === null)) {
      var localResponse = JSON.parse(localPaperDraft);
      var paper = {};
      paper.paperDraftId = i;
      paper.instituteName = localResponse.instituteName;
      paper.examType = localResponse.examType;
      paper.paperSubject = localResponse.paperSubject;
      paper.paperLevel = localResponse.paperLevel;
      paper.time = localResponse.time;
      paper.totalQuestions = localResponse.question.length;
      paper.totalMarks = "-";
      paper.totalInstructions = localResponse.instructionsId.length;

      paperDraftList.push(paper);
    }
    else {
      response.totalPapers = i+1;
      break;
    }
  }
  if(i == startPos+limit) {
    if(localStorage.getItem("qbdPaperDraft"+i) === null) {
      response.totalPapers = i;
    }
    else {
      response.totalPapers = i+1;
    }
  }

  response.nextIndex = startPos+limit;
  response.prevIndex = startPos-limit;
  response.paper = paperDraftList;

  if(response.paper.length > 0) {
    handleGetPaperDraftList(JSON.stringify(response));
  }
}

function handleGetPaperDraftList(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  startPos = response.nextIndex;
  prevStartPos = response.prevIndex;

  for(var i=0;i < response.paper.length; ++i) {

    var paper = createPaperDraftElement(response.paper[i]);
    paperDraftList.appendChild(paper);

  }
  populatePageTrack(startPos,response.totalPapers);
  if(paperDraftList.children.length == 0) { $("#noResultsFound").show(); }
}

function populatePageTrack(paperNum,totalPapers) {
  if(paperNum>limit) {
    $("#prevListLink").show();
  }
  if(paperNum<totalPapers) {
    $("#nextListLink").show();
  }
}

function previousPaperDraftList() {
  /*$('html, body').animate({
    scrollTop: $(".selectDiv").offset().top
  },10);*/
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  $("#paperDraftListDiv").empty();

  getPaperDraftListFromLocalStorage(prevStartPos,limit);

}

function createPaperDraftElement(paperData) {

  var paper= document.createElement("li");
  paper.className = "paperCard prettybox";

  var hiddenInput = document.createElement("INPUT");
  hiddenInput.type = "hidden";
  hiddenInput.value = paperData.paperDraftId;
  paper.appendChild(hiddenInput);

  var paperTable = document.createElement("table");
  if(!varEmptyOrNull(paperData.heading)) {
    appendRow(paperTable,"Paper Name:",paperData.heading);
  }
  else {
    var heading = "";
    var linebreak = false;
    if(!varEmptyOrNull(paperData.instituteName)) {
      heading += paperData.instituteName;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.examType)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.examType;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.paperSubject)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.paperSubject;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.paperLevel)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.paperLevel;
    }
    appendRow(paperTable,"Paper Name:",heading);
  }
  appendRow(paperTable,"Time:",paperData.time);
  appendRow(paperTable,"Total Questions:",paperData.totalQuestions);
  //appendRow(paperTable,"Total Marks:",paperData.totalMarks);
  appendRow(paperTable,"Total Instructions:",paperData.totalInstructions);

  var functionRow = document.createElement("tr");
  functionRow.colSpan = "2";
  functionRow.innerHTML = "<td><a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='openPaperDraft(this.parentNode.parentNode.parentNode.parentNode);'>Open</a>"
                            //+"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='printPaper(this.parentNode.parentNode.parentNode.parentNode);'>Print</a>"
                            //+"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='copyPaper(this.parentNode.parentNode.parentNode.parentNode);'>Copy</a>"
                            +"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='deletePaperDraft(this.parentNode.parentNode.parentNode.parentNode);'>Delete</a></td>";
  paperTable.append(functionRow);

  paper.appendChild(paperTable);

  return paper;
}

function appendRow(table,firstColumn,secondColumn) {
  $(table).append("<tr style='vertical-align: top'><td style='width:39.675mm'>"+firstColumn+"</td><td>"+secondColumn+"</td></tr>");
}

function openPaperDraft(paperDraft) {
  var paperDraftId = paperDraft.getElementsByTagName("INPUT")[0].value;
  setCookie("paperDraftId",paperDraftId,1);
  location.href = rootPath+"index.html";
}

function deletePaperDraft(paperDraft) {
  var paperDraftId = paperDraft.getElementsByTagName("INPUT")[0].value;

  var localPaperDraft = localStorage.getItem("qbdPaperDraft"+paperDraftId);
  localStorage.removeItem("qbdPaperDraft"+paperDraftId);
  if(!(localPaperDraft === null)) {
    var i = Number(paperDraftId)+1;
    while(true) {
      var tempPaperDraft = localStorage.getItem("qbdPaperDraft"+i);
      if(tempPaperDraft === null) {
        break;
      }
      else {
        var tempIndex = i-1;
        localStorage.setItem("qbdPaperDraft"+tempIndex,tempPaperDraft);
        localStorage.removeItem("qbdPaperDraft"+i);
      }
      ++i;
    }
  }

  for(var i=0;i<paperDraftList.children.length; ++i) {
    var tempId = paperDraftList.children[i].getElementsByTagName("INPUT")[0].value;
    if(tempId > paperDraftId) {
      paperDraftList.children[i].getElementsByTagName("INPUT")[0].value = tempId-1;
    } 
  }

  paperDraftList.removeChild(paperDraft);
}

