function initSubmitPaper() {
  $('.container').css("min-height",($(window).height()+10) + 'px');
  selectedQuestionList = document.getElementById("selectedQuestionListDiv");
  simpleQuestionList = document.getElementById("questionListDiv");
  $(simpleQuestionList).addClass(REFRESH_QUESTION_LIST_IN_PAPER);
  initHandleInstruction("selectedInstructionList","simpleInstructionList");
  var paperId = getCookie("paperIdToCopy");
  if(!varEmptyOrNull(paperId)) {
    $('.container').hide();
    $('body').append("<p id='loadingMessage'>Please wait while the paper is loading</p>");
    deleteCookie("paperIdToCopy");
    postRequest("paper/getquestionpaper.php","paperId="+paperId,initPaperFields);
  }
}

function handleSubmitPaper(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
  if(response.result == "success") {
    location.reload();
  }
}

function submitPaper() {

  if(!isPaperValid()) { return; }

  removeMathJaxAndCallback(document.getElementById("paperSpace"),submitPaperToDB);
}

function submitPaperToDB() {

  var postData = getPostData();

  postRequest("paper/submitpaperquestionandcategory.php",postData,handleSubmitPaper,100);

  MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("paperSpace")]);
}
