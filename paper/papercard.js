var REFRESH_PAPER_LIST = "refreshPaperList";
var REFRESH_PAPER_LIST_IN_TEST = "refreshPaperListInTest";

function createPaperElement(paperData) {

  var paper= document.createElement("li");
  paper.className = "paperCard prettybox";

  /*var floatingDiv = document.createElement("div");
  floatingDiv.className = "floatingDiv";
  paper.appendChild(floatingDiv);
  addToolImageToPaperElement(paper);*/
  //$(paper).click( function(event) { showPaperFunctions($(this)[0],event); event.stopPropagation(); });

  var hiddenInput = document.createElement("INPUT");
  hiddenInput.type = "hidden";
  hiddenInput.value = paperData.paperId;
  paper.appendChild(hiddenInput);

  var paperTable = document.createElement("table");
  if(!varEmptyOrNull(paperData.heading)) {
    appendRow(paperTable,"Paper Name:",paperData.heading);
  }
  else {
    var heading = "";
    var linebreak = false;
    if(!varEmptyOrNull(paperData.instituteName)) {
      heading += paperData.instituteName;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.examType)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.examType;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.paperSubject)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.paperSubject;
      linebreak = true;
    }
    if(!varEmptyOrNull(paperData.paperLevel)) {
      if(linebreak) { heading += "<br>";}
      heading += paperData.paperLevel;
    }
    appendRow(paperTable,"Paper Name:",heading);
  }
  appendRow(paperTable,"Time:",paperData.time);
  appendRow(paperTable,"Total Questions:",paperData.totalQuestions);
  appendRow(paperTable,"Total Marks:",paperData.totalMarks);
  appendRow(paperTable,"Total Instructions:",paperData.totalInstructions);

  var functionRow = document.createElement("tr");
  functionRow.colSpan = "2";
  functionRow.innerHTML = "<td><a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='editPaper(this.parentNode.parentNode.parentNode.parentNode);'>Edit</a>"
                            +"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='printPaper(this.parentNode.parentNode.parentNode.parentNode);'>Print</a>"
                            +"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='copyPaper(this.parentNode.parentNode.parentNode.parentNode);'>Copy</a>"
                            +"<a href='javascript:void(0);' style='padding: 1mm; color: blue;' onclick='deletePaper(this.parentNode.parentNode.parentNode.parentNode);'>Delete</a></td>";
  paperTable.append(functionRow);

  paper.appendChild(paperTable);

  return paper;
}

function getId(paper) {
  return paper.getElementsByTagName("INPUT")[0].value;
}

function getTime(paper) {
  return paper.getElementsByTagName("td")[3].innerHTML;
}

function getTotalQuestions(paper) {
  return paper.getElementsByTagName("td")[5].innerHTML;
}

function getTotalMarks(paper) {
  return paper.getElementsByTagName("td")[7].innerHTML;
}

function isSimilar(paperOne,paperTwo) {
  if(getTime(paperOne) != getTime(paperTwo)) {
    return false;
  }
  if(getTotalQuestions(paperOne) != getTotalQuestions(paperTwo)) {
    return false;
  }
  if(getTotalMarks(paperOne) != getTotalMarks(paperTwo)) {
    return false;
  }
  return true;
}

function appendRow(table,firstColumn,secondColumn) {
  $(table).append("<tr style='vertical-align: top'><td style='width:39.675mm'>"+firstColumn+"</td><td>"+secondColumn+"</td></tr>");
}

function removePaperFromPaperList(paperId,paperList) {
  for(var i=0;i<paperList.children.length;++i) {
    var tempId = paperList.children[i].getElementsByTagName("INPUT")[0].value;
    if(tempId==paperId) {
      paperList.removeChild(paperList.children[i]);
      return;
    }
  }
}

function createPaperFunctionButton(text,customFunction,paper,functionDiv) {
  var toolButton = document.createElement("button");
  toolButton.type = "button";
  toolButton.className = "floatingToolButton";
  $(toolButton).click( function(event) { 
    $(functionDiv).remove();
    customFunction(paper);
    if($("#transparentCover").length>0) {
      $("#transparentCover").remove();
    }
    event.stopPropagation();
  });
  toolButton.innerHTML = text;
  return toolButton;
}

function showPaperFunctions(paper,event) {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  var functionDiv = document.createElement("div");
  functionDiv.id = "functionDiv";
  var x = event.clientX;
  var y = event.clientY;
  $(functionDiv).css("background-color","lightgrey");
  $(functionDiv).css({
    position: 'fixed',
    top: y,
    left: x,
    zIndex: '1000'
  });
  $(functionDiv).append(createPaperFunctionButton("Edit",editPaper,paper,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Print",printPaper,paper,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Copy",copyPaper,paper,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Delete",deletePaper,paper,functionDiv));
  $('body').append(functionDiv);
}

$(window).click(function() {
  if($('#functionDiv').length > 0) {
    $('#functionDiv').remove();
  }
});

$(window).scroll(function() {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  if($('#transparentCover').length > 0) { $('#transparentCover').remove(); }
});

/*function addTickToPaperElement(paper) {
  var floatingDiv = paper.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var image = document.createElement("IMG");
  image.src = rootPath+"images/tick.png";
  image.style.width = "3.9675mm";
  floatingDiv.appendChild(image);
}

function addToolImageToPaperElement(paper) {
  var floatingDiv = paper.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolImage = document.createElement("IMG");
  toolImage.src = rootPath+"images/spanner.png";
  toolImage.style.width = "6.6125mm";
  //$(toolImage).mouseenter(function() { addToolDivToPaperElement(paper); });
  $(toolImage).mouseenter(
    function() { 
      timeoutId = window.setTimeout(
        function() {
          timeoutId = null;
          addToolDivToPaperElement(paper);
        }, 150);
    });
  $(toolImage).mouseleave(function() { window.clearTimeout(timeoutId); });
  floatingDiv.appendChild(toolImage);
}

function createToolButtonForPaperElement(text,customFunction) {
  var toolButton = document.createElement("button");
  toolButton.type = "button";
  toolButton.className = "floatingToolButton";
  $(toolButton).click( function(event) { customFunction(getGreatGrandParent($(this)[0])); event.stopPropagation(); });
  toolButton.innerHTML = text;
  return toolButton;
}

function addToolDivToPaperElement(paper) {
  var floatingDiv = paper.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolDiv = document.createElement("div");
  $(toolDiv).css("background-color","lightgrey");
  $(toolDiv).append(createToolButtonForPaperElement("Print",printPaper));
  //$(toolDiv).append(createToolButtonForPaperElement("Print A4",printPaper));
  //$(toolDiv).append(createToolButtonForPaperElement("Print A5",printPaperA5));
  $(toolDiv).append(createToolButtonForPaperElement("Copy",copyPaper));
  $(toolDiv).append(createToolButtonForPaperElement("Edit",editPaper));
  $(toolDiv).append(createToolButtonForPaperElement("Delete",deletePaper));
  $(toolDiv).mouseleave(function() { addToolImageToPaperElement(paper); });
  floatingDiv.appendChild(toolDiv);
}*/

function handleDeletePaper(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  if(response.result=="success") {
    alert(response.message);
    $("."+REFRESH_PAPER_LIST).each(function(index,element) {
      removePaperFromPaperList(response.paperId,element);
    });
  }
  else if(response.result=="failure"){
    alert(response.message);
  }
  else {
    alert("Unexpected response result: " + response.result);
  }
}

function deletePaper(paper) {
  if(confirm("Are you sure you want to permanantly delete this paper from database!")) {
    var paperId = paper.getElementsByTagName("INPUT")[0].value;
    postRequest("paper/deletepaper.php","paperId="+paperId,handleDeletePaper,100);
  }
}

function copyPaper(paper) {
  var paperId = paper.getElementsByTagName("INPUT")[0].value;
  setCookie("paperIdToCopy",paperId,1);
  //window.open(rootPath+"paper/submitpaper.html");
  window.open(rootPath+"index.html");
}

function refreshPaperInList(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  $("."+REFRESH_PAPER_LIST).each(function(index,paperList) {
    for(var i=0;i<paperList.children.length;++i) {
      var tempId = paperList.children[i].getElementsByTagName("INPUT")[0].value;
      if(tempId==response.paperId) {
        var paper = createPaperElement(response);
        paperList.replaceChild(paper,paperList.children[i]);
        return;
      }
    }
  });
}

function handleEditPaper(paperId) {
  postRequest("paper/getpapercardinfo.php","paperId="+paperId,refreshPaperInList,100);
}

function editPaper(paper) {
  var paperId = paper.getElementsByTagName("INPUT")[0].value;
  setCookie("paperIdToModify",paperId,1);
  window.open(rootPath+"paper/modifypaper.html");
}

function printPaper(paper) {
  var paperId = paper.getElementsByTagName("INPUT")[0].value;
  postRequest("paper/getquestionpaper.php","paperId="+paperId,handlePrintPaper,100);
}

function handlePrintPaper(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  if(response.pageSize == "A4") {
    handlePrintPaperA4(jsonResponse);
  }
  else if(response.pageSize == "A5") {
    handlePrintPaperA5(jsonResponse);
  }
  else {
    alert("no page size");
  }
}

