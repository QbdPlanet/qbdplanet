//var levelCategory = ["ALL","Class - 6<sup>th</sup>", "Class - 1<sup>st</sup>", "Class - 3<sup>rd</sup>"];
$(function() {
  $("#okay").click(function(event) {
    var livesearch = document.getElementById("livesearch");
    if(livesearch.style.display == "none") {
      livesearch.style.display = "block";
      for(var i=0; i<livesearch.getElementsByClassName("suggestionSelectItem").length; ++i) {
        if(livesearch.getElementsByClassName("suggestionSelectItem")[i].innerHTML == document.getElementById("okay").innerHTML) {
          livesearch.getElementsByClassName("suggestionSelectItem")[i].focus();
          break;
        }
      }
      event.stopPropagation();
    }
  });
});
function populateSelectLevel() {
  var item = document.createElement("div");
  item.className = "suggestionSelectItem";
  item.tabIndex = 1;
  item.innerHTML = "ALL";
  item.onclick = populateOkay;
  item.onkeydown = function(event) { return handleSelectKeyDown(event,this) };
  document.getElementById("livesearch").appendChild(item);
  for(var i=0; i<levelCategory.length; ++i) {
    var item = document.createElement("div");
    item.className = "suggestionSelectItem";
    item.tabIndex = i+2;
    item.innerHTML = levelCategory[i].level;
    item.onclick = populateOkay;
    item.onkeydown = function(event) { return handleSelectKeyDown(event,this) };
    document.getElementById("livesearch").appendChild(item);
  }
  document.getElementById("okay").innerHTML = "ALL";
  document.getElementById("livesearch").style.display = "block";
  //document.getElementById("okay").style.width = (document.getElementById("livesearch").offsetWidth) + "px";
  document.getElementById("livesearch").style.display = "none";
}
$(window).click(function(event) {
  document.getElementById("livesearch").style.display = "none";
});
function populateOkay() {
  document.getElementById("okay").innerHTML = this.innerHTML;
  document.getElementById("livesearch").style.display = "none";
}
function handleSelectKeyDown(evt,focusElement) {
  var charCode = evt.which || evt.keyCode;
  if(charCode == 40) {
    if(focusElement.nextSibling) {
      focusElement.nextSibling.focus();
    }
    return false;
  }
  else if(charCode == 38) {
    if(focusElement.previousSibling) {
      focusElement.previousSibling.focus();
    }
    return false;
  }
  else if(charCode == 13) {
    document.getElementById("okay").innerHTML = focusElement.innerHTML;
    document.getElementById("livesearch").style.display = "none";
    return false;
  }
  return true;
}
