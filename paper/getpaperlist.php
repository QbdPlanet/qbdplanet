<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("subject;topic;level;startPos;limit");

$subject = $_POST["subject"];
$topic = $_POST["topic"];
$level = $_POST["level"];
$startPos = $_POST["startPos"];
$limit = $_POST["limit"];

$institute = "ALL";
$exam = "ALL";

$userId = $_SESSION["userId"];

/*$subject = "ALL";
$topic = "ALL";
$level = "ALL";
$startPos = 0;
$limit = 20;*/

$defaultValue = "ALL";

/*$query = "SELECT p.PaperId,p.Heading,p.Time,p.Language,p.PageSize,"
                   . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalQuestions,"
                   . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOr,"
                   . "(SELECT COUNT(pi.PaperId) FROM PaperInstruction pi WHERE pi.PaperId=p.PaperId) AS TotalInstructions,"
                   . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalMarks,"
                   . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOrMarks"
                   . " FROM Paper p JOIN PaperQuestion pq ON p.PaperId=pq.PaperId"
                   . " JOIN Question q ON pq.QuestionId=q.QuestionId JOIN SubjectTopic st ON q.SubjectTopicId=st.SubjectTopicId"
                   . " JOIN DifficultyLevel dl ON q.DifficultyLevelId=dl.DifficultyLevelId"
                   . " WHERE p.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (st.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (st.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (dl.DifficultyLevel='$level') )"
                   . " GROUP BY p.PaperId"
                   . " ORDER BY COUNT(pq.QuestionId) DESC"
                   . " LIMIT $startPos, $limit";*/
$query = "SELECT p.PaperId,p.Heading,p.Time,p.Language,p.PageSize,i.InstituteName,e.ExamName,s.Subject,d.DifficultyLevel," 
         . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalQuestions,"
         . "(SELECT COUNT(pq.PaperId) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOr,"
         . "(SELECT COUNT(pi.PaperId) FROM PaperInstruction pi WHERE pi.PaperId=p.PaperId) AS TotalInstructions,"
         . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='question') AS TotalMarks,"
         . "(SELECT SUM(pq.PositiveMarks) FROM PaperQuestion pq WHERE pq.PaperId=p.PaperId AND pq.Type='or') AS TotalOrMarks"
         . " FROM Paper p LEFT JOIN Institute i ON p.InstituteId=i.InstituteId" 
         . " LEFT JOIN Exam e ON p.ExamId=e.ExamId"
         . " LEFT JOIN SubjectTopic s ON p.PaperSubjectId=s.SubjectTopicId"
         . " LEFT JOIN DifficultyLevel d ON p.PaperLevelId=d.DifficultyLevelId"
         . " WHERE ( (s.Subject='$subject') OR ('$subject'='$defaultValue') )"
         . " AND ( (d.DifficultyLevel='$level') OR ('$level'='$defaultValue') )"
         . " AND ( (i.InstituteName='$institute') OR ('$institute'='$defaultValue') )"
         . " AND ( (e.ExamName='$exam') OR ('$exam'='$defaultValue') )"
         . " AND p.UserId='$userId'"
         . " ORDER BY p.PaperId DESC"
         . " LIMIT $startPos, $limit";
$result = mysqli_query($con,$query);

$response = array();
$paperResponse = array();

while($row = mysqli_fetch_array($result)) {
  $paper = array();
  $paper["paperId"] = $row[0];
  $paper["heading"] = $row[1];
  $paper["time"] = $row[2];
  $paper["language"] = $row[3];
  $paper["pageSize"] = $row[4];
  $paper["instituteName"] = $row[5];
  $paper["examType"] = $row[6];
  $paper["paperSubject"] = $row[7];
  $paper["paperLevel"] = $row[8];
  $paper["totalMarks"] = $row['TotalMarks'] - $row['TotalOrMarks'];
  $paper["totalQuestions"] = $row['TotalQuestions'] - $row['TotalOr'];
  $paper["totalInstructions"] = $row['TotalInstructions'];
  //array_push($response,$paper);
  array_push($paperResponse,$paper);
}
$response["paper"] = $paperResponse;

/*$query = "SELECT COUNT(DISTINCT p.PaperId) AS TotalPapers"
                   . " FROM Paper p JOIN PaperQuestion pq ON p.PaperId=pq.PaperId"
                   . " JOIN Question q ON pq.QuestionId=q.QuestionId JOIN SubjectTopic st ON q.SubjectTopicId=st.SubjectTopicId"
                   . " JOIN DifficultyLevel dl ON q.DifficultyLevelId=dl.DifficultyLevelId"
                   . " WHERE p.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (st.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (st.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (dl.DifficultyLevel='$level') )";*/
$query = "SELECT COUNT(p.PaperId) AS TotalPapers" 
         . " FROM Paper p LEFT JOIN Institute i ON p.InstituteId=i.InstituteId" 
         . " LEFT JOIN Exam e ON p.ExamId=e.ExamId"
         . " LEFT JOIN SubjectTopic s ON p.PaperSubjectId=s.SubjectTopicId"
         . " LEFT JOIN DifficultyLevel d ON p.PaperLevelId=d.DifficultyLevelId"
         . " WHERE ( (s.Subject='$subject') OR ('$subject'='$defaultValue') )"
         . " AND ( (d.DifficultyLevel='$level') OR ('$level'='$defaultValue') )"
         . " AND ( (i.InstituteName='$institute') OR ('$institute'='$defaultValue') )"
         . " AND ( (e.ExamName='$exam') OR ('$exam'='$defaultValue') )"
         . " AND p.UserId='$userId'";
$result = mysqli_query($con,$query);

if($row = mysqli_fetch_array($result) ) {
  $response["totalPapers"] = $row['TotalPapers'];
}

$response["nextIndex"] = $startPos+$limit;
$response["prevIndex"] = $startPos-$limit;

echo json_encode($response);

mysqli_close($con);

exit();

?>
