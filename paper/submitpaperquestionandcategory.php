<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

//checkPostVariables("paperName;time;questions;instructions;language;pageSize");
checkPostVariables("instituteName;examType;paperSubject;paperLevel;time;questions;instructions;language;pageSize;paperSet");

//$paperName = $_POST["paperName"];
$instituteName = $_POST["instituteName"];
$examType = $_POST["examType"];
$paperSubject = $_POST["paperSubject"];
$paperLevel = $_POST["paperLevel"];
$time = $_POST["time"];
$language = $_POST["language"];
$pageSize = $_POST["pageSize"];
$paperSet = $_POST["paperSet"];
$questions = $_POST["questions"];
$instructions = $_POST["instructions"];

$userId = $_SESSION["userId"];

$questionArray = json_decode($questions,true); // true variable states that first variable is object
if(count($questionArray) == 0 ) {
  $response = array();
  $response["result"] = "failure_1";
  $response["message"] = "Question Field is empty";
  echo json_encode($response);
  exit();
}

$instructionArray = json_decode($instructions,true);
if(count($instructionArray) == 0 ) {
  $response = array();
  $response["result"] = "failure_2";
  $response["message"] = "Instruction Field is empty";
  echo json_encode($response);
  exit();
}

for($i=0;$i<count($instructionArray);++$i) {
  if($instructionArray[$i]["id"] == 0) {
    $instruction = mysqli_real_escape_string($con,$instructionArray[$i]["instruction"]);
    $query = "INSERT INTO Instructions (Instruction,UserId) VALUES ('$instruction','$userId')";
    $result = mysqli_query($con,$query);
    $instructionArray[$i]["id"] = $con->insert_id;
  }
}

$instituteId;
if($instituteName != NULL && $instituteName != "") {
  $instituteId = getInstituteId($con,$instituteName,$userId);
  $instituteId = "'$instituteId'";
}
else {
  $instituteId = "NULL";
}

$examId;
if($examType != NULL && $examType != "") {
  $examId = getExamId($con,$examType,$userId);
  $examId = "'$examId'";
}
else {
  $examId = "NULL";
}

$paperSubjectId;
$questionSubjectId;
if($paperSubject != NULL && $paperSubject != "") {
  $paperSubjectId = getSubjectTopicId($con,$paperSubject,"General",$userId);
  $paperSubjectId = "'$paperSubjectId'";
  $questionSubjectId = $paperSubjectId;
}
else {
  $paperSubjectId = "NULL";
  $questionSubjectId = getSubjectTopicId($con,"General","General",$userId);
}

$paperLevelId;
$questionLevelId;
if($paperLevel !=NULL && $paperLevel != "") {
  $paperLevelId = getDifficultyLevelId($con,$paperLevel,$userId);
  $paperLevelId = "'$paperLevelId'";
  $questionLevelId = $paperLevelId;
}
else {
  $paperLevelId = "NULL";
  $questionLevelId = getDifficultyLevelId($con,"General",$userId);
}

$paperId=0;

$time = mysqli_real_escape_string($con,$time);
$query="INSERT INTO Paper (InstituteId,ExamId,PaperSubjectId,PaperLevelId,Time,Language,PageSize,UserId,PaperSet)" 
       . " VALUES ($instituteId,$examId,$paperSubjectId,$paperLevelId,'$time','$language','$pageSize','$userId','$paperSet')";
$result = mysqli_query($con,$query);
$paperId = $con->insert_id;

if($paperId == 0) {
  $response = array();
  $response["result"] = "failure_3";
  $response["message"] = "Unable to create new paper";
  echo json_encode($response);
  mysqli_close($con);
  exit();
}

for($i=0;$i<count($questionArray);++$i) {
  if($questionArray[$i]["type"] == "question" && $questionArray[$i]["questionId"] != 0 && $questionArray[$i]["modified"] == "yes") {
    $questionArray[$i]["questionId"] = 0;
  }
}

for($i=0;$i<count($questionArray);++$i) {
  if($questionArray[$i]["type"] == "question" && $questionArray[$i]["questionId"] == 0) {
    /*$subject = mysqli_real_escape_string($con,$questionArray[$i]["subject"]);
    $topic = mysqli_real_escape_string($con,$questionArray[$i]["topic"]);
    $query = "SELECT SubjectTopicId FROM SubjectTopic WHERE Subject='$subject' AND Topic='$topic' AND UserId='$userId'";
    $result = mysqli_query($con,$query);

    $subjectTopicId;
    if($row = mysqli_fetch_array($result)) {
      $subjectTopicId = $row[0];
    }
    else {
      $query = "INSERT INTO SubjectTopic (Subject,Topic,UserId) VALUES ('$subject','$topic','$userId')";
      $result = mysqli_query($con,$query);
      $subjectTopicId = $con->insert_id;
    }

    $level = mysqli_real_escape_string($con,$questionArray[$i]["level"]);
    $query = "SELECT DifficultyLevelId FROM DifficultyLevel WHERE DifficultyLevel='$level' AND UserId='$userId'";
    $result = mysqli_query($con,$query);

    $difficultyLevelId;
    if($row = mysqli_fetch_array($result)) {
      $difficultyLevelId = $row[0];
    }
    else {
      $query = "INSERT INTO DifficultyLevel (DifficultyLevel,UserId) VALUES ('$level','$userId')";
      $result = mysqli_query($con,$query);
      $difficultyLevelId = $con->insert_id;
    }*/

    $imageName="NULL";
    $question = mysqli_real_escape_string($con,$questionArray[$i]["question"]);
    //$query = "INSERT INTO Question (Question,SubjectTopicId,DifficultyLevelId,ImageUrl,UserId,SourceId) VALUES ('$question','$subjectTopicId','$difficultyLevelId',$imageName,'$userId','$paperId')";
    $query = "INSERT INTO Question (Question,SubjectTopicId,DifficultyLevelId,ImageUrl,UserId,SourceId) VALUES ('$question',$questionSubjectId,$questionLevelId,$imageName,'$userId','$paperId')";
    //echo $query; exit();
    if( $result = mysqli_query($con,$query) ) {
      $questionArray[$i]["questionId"] = $con->insert_id;
    }
    else {
      $response = array();
      $response["result"] = "failure_4";
      $response["message"] = "Paper submission failed because of question:" + $questionArray[$i]["question"];
      echo json_encode($response);
      exit();
    }
  }
}

$rowArray = array();
for($i=0;$i<count($questionArray);++$i) {
  if($questionArray[$i]["type"] == "section") {
    $section = mysqli_real_escape_string($con,$questionArray[$i]["section"]);
    array_push($rowArray,"('" . $paperId . "','" . $questionArray[$i]["type"] . "','" . ($i+1) . "',NULL,NULL,'" . $section . "')");
  }
  else if($questionArray[$i]["type"] == "or") {
    array_push($rowArray,"('" . $paperId . "','" . $questionArray[$i]["type"] . "','" . ($i+1) . "',NULL,'" . $questionArray[$i]["positiveMarks"] . "',NULL)");
  }
  else if($questionArray[$i]["type"] == "question") {
  array_push($rowArray,"('" . $paperId . "','" . $questionArray[$i]["type"] . "','" . ($i+1) . "','" . $questionArray[$i]["questionId"] . "','" . $questionArray[$i]["positiveMarks"] . "',NULL)");
  }
  else if($questionArray[$i]["type"] == "pagebreak") {
    array_push($rowArray,"('" . $paperId . "','" . $questionArray[$i]["type"] . "','" . ($i+1) . "',NULL,Null,Null)");
  }
  else if($questionArray[$i]["type"] == "line") {
    array_push($rowArray,"('" . $paperId . "','" . $questionArray[$i]["type"] . "','" . ($i+1) . "',NULL,Null,Null)");
  }
}

$rows = implode(",",$rowArray);

$query = "INSERT INTO PaperQuestion (PaperId,Type,QuestionNum,QuestionId,PositiveMarks,Content) VALUES " . $rows;
$result = mysqli_query($con,$query);

$rowArray = array();
for($i=0;$i<count($instructionArray);++$i) {
  array_push($rowArray,"('" . $paperId . "','" . $instructionArray[$i]["id"] . "','" . ($i+1) . "')");
}

$rows = implode(",",$rowArray);

$query = "INSERT INTO PaperInstruction (PaperId,InstructionId,InstructionNum) VALUES " . $rows;
$result = mysqli_query($con,$query);

$response = array();
$response["result"] = "success";
$response["message"] = "Paper submitted successfully";
echo json_encode($response);

mysqli_close($con);


?>
