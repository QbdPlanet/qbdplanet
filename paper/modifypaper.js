var paperIdToModify;

function initModifyPaper() {
  paperIdToModify = getCookie("paperIdToModify");
  if(!varEmptyOrNull(paperIdToModify)) {
    $('.container').hide();
    $('body').append("<p id='loadingMessage'>Please wait while the paper is loading</p>");
    deleteCookie("paperIdToModify");
    postRequest("paper/getquestionpaper.php","paperId="+paperIdToModify,initPaperFields);
  }
  else {
    window.close();
  }
  $('.container').css("min-height",($(window).height()+10) + 'px');
  selectedQuestionList = document.getElementById("selectedQuestionListDiv");
  simpleQuestionList = document.getElementById("questionListDiv");
  $(simpleQuestionList).addClass(REFRESH_QUESTION_LIST_IN_PAPER);
  initHandleInstruction("selectedInstructionList","simpleInstructionList");
}

function handleModifyPaper(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  alert(response.message);
  if(response.result == "success") {
    window.opener.handleEditPaper(paperIdToModify);
    window.close();
  }
}

function modifyPaper() {

  if(!isPaperValid()) { return; }

  removeMathJaxAndCallback(document.getElementById("paperSpace"),modifyPaperInDB);
}

function modifyPaperInDB() {

  var postData = getPostData();

  postData += "&paperId="+paperIdToModify;

  postRequest("paper/modifypaperquestionandcategory.php",postData,handleModifyPaper,100);

  MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("paperSpace")]);
}

