<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

checkPostVariables("paperId");

$paperId = $_POST["paperId"];

/*
  Check if the paper exist in some students testscore.
  If it does we will not delete it.
*/

$response = array();

$query="DELETE FROM PaperQuestion WHERE PaperId='$paperId'";
$result = mysqli_query($con,$query);

$query="DELETE FROM PaperInstruction WHERE PaperId='$paperId'";
$result = mysqli_query($con,$query);

$query="DELETE FROM Paper WHERE PaperId='$paperId'";
$result = mysqli_query($con,$query);

$response["result"] = "success";
$response["message"] = "Paper Deleted Successfully.";
$response["paperId"] = $paperId;

echo json_encode($response);
exit();

?>
