var selectedQuestionList;
var simpleQuestionList;

var limit=10;
var startPos=0;
var prevStartPos=0;

var listIdCounter = 0;

var subjectTopicCategory;
var levelCategory;

var searchStr = "";

//var resizeImageVar;

function checkIfQuestionSelected(id) {
  if($("#selectedQuestionListDiv .questionCardTable").length==0){
    return false;
  }
  for(var i=0;i<$("#selectedQuestionListDiv .questionCardTable").length;++i) {
    var tempId = $("#selectedQuestionListDiv .questionCardTable")[i].getElementsByTagName("INPUT")[0].value;
    if(tempId==id) {
      return true;
    }
  }
  return false;
}

function updatePaperLogistics() {
  updateNumbering();
  updateTotalQuestionsAndMarks();
}

function updateNumbering() {
  var counter=0;
  for(var i=0;i<selectedQuestionList.children.length; ++i) {
    if($(selectedQuestionList.children[i]).hasClass("questionCardTable")) {
      if(i!=0 && $(selectedQuestionList.children[i-1]).hasClass("orCard")) {
        selectedQuestionList.children[i].getElementsByClassName("questionNum")[0].innerHTML = "Q." + counter;
        selectedQuestionList.children[i].getElementsByClassName("questionNum")[0].style.color = "white";
      }
      else {
        selectedQuestionList.children[i].getElementsByClassName("questionNum")[0].innerHTML = "Q." + (++counter);
        selectedQuestionList.children[i].getElementsByClassName("questionNum")[0].style.color = "black";
      }
    }
  }
}

function updateTotalQuestionsAndMarks() {
  document.getElementById("totalQuestions").value = $("#selectedQuestionListDiv .questionCardTable").length-$("#selectedQuestionListDiv .orCard").length;
  updateTotalMarks();
}

function updateTotalMarks() {
  var totalMarks = 0;
  for(var i=0;i<selectedQuestionList.children.length;++i) {
    if($(selectedQuestionList.children[i]).hasClass("questionCardTable")) { 
      if((i==0 || !$(selectedQuestionList.children[i-1]).hasClass("orCard"))) {
        selectedQuestionList.children[i].getElementsByClassName("marksWithBrackets")[0].style.color = "black";
        var positiveMarks = selectedQuestionList.children[i].getElementsByClassName("positiveMarksSpan")[0].innerHTML;
        if(!$.isNumeric(positiveMarks) || positiveMarks <= 0) {
          alert("All questions should have positive number in positive marks field");
          totalMarks = 0;
          break;
        }
        totalMarks += parseFloat(positiveMarks);
      }
      else {
        selectedQuestionList.children[i].getElementsByClassName("marksWithBrackets")[0].style.color = "white";
      }
    }
  }
  document.getElementById("totalMarks").innerHTML = totalMarks;
}

function inAddQuestionState(question) {
  question.style.backgroundColor = "White";
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  question.onclick = function(event) {
    var mathJax = MathJax.Hub.getAllJax(question);
    for(i=0; i<mathJax.length; ++i) {
      if(i==mathJax.length-1) {
        MathJax.Hub.Queue(["Remove",mathJax[i]],[function() {
          addQuestionToSelectedQuestionList(question);
          inRemoveQuestionState(question);
        }]);
      }
      else {
        MathJax.Hub.Queue(["Remove",mathJax[i]]);
      }
    }
    if(mathJax.length==0) {
      addQuestionToSelectedQuestionList(question);
      inRemoveQuestionState(question);
    }
    event.stopPropagation();
  };
}

function removeQuestionFromSelectedQuestionList(questionToRemove) {
  if($(questionToRemove).hasClass("questionCardTable")) {
    $(questionToRemove).remove();
    //updating question in search
    var questionId = questionToRemove.getElementsByTagName("INPUT")[0].value;
    if(questionId!=0) {
      var questionInSearch = getQuestionInSearchFromQuestionId(questionId);
      if(!varEmptyOrNull(questionInSearch)) {
        inAddQuestionState(questionInSearch);
      }
    }
  }
  else {
    var questionId = questionToRemove.getElementsByTagName("INPUT")[0].value;
    for(var i=0;i<selectedQuestionList.children.length;++i) {
      if($(selectedQuestionList.children[i]).hasClass("questionCardTable")) {
        var tempId = selectedQuestionList.children[i].getElementsByTagName("INPUT")[0].value;
        if(tempId==questionId) {
          selectedQuestionList.removeChild(selectedQuestionList.children[i]);
        }
      }
    }
  }
  updatePaperLogistics();
}

function getQuestionInSearchFromQuestionId(questionId) {
  for(var i=0; i<simpleQuestionList.children.length;++i) {
    var tempId = simpleQuestionList.children[i].getElementsByTagName("INPUT")[0].value;
    if(tempId==questionId) {
      return simpleQuestionList.children[i];
    }
  }
  return null;
}

function inRemoveQuestionState(question) {
  question.style.backgroundColor = "AliceBlue";
  addTick(question);
  question.onclick = function(event) {
    removeQuestionFromSelectedQuestionList(this);
    inAddQuestionState(question);
    event.stopPropagation();
  };
}

function addQuestionToSelectedQuestionList(questionToAdd) {
  var questionArray = {};
  questionArray["question"] = questionToAdd.getElementsByClassName("questionStatement")[0].innerHTML;
  questionArray["questionId"] = questionToAdd.getElementsByTagName("INPUT")[0].value;
  questionArray["subject"] = questionToAdd.getElementsByTagName("INPUT")[1].value;
  questionArray["topic"] = questionToAdd.getElementsByTagName("INPUT")[2].value;
  questionArray["level"] = questionToAdd.getElementsByTagName("INPUT")[3].value;
  questionArray["imageUrl"] = "";
 
  appendQuestionToSelectedQuestionList(questionArray);
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,questionToAdd]);
  updatePaperLogistics();
}

function appendQuestionToSelectedQuestionList(questionArray,positiveMarks) {
  var questionTable = document.createElement("table");
  questionTable.className = "questionCardTable";
  questionTable.style.width = "100%";
  questionTable.id = ++listIdCounter;

  var questionRow = document.createElement("tr");

  var questionNum = document.createElement("td");
  questionNum.className = "questionNum";
  questionNum.style.width = "10mm";
  questionRow.appendChild(questionNum);

  var question = document.createElement("td");
  question.innerHTML = questionArray.question;
  //$(question).find('script').remove();
  handleOptionContainer(question);
  question.className = "questionCard prettybox";
  //question.style.width = "100%";
  //question.style.width = "194mm";
  questionRow.appendChild(question);
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,question]);

  var marks = document.createElement("td");
  marks.className = "marksWithBrackets";
  marks.style.width = "10mm";
  var positiveMarksSpan = document.createElement("span");
  positiveMarksSpan.contentEditable = true;
  positiveMarksSpan.className = "positiveMarksSpan";
  if(varEmptyOrNull(positiveMarks)) {
    positiveMarksSpan.innerHTML = "4";
  }
  else {
    positiveMarksSpan.innerHTML = positiveMarks;
  }
  positiveMarksSpan.onblur = function() { updateTotalMarks() };
  $(positiveMarksSpan).keydown(function(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if(charCode == 13 || (charCode >= 65 && charCode <= 90) || (charCode >= 106 && charCode <= 222)) {
      return false;
    }
  });
  $(marks).append("[");
  marks.appendChild(positiveMarksSpan);
  $(marks).append("]");
  $(marks).click(function(event) {
    placeCursorAtEnd(positiveMarksSpan);
    event.stopPropagation(); 
  });
  questionRow.appendChild(marks);

  questionTable.appendChild(questionRow);

  questionTable.appendChild(getHiddenInput(questionArray.questionId));
  questionTable.appendChild(getHiddenInput(questionArray.subject));
  questionTable.appendChild(getHiddenInput(questionArray.topic));
  questionTable.appendChild(getHiddenInput(questionArray.level));
  questionTable.appendChild(getHiddenInput("no"));

  selectedQuestionList.appendChild(questionTable);
  questionTable.onclick = function(event) { showPaperQuestionFunctions($(this)[0],event); event.stopPropagation(); };
  enableElementDraggable(questionTable);
}

function getHiddenInput(data) {
  var hiddenInput = document.createElement("INPUT");
  hiddenInput.type = "hidden";
  hiddenInput.value = data;
  return hiddenInput;
}

function handleGetQuestionList(jsonResponse) {
  $("#questionListDiv").show();
  var response = JSON.parse(jsonResponse);

  startPos = response.nextIndex;
  prevStartPos = response.prevIndex;

  for(var i=0;i < response.question.length; ++i) {

    var question = createQuestionElement(response.question[i],"li");
    simpleQuestionList.appendChild(question);
    if(i==0) { question.value = startPos-limit+1; }

    if( checkIfQuestionSelected(response.question[i].questionId) ) {
      inRemoveQuestionState(question);
    }
    else {
      inAddQuestionState(question);
    }

  }
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("searchSpace")]);
  populatePageTrack(startPos,response.totalQuestions);
  if(simpleQuestionList.children.length == 0) { $("#noResultsFound").show(); }
}

function populatePageTrack(questionNum,totalQuestions) {
  if(questionNum>limit) {
    $("#prevListLink").show();
  }
  if(questionNum<totalQuestions) {
    $("#nextListLink").show();
  }
  $("#hideLink").show();
}

function previousQuestionList() {
  $('html, body').animate({
    scrollTop: $("#searchStrDiv").offset().top
  },10);
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  $("#hideLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+prevStartPos;
  postData += "&limit="+limit;
  $("#questionListDiv").html("");
  if(!varEmptyOrNull(searchStr)) { postData += "&searchStr="+searchStr; }
  postRequest("questions/getquestionlist.php",postData,handleGetQuestionList,100);
}

function hideSearchResult() {
  $('html, body').animate({
    scrollTop: $("#searchStrDiv").offset().top
  },10);
  $("#searchQuestionListRow").hide();
  $("#questionListDiv").empty();
  $("#noResultsFound").hide();
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  $("#hideLink").hide();
}

function addToolImageToOrElement(or) {
  var floatingDiv = or.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolImage = document.createElement("IMG");
  toolImage.src = rootPath+"images/spanner.png";
  toolImage.style.width = "6.6125mm";
  $(toolImage).mouseenter(
    function() { 
      timeoutId = window.setTimeout(
        function() {
          timeoutId = null;
          addToolDivToOrElement(or);
        }, 150);
    });
  $(toolImage).mouseleave(function() { window.clearTimeout(timeoutId); });
  floatingDiv.appendChild(toolImage);
}

function createToolButtonForOrElement(text,customFunction) {
  var toolButton = document.createElement("button");
  toolButton.type = "button";
  toolButton.className = "floatingToolButton";
  $(toolButton).click( function(event) { customFunction(getGreatGrandParent($(this)[0])); event.stopPropagation(); });
  toolButton.innerHTML = text;
  return toolButton;
}

function addToolDivToOrElement(or) {
  var floatingDiv = or.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  var toolDiv = document.createElement("div");
  $(toolDiv).css("background-color","lightgrey");
  $(toolDiv).append(createToolButtonForOrElement("Remove",removeElement));
  $(toolDiv).append(createToolButtonForOrElement("Move Up",moveElementUp));
  $(toolDiv).append(createToolButtonForOrElement("Move Down",moveElementDown));
  $(toolDiv).mouseleave(function() { addToolImageToOrElement(or); });
  floatingDiv.appendChild(toolDiv);
}

function moveElementUp(element) {
  var parentElement = element.parentNode;
  var length = parentElement.children.length;
  if(length==1)
  {
    return;
  }
  var index = $(element).index();
  if(index==0) {
    parentElement.insertBefore(element,parentElement.children[length-1]);
    parentElement.insertBefore(parentElement.children[length-1],parentElement.children[length-2]);
  }
  else {
    parentElement.insertBefore(element,parentElement.children[index-1]);
  }
  updatePaperLogistics();
}

function moveElementDown(element) {
  var parentElement = element.parentNode;
  var length = parentElement.children.length;
  if(length==1) {
    return;
  }
  var index = $(element).index();
  if(index==length-1) {
    parentElement.insertBefore(element,parentElement.children[0]);
  }
  else {
    parentElement.insertBefore(parentElement.children[index+1],element);
  }
  updatePaperLogistics();
}

function removeElement(element) {
  element.parentNode.removeChild(element);
  updatePaperLogistics();
}

function createOrElement() {
  var or = document.createElement("div");
  or.className = "orCard";
  or.id = ++listIdCounter;
  enableElementDraggable(or);

  $(or).click(function(event) { showPaperElementFunctions($(this)[0],event); event.stopPropagation();});

  if($("#language").val() == "english") {
    $(or).append("<div style='text-align: center'><u><i>OR</i></u></div>");
  }
  else if($("#language").val() == "hindi")  {
    $(or).append("<div style='text-align: center'><u><i>&#x905;&#x925;&#x935;&#x93E;</i></u></div>");
  }

  return or;
}

function createLineElement() {
  var line = document.createElement("div");
  line.className = "lineCard";
  line.id = ++listIdCounter;
  enableElementDraggable(line);

  $(line).click(function(event) { showPaperElementFunctions($(this)[0],event); event.stopPropagation();});

  if($("#language").val() == "english") {
    $(line).append("<div style='text-align: center'>------ Blank Line ------</div>");
  }
  else if($("#language").val() == "hindi")  {
    $(line).append("<div style='text-align: center'>------ &#x905;&#x924;&#x93F;&#x930;&#x93F;&#x915;&#x94D;&#x924; &#x930;&#x947;&#x916;&#x93E; ------</div>");
  }

  return line;
}

function createPageBreakElement() {
  var pagebreak = document.createElement("div");
  pagebreak.className = "pagebreakCard";
  pagebreak.id = ++listIdCounter;
  enableElementDraggable(pagebreak);

  $(pagebreak).click(function(event) { showPaperElementFunctions($(this)[0],event); event.stopPropagation();});

  if($("#language").val() == "english") {
    $(pagebreak).append("<div style='text-align: center'>------ New Page ------</div>");
  }
  else if($("#language").val() == "hindi")  {
    $(pagebreak).append("<div style='text-align: center'>------ &#x928;&#x92F;&#x93E; &#x92A;&#x943;&#x937;&#x94D;&#x920; ------</div>");
  }

  return pagebreak;
}

function createSectionElement(sectionContent) {
  var section = document.createElement("div");
  section.className = "sectionCard prettybox multiLanguage";
  section.id = ++listIdCounter;
  enableElementDraggable(section);
  $(section).css("text-align","center");
  section.innerHTML = sectionContent;

  section.onclick = function(event) { showPaperSectionFunctions($(this)[0],event); event.stopPropagation();};

  return section;
}

function addToolImageSecond(question,positiveMarks) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";
  $(floatingDiv).click( function(event) { event.stopPropagation(); });
  $(floatingDiv).css("cursor","default");

  var positiveMarksSpan = document.createElement("span");
  positiveMarksSpan.className = "positiveMarksSpan";
  positiveMarksSpan.innerHTML = positiveMarks;

  var timeoutId;
  $(positiveMarksSpan).mouseenter(
    function() { 
      timeoutId = window.setTimeout(
        function() {
          timeoutId = null;
          addToolDivSecond(question,positiveMarksSpan.innerHTML); 
        }, 150);
    });
  $(positiveMarksSpan).mouseleave(function() { window.clearTimeout(timeoutId); });

  $(floatingDiv).append("[");
  floatingDiv.appendChild(positiveMarksSpan);
  $(floatingDiv).append("]");

  updateTotalMarks();
}

function addToolDivSecond(question,positiveMarks) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";

  var positiveMarksDiv = document.createElement("div");
  positiveMarksDiv.contentEditable = true;
  positiveMarksDiv.className = "positiveMarksSpan";
  $(positiveMarksDiv).css("text-align","center");
  $(positiveMarksDiv).css("cursor","text");
  positiveMarksDiv.innerHTML = positiveMarks;

  var toolDiv = document.createElement("div");
  $(toolDiv).css("background-color","lightgrey");
  $(toolDiv).append(positiveMarksDiv);
  $(toolDiv).append(createToolButton("Move Up",moveElementUp));
  $(toolDiv).append(createToolButton("Move Down",moveElementDown));
  $(toolDiv).append(createToolButton("Insert Or",insertOr));
  $(toolDiv).append(createToolButton("Insert Section",insertSection));
  $(toolDiv).append(createToolButton("Insert Page Break",insertPageBreak));
  $(toolDiv).append(createToolButton("Insert Line",insertLine));
  if(getQuestionIdFromCard(question) == 0) {
    $(toolDiv).append(createToolButton("Edit",editPaperQuestion));
  }
  $(toolDiv).mouseleave(function() { addToolImageSecond(question,positiveMarksDiv.innerHTML); });
  floatingDiv.appendChild(toolDiv);
}

function insertOr(question) {
  if(varEmptyOrNull(question)) {
    var orCard = createOrElement();
    selectedQuestionList.appendChild(orCard);
  }
  else {
    var orCard = createOrElement();
    $(orCard).insertAfter(question);
    updatePaperLogistics();
  }
}

function insertLine(question) {
  var lineCard = createLineElement();
  $(lineCard).insertAfter(question);
}

function insertPageBreak(question) {
  var pagebreakCard = createPageBreakElement();
  $(pagebreakCard).insertAfter(question);
}

function insertSection(question) {
  if(varEmptyOrNull(question)) {
    var sectionCard = createSectionElement($("#question").html());
    selectedQuestionList.appendChild(sectionCard);
    editPaperSection(sectionCard);
    event.stopPropagation();
  }
  else {
    var sectionCard = createSectionElement("");
    question.parentNode.insertBefore(sectionCard,question);
    editPaperSection(sectionCard);
  }
}

function editPaperSection(section) {
  section.contentEditable = "true";
  $(section).addClass("sectionEdit");
  $(section).addClass("editbox");
  section.onclick = function(event) { event.stopPropagation(); };
  $(section).focus();
  $("#toolbar")[0].onclick = function(event) { event.stopPropagation(); };
  $("#question").removeClass("formatbox");
}

$(window).click(function(event) {
  if($(".sectionEdit").length) {
    var section = $(".sectionEdit")[0];
    section.contentEditable = "false";
    $(section).removeClass("sectionEdit");
    $(section).removeClass("editbox");
    section.onclick = function(event) { showPaperSectionFunctions($(this)[0],event); event.stopPropagation(); };
    $("#toolbar")[0].onclick = function() {};
    $("#question").addClass("formatbox");
  }
});

function editPaperQuestion(questionTable) {
  var question = questionTable.getElementsByClassName("questionCard")[0];
  invertHandleOptionContainer(question);
  question.contentEditable = "true";
  question.className = "questionCard multiLanguage prettybox editbox formatbox";
  question.onpaste = function(event) { onFormatBoxPaste(event,this); /*renderMathMl(event,this);*/ };
  handleTabs(question);
  questionTable.className = "questionCardTable inEditMode";
  questionTable.onclick = function(event) { event.stopPropagation(); };
  questionTable.getElementsByTagName("INPUT")[4].value = "yes";
  $(question).focus();
  $("#toolbar")[0].onclick = function(event) { event.stopPropagation(); };
  $("#question").removeClass("formatbox");
}

$(window).click(function(event) {
  if($(".inEditMode").length) {
    var questionTable = $(".inEditMode")[0];
    var question = questionTable.getElementsByClassName("questionCard")[0];
    handleOptionContainer(question);
    question.contentEditable = "false";
    question.className = "questionCard prettybox";
    question.onpaste = function(event) { };
    questionTable.className = "questionCardTable";
    questionTable.onclick = function(event) { showPaperQuestionFunctions($(this)[0],event); event.stopPropagation(); }
    //event.stopPropagation();
    $("#toolbar")[0].onclick = function() {};
    $("#question").addClass("formatbox");
  }
});

function refreshQuestionList() {
  //if( $("#subject > option").length == 0 || $("#topic > option").length == 0 || $("#level > option").length == 0) {
  if( $("#subject > option").length == 0 || $("#topic > option").length == 0) {
    alert("One or more category fields has zero length");
    return;
  }
  $("#searchQuestionListRow").show();
  $("#questionListDiv").empty();
  selectedSubject = $('#subject option:selected').text();
  selectedTopic = $('#topic option:selected').text();
  //selectedLevel = $('#level').val();
  selectedLevel = $('#okay').html();
  searchStr = $('#searchStr').val();
  startPos = 0;
  $("#noResultsFound").hide();
  getQuestionList();
}

function getQuestionList() {
  $('html, body').animate({
    scrollTop: $("#searchStrDiv").offset().top
  },10);
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  $("#hideLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+startPos;
  postData += "&limit="+limit;
  $("#questionListDiv").empty();
  if(!varEmptyOrNull(searchStr)) { postData += "&searchStr="+searchStr; }
  postRequest("questions/getquestionlist.php",postData,handleGetQuestionList,100);
}

function isPaperValid() {

  initValidForm();

  checkTextDiv("#time","Time must be filled out");
  checkPositiveNumber("totalQuestions","totalQuestions","Questions");
  checkPositiveNumber("totalInstructions","totalInstructions","Instructions");

  for(var i=0;i<$("#selectedQuestionList .questionCardTable").length;++i) {
    var positiveMarks = $("#selectedQuestionList .questionCardTable")[i].getElementsByClassName("positiveMarksSpan")[0].innerHTML;
    if(!$.isNumeric(positiveMarks) || positiveMarks <= 0) {
      validForm = false;
      appendValidFormAlert("All questions should have positive integer in positive marks field");
    }
  }

  var totalElements = selectedQuestionList.children.length;
  for(var i=0;i<totalElements; ++i) {
    if($(selectedQuestionList.children[i]).hasClass("orCard")) {
      if(i==0
        || i==totalElements-1
        || !$(selectedQuestionList.children[i-1]).hasClass("questionCardTable")
        || !$(selectedQuestionList.children[i+1]).hasClass("questionCardTable") ) {
        validForm = false;
        appendValidFormAlert("The 'OR' element should have questions before and after it");
        break;
      }
    }
    else if($(selectedQuestionList.children[i]).hasClass("sectionCard")) {
      if(i==totalElements-1 || !$(selectedQuestionList.children[i+1]).hasClass("questionCardTable") ) {
        validForm = false;
        appendValidFormAlert("The 'Section' element should have atleast one question after it");
        break;
      }
    }
  }

  for(var i=0;i<$("#selectedQuestionListDiv .sectionCard").length;++i) {
    if(varEmptyOrNull($("#selectedQuestionListDiv .sectionCard")[i].innerHTML)) {
      validForm = false;
      appendValidFormAlert("'Section' element must be filled.");
      break;
    }
  }

  if(totalElements > 0 
    && ( $(selectedQuestionList.children[0]).hasClass("pagebreakCard")
       || $(selectedQuestionList.children[totalElements-1]).hasClass("pagebreakCard") ) ) {
    validForm = false;
    appendValidFormAlert("New Page cannot be the first or the last element in paper");
  }

  if(!isFormValid()) { return false; }

  return true;

}

function printRawPaper() {

  if(!isPaperValid()) { return; }

  removeMathJaxAndCallback(document.getElementById("paperSpace"),transferPaperForPrinting);
}

function transferPaperForPrinting() {
  var paper = {};
  paper.instituteName = $("#instituteName").val();
  paper.examType = $("#examType").val();
  paper.paperSubject = $("#paperSubject").val();
  //paper.paperLevel = $("#paperLevel").val();
  paper.paperLevel = $("#paperLevel").html();
  paper.language = $("#language").val();
  paper.pageSize = $("#pageSize").val();
  paper.paperSet = $("#paperSet").html();
  paper.time = $("#time").html();
  paper.totalMarks = $("#totalMarks").html();

  paper.instructions = [];
  for(var i=0;i<selectedInstructionList.children.length;++i) {
    paper.instructions.push(selectedInstructionList.children[i].getElementsByTagName("div")[0].innerHTML);
  }

  paper.question = [];
  for(var i=0;i<selectedQuestionList.children.length;++i) {
    var paperElement = {};
    if($(selectedQuestionList.children[i]).hasClass("sectionCard")) {
      paperElement.type = "section";
      paperElement.content = selectedQuestionList.children[i].innerHTML;
    }
    else if($(selectedQuestionList.children[i]).hasClass("orCard")) {
      var positiveMarks = selectedQuestionList.children[i+1].getElementsByClassName("positiveMarksSpan")[0].innerHTML;
      paperElement.type = "or";
      paperElement.positiveMarks = positiveMarks;
    }
    else if($(selectedQuestionList.children[i]).hasClass("questionCardTable")) {
      var positiveMarks = selectedQuestionList.children[i].getElementsByClassName("positiveMarksSpan")[0].innerHTML;
      paperElement.type = "question";
      paperElement.positiveMarks = positiveMarks;
      paperElement.question = selectedQuestionList.children[i].getElementsByClassName("questionCard")[0].innerHTML;
    }
    else if($(selectedQuestionList.children[i]).hasClass("pagebreakCard")) {
      paperElement.type = "pagebreak";
    }
    else if($(selectedQuestionList.children[i]).hasClass("lineCard")) {
      paperElement.type = "line";
    }
    else {
      alert("Unhandled element in selected question list");
    }
    paper.question.push(paperElement);
  }

  handlePrintPaper(JSON.stringify(paper));
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("paperSpace")]);
}

function getPostData() {
  var questions= {};
  for(var i=0;i<selectedQuestionList.children.length;++i) {
    var paperElement = {};
    if($(selectedQuestionList.children[i]).hasClass("sectionCard")) {
      paperElement.type = "section";
      paperElement.section = selectedQuestionList.children[i].innerHTML;
    }
    else if($(selectedQuestionList.children[i]).hasClass("orCard")) {
      var positiveMarks = selectedQuestionList.children[i+1].getElementsByClassName("positiveMarksSpan")[0].innerHTML;
      paperElement.type = "or";
      paperElement.positiveMarks = positiveMarks;
    }
    else if($(selectedQuestionList.children[i]).hasClass("questionCardTable")) {
      var questionTable = selectedQuestionList.children[i];
      var positiveMarks = questionTable.getElementsByClassName("positiveMarksSpan")[0].innerHTML;
      paperElement.type = "question";
      paperElement.questionId = questionTable.getElementsByTagName("INPUT")[0].value;
      paperElement.positiveMarks = positiveMarks;
      paperElement.question = questionTable.getElementsByClassName("questionCard")[0].innerHTML;
      paperElement.subject = questionTable.getElementsByTagName("INPUT")[1].value;
      paperElement.topic = questionTable.getElementsByTagName("INPUT")[2].value;
      paperElement.level = questionTable.getElementsByTagName("INPUT")[3].value;
      paperElement.modified = questionTable.getElementsByTagName("INPUT")[4].value;
    }
    else if($(selectedQuestionList.children[i]).hasClass("pagebreakCard")) {
      paperElement.type = "pagebreak";
    }
    else if($(selectedQuestionList.children[i]).hasClass("lineCard")) {
      paperElement.type = "line";
    }
    else {
      alert("Unhandled element in selected question list");
    }
    questions[i] = paperElement;
  }

  var jsonQuestionsArray = encodeURIComponent(JSON.stringify(questions));

  var jsonInstructionsArray = encodeURIComponent(getInstructionsArray());

  var postData = "instituteName="+$('#instituteName').val();
  //postData += "&examType="+$("#examType").val()+"&paperSubject="+$("#paperSubject").val()+"&paperLevel="+$("#paperLevel").val();
  postData += "&examType="+$("#examType").val()+"&paperSubject="+$("#paperSubject").val()+"&paperLevel="+$("#paperLevel").html();
  postData += "&time="+$('#time').html();
  postData += "&questions="+jsonQuestionsArray+"&instructions="+jsonInstructionsArray;
  postData += "&language="+$("#language").val();
  postData += "&pageSize="+$("#pageSize").val();
  postData += "&paperSet="+$("#paperSet").html();

  return postData;
}

function updateLanguage() {
  if($("#language").val() == "english") {
    $("#rollNumLabel").html("Roll No. ...............");
    $("#paperSetLabel").html("Set - ");
    $("#timeLabel").html("Time:");
    $("#totalMarksLabel").html("Total Marks:");
    $("#paperName").attr("placeholder","Paper Name");
    $("#instructionHeading").html("Instructions");
    $("#questionHeading").html("Questions");
    $(".orCard > div:odd").each(function() { this.innerHTML = "<u><i>OR</i></u>"; });
    disableTransliteration();
  }
  else if($("#language").val() == "hindi") {
    $("#rollNumLabel").html("&#x930;&#x94B;&#x932; &#x928;&#x902; ...............");
    $("#paperSetLabel").html("&#x938;&#x947;&#x91F; - ");
    $("#timeLabel").html("&#x938;&#x92E;&#x92F;:");
    $("#totalMarksLabel").html("&#x92A;&#x942;&#x930;&#x94D;&#x923;&#x93E;&#x902;&#x915;:");
    var tempDiv = document.createElement("div");
    $(tempDiv).html("&#x92A;&#x930;&#x940;&#x915;&#x94D;&#x937;&#x93E; &#x915;&#x93E; &#x928;&#x93E;&#x92E;");
    $("#paperName").attr("placeholder",$(tempDiv).html());
    $("#instructionHeading").html("&#x928;&#x93F;&#x930;&#x94D;&#x926;&#x947;&#x936;");
    //if(isFontAvailable('DevLys 010')) {
    if(isFontAvailable('DLys 010')) {
      //$("#questionHeading")[0].style.fontFamily = 'DevLys 010';
      $("#questionHeading")[0].style.fontFamily = 'kruti';
      //$("#questionHeading")[0].style.fontFamily = 'Impact';
      $("#questionHeading").html("iz'u");
    }
    else {
      $("#questionHeading").html("&#x92A;&#x94D;&#x930;&#x936;&#x94D;&#x928;");
    }
    //$("#questionHeading").html("&#x92A;&#x94D;&#x930;&#x936;&#x94D;&#x928;");
    $(".orCard > div:odd").each(function() { this.innerHTML = "<u><i>&#x905;&#x925;&#x935;&#x93E;</i></u>"; });
    enableTransliteration();
  }
}

function populatePaperSubjectDataList() {
  var prevSubject="";
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#paperSubjectDataList").append("<option value='" + subjectTopicCategory[i].subject + "'/>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

/*function populatePaperLevelDataList() {
  for(var i=0; i<levelCategory.length; ++i) {
    $("#paperLevelDataList").append("<option value='" + levelCategory[i].level + "'/>");
  }
}*/

/*function populateSubjectDataList() {
  var prevSubject="";
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#subjectDataList").append("<option value='" + subjectTopicCategory[i].subject + "'/>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

function populateTopicDataList() {
  $("#topicDataList").empty();
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject == $("#inputSubject").val()) {
      $("#topicDataList").append("<option value='" + subjectTopicCategory[i].topic + "'/>");
    }
  }
}

function populateLevelDataList() {
  for(var i=0; i<levelCategory.length; ++i) {
    $("#levelDataList").append("<option value='" + levelCategory[i].level + "'/>");
  }
}*/

function populateSelectSubject() {
  var prevSubject="";
  $("#subject").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#subject").append("<option>" + subjectTopicCategory[i].subject + "</option>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

function populateSelectTopic() {
  $("#topic").empty();
  $("#topic").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject == $("#subject option:selected").text()) {
      $("#topic").append("<option>" + subjectTopicCategory[i].topic + "</option>");
    }
  }
}

/*function populateSelectLevel() {
  $("#level").append("<option>ALL</option");
  for(var i=0; i<levelCategory.length; ++i) {
    $("#level").append("<option>" + levelCategory[i].level + "</option>");
  }
}*/

function populateCategory(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  subjectTopicCategory = response.subjectTopic;
  levelCategory = response.level;
  populatePaperSubjectDataList();
  //populatePaperLevelDataList();
  /*populateSubjectDataList();
  populateLevelDataList();*/
  populateSelectSubject();
  populateSelectTopic();
  populateSelectLevel();
}

function populateInstituteAndExamList(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  for(var i=0; i<response.institute.length; ++i) {
    $("#instituteNameDataList").append("<option value='" + response.institute[i] + "'/>");
  }
  for(var i=0; i<response.exam.length; ++i) {
    $("#examTypeDataList").append("<option value='" + response.exam[i] + "'/>");
  }
}

function submitQuestion(){

  initValidForm();

  checkTextDiv("#question","Question must be filled out");

  if(!isFormValid()) {
    return;
  }

  removeMathJaxAndCallback(document.getElementById("question"),transferQuestionFromEditBoxToSelectedQuestionList);

  /*var questionArray = {};
  questionArray["question"] = $("#question").html();
  questionArray["subject"] = "";
  questionArray["topic"] = "";
  questionArray["level"] = "";
  questionArray["questionId"] = 0;
  questionArray["imageUrl"] = "";
 
  appendQuestionToSelectedQuestionList(questionArray,$("#submitQuestionMarks").val());
  updatePaperLogistics();

  $("#question").html("");*/
}

function submitOrWithQuestion(){

  initValidForm();

  checkTextDiv("#question","Question must be filled out");

  if(!isFormValid()) {
    return;
  }

  insertOr();

  removeMathJaxAndCallback(document.getElementById("question"),transferQuestionFromEditBoxToSelectedQuestionList);

  /*var questionArray = {};
  questionArray["question"] = $("#question").html();
  questionArray["subject"] = "";
  questionArray["topic"] = "";
  questionArray["level"] = "";
  questionArray["questionId"] = 0;
  questionArray["imageUrl"] = "";
 
  appendQuestionToSelectedQuestionList(questionArray,$("#submitQuestionMarks").val());
  updatePaperLogistics();

  $("#question").html("");*/
}

function transferQuestionFromEditBoxToSelectedQuestionList() {
  var questionArray = {};
  questionArray["question"] = $("#question").html();
  //console.log(questionArray["question"]);
  questionArray["subject"] = "";
  questionArray["topic"] = "";
  questionArray["level"] = "";
  questionArray["questionId"] = 0;
  questionArray["imageUrl"] = "";
 
  appendQuestionToSelectedQuestionList(questionArray,$("#submitQuestionMarks").val());
  updatePaperLogistics();

  $("#question").html("");
  $("#question").focus();
  window.scrollTo(0,document.body.scrollHeight);
}

function removeMathJaxAndCallback(containerElement,customFunction) {
  var mathJax = MathJax.Hub.getAllJax(containerElement);
  for(i=0; i<mathJax.length; ++i) {
    if(i==mathJax.length-1 && customFunction) {
      MathJax.Hub.Queue(["Remove",mathJax[i]],[customFunction]);
    }
    else {
      MathJax.Hub.Queue(["Remove",mathJax[i]]);
    }
  }
  if(mathJax.length==0) {
    customFunction();
  }
}

function enableElementDraggable(element) {
  element.draggable = "true";
  element.ondragstart = function(event) { drag(event,$(this)[0]); };
  element.ondrop = function(event) { drop(event,element); };
  element.ondragover = function(event) { allowDrop(event,element); };
  element.ondragleave = function(event) { dragLeaving(event,element); };
}

function dragLeaving(event,element) {
  element.style.border= "none";
}

function allowDrop(ev,element) {
  ev.preventDefault();
  element.style.borderTop = "0.2645mm solid black";
  $(element).hover(function(event) { $(this).css("border","0.2645mm solid black"); },
    function() { $(this).css("border","none"); });
}

function drag(ev,element) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev,element) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  var parentElement = element.parentNode;
  parentElement.insertBefore(document.getElementById(data),element);
  element.style.border = "none";
  document.getElementById(data).style.border = "none";
  $(element).hover(function() { $(this).css("border","0.2645mm solid black"); },
    function() { $(this).css("border","none"); });
  $("#"+data).hover(function(event) { $(this).css("border","0.2645mm solid black"); },
    function() { $(this).css("border","none"); });
  updatePaperLogistics();
}

function addPositiveMarksSpan(question,positiveMarks) {
  var floatingDiv = question.getElementsByClassName("floatingDiv")[0];
  floatingDiv.innerHTML = "";

  var positiveMarksSpan = document.createElement("span");
  positiveMarksSpan.contentEditable = true;
  positiveMarksSpan.className = "positiveMarksSpan";
  if(varEmptyOrNull(positiveMarks)) {
    positiveMarksSpan.innerHTML = "4";
  }
  else {
    positiveMarksSpan.innerHTML = positiveMarks;
  }
  positiveMarksSpan.onblur = function() { updateTotalMarks() };

  $(floatingDiv).append("[");
  floatingDiv.appendChild(positiveMarksSpan);
  $(floatingDiv).append("]");
  $(floatingDiv).click(function(event) { event.stopPropagation(); });
}

function showPaperQuestionFunctions(question,event) {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  var functionDiv = document.createElement("div");
  functionDiv.id = "functionDiv";
  var x = event.clientX;
  var y = event.clientY;
  $(functionDiv).css("background-color","lightgrey");
  $(functionDiv).css({
    position: 'fixed',
    top: y,
    left: x,
    zIndex: '1000'
  });
  $(functionDiv).append(createPaperFunctionButton("Edit",editPaperQuestion,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Insert Or",insertOr,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Insert Section",insertSection,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Insert Page Break",insertPageBreak,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Insert Line",insertLine,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Up",moveElementUp,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Down",moveElementDown,question,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Remove",removeQuestionFromSelectedQuestionList,question,functionDiv));
  var transparentCover = createTransparentCover();
  $('body').append(transparentCover);
  $('body').append(functionDiv);
}

function createTransparentCover() {
  var transparentCover = document.createElement("div");
  transparentCover.id = "transparentCover";
  $(transparentCover).css("background-color","rgba(1,1,1,0)");
  $(transparentCover).css({
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: '999'
  });
  $(transparentCover).click(function(event) {
    if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
    $(this).remove();
    event.stopPropagation();
  });
  return transparentCover;
}

function showPaperElementFunctions(element,event) {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  var functionDiv = document.createElement("div");
  functionDiv.id = "functionDiv";
  var x = event.clientX;
  var y = event.clientY;
  $(functionDiv).css("background-color","lightgrey");
  $(functionDiv).css({
    position: 'fixed',
    top: y,
    left: x,
    zIndex: '1000'
  });
  $(functionDiv).append(createPaperFunctionButton("Remove",removeElement,element,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Up",moveElementUp,element,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Down",moveElementDown,element,functionDiv));
  var transparentCover = createTransparentCover();
  $('body').append(transparentCover);
  $('body').append(functionDiv);
}

function showPaperSectionFunctions(element,event) {
  if($('#functionDiv').length > 0) { $('#functionDiv').remove(); }
  var functionDiv = document.createElement("div");
  functionDiv.id = "functionDiv";
  var x = event.clientX;
  var y = event.clientY;
  $(functionDiv).css("background-color","lightgrey");
  $(functionDiv).css({
    position: 'fixed',
    top: y,
    left: x,
    zIndex: '1000'
  });
  $(functionDiv).append(createPaperFunctionButton("Edit",editPaperSection,element,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Up",moveElementUp,element,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Move Down",moveElementDown,element,functionDiv));
  $(functionDiv).append(createPaperFunctionButton("Remove",removeElement,element,functionDiv));
  var transparentCover = createTransparentCover();
  $('body').append(transparentCover);
  $('body').append(functionDiv);
}

/*$(window).click(function() {
  if($('#functionDiv').length > 0) {
    $('#functionDiv').remove();
  }
});*/

$(function() {
  $("#language").change(function() {
    updateLanguage();
  });
  $("#filter").click(function() {
    $('html, body').animate({
      scrollTop: $("#searchStrDiv").offset().top
    },10);
    $('.selectDiv').toggle();
    if(this.innerHTML == "Show Filter") {
      this.innerHTML = "Hide Filter";
    }
    else if(this.innerHTML == "Hide Filter") {
      this.innerHTML = "Show Filter";
      $("#subject")[0].selectedIndex = 0;
      $("#topic")[0].selectedIndex = 0;
      //$("#level")[0].selectedIndex = 0;
      $("#okay").html("ALL");
    }
  });
  $(window).keydown(function(evt) {
    var e = event || evt;
    if(e.ctrlKey) {
      if(e.keyCode == 104 || e.keyCode == 72) {
        if($("#language").val() == "english") {
          $("#language").val("hindi");
        }
        else {
          $("#language").val("english");
        }
        updateLanguage();
        return false;
      }
    }
  });
  getRequest("categories/getallcategories.php",populateCategory);
  getRequest("paper/getInstituteExamList.php",populateInstituteAndExamList);
  $("#question").keydown(function(evt) {
    var e = event || evt;
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) {
      window.scrollTo(0,document.body.scrollHeight);
    }
  });
  $("#toolbar").append('<button type="button" onclick="toggleSpace(this);">Search</button>');
  $(".formatbox").on('paste',function(e) {
    onFormatBoxPaste(e,this);
    //renderMathMl(e,this);
  });
  handleTabs(document.getElementById("question"));
});

function onFormatBoxPaste(e,variable) {
  //renderMathMl(e,variable);
  renderImage(e,variable);
}

/*function renderMathMl(e,variable) {
  var text = (e.originalEvent || e).clipboardData.getData('text');// || prompt('Paste something..');
  if(!varEmptyOrNull(text) && text.search("</math>") != -1) {
    text = text.replace("<math display='block'","<math");
    e.preventDefault();
    console.log(text);
    pasteHtmlAtCaret(text,false);
    //$(variable).append(text);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
  }
}*/

function renderImage(e,variable) {
	//if(typeof e.originalEvent.clipboardData != 'undefined') {
		var copiedData = (e.originalEvent || e).clipboardData.items[0]; //Get the clipboard data

		/*If the clipboard data is of type image, read the data*/
		if(copiedData && copiedData.type.indexOf("image") == 0) {
			var imageFile = copiedData.getAsFile(); 

			/*We will use HTML5 FileReader API to read the image file*/
			var reader = new FileReader();
			
			reader.onload = function (evt) {
				var result = evt.target.result; //base64 encoded image

				/*Create an image element and append it to the content editable div*/
				var img = document.createElement("img");
        img.className = "resizeImage";
        img.style.cssText = "vertical-align:middle; padding: 0mm; margin: 0mm;";
				img.src = result;
        pasteHtmlAtCaret(img.outerHTML,false);
        resizeImage(img);
      };

      /*Read the image file*/
      reader.readAsDataURL(imageFile);
      //console.log(copiedData.type);
		}
    else if(copiedData && copiedData.type.indexOf("text") == 0) {
      var text = (e.originalEvent || e).clipboardData.getData('text');// || prompt('Paste something..');
      if(!varEmptyOrNull(text) && text.indexOf("<math") == 0 && text.length-text.lastIndexOf("</math>") == 7) {
        e.preventDefault();
        pasteHtmlAtCaret("<span contenteditable='false'>"+text+"</span> ",false);
        MathJax.Hub.Queue(["Typeset",MathJax.Hub,variable]);
      }
    }
    //console.log(copiedData.type);
	//}
}

function handleOkayClick() {
  console.log($('#question').html());
}

function resizeImage(img) {
  //$(img).addClass("resizeImage");
  YUI().use('resize', function(Y) {
    resizeImageVar = new Y.Resize({
      //Selector of the node to resize
      node: '.resizeImage'
    });
    resizeImageVar.plug(Y.Plugin.ResizeConstrained, {
      constrain: '#question',
      //minHeight: 50,
      //minWidth: 50
    });
  });
}

/*$(".resizeImage").click(function(event) {
  event.stopPropagation();
});*/

$(window).click(function(event) {
  if($(".resizeImage").length) {
    var image = $(".resizeImage")[0];
    resizeImageVar.destroy();
    $(image).removeClass("resizeImage");
    image.onclick = function(event) { this.className="resizeImage"; resizeImage($(this)[0],event); event.stopPropagation(); };
  }
});

function toggleSpace(button) {
  if(button.innerHTML == "Search") {
    $("#paperSpace").hide();
    $("#searchSpace").show();
    $("#searchStr").focus();
    button.innerHTML = "Back To Paper";
  }
  else if(button.innerHTML == "Back To Paper") {
    /*$("#questionListDiv").empty();
    $("#noResultsFound").hide();
    $("#prevListLink").hide();
    $("#nextListLink").hide();
    $("#hideLink").hide();*/
    $("#searchSpace").hide();
    $("#paperSpace").show();
    button.innerHTML = "Search";
  }
}

$(window).scroll(function() { 
  var window_top = $(window).scrollTop();
  var div_top = $("#dummyTop").offset().top;
  if (window_top > div_top) {
    $('#toolbar').css({
      position: 'fixed',
      top: '0',
      width: '100%',
    });
    $('#dummyTop').height($('#toolbar').outerHeight());
  } else {
    $('#toolbar').css({
      position: 'static'
    });
    $('#dummyTop').height(0);
  }
});

function initPaperFields(jsonResponse) {
  $('#loadingMessage').hide();
  $('#paperSpace').show();
  var response = JSON.parse(jsonResponse);

  if(!varEmptyOrNull(response.heading)) {
    $("#instituteName").val(response.heading);
  }
  else {
    $("#instituteName").val(response.instituteName);
    $("#examType").val(response.examType);
    $("#paperSubject").val(response.paperSubject);
    //$("#paperLevel").val(response.paperLevel);
    $("#paperLevel").html(response.paperLevel);
  }
  $("#time").html(response.time);
  $("#language").val(response.language);
  $("#pageSize").val(response.pageSize);
  if(!varEmptyOrNull(response.paperSet)) {
    $("#paperSet").html(response.paperSet);
  }

  for(var i=0; i<response.question.length; ++i) {
    if(response.question[i].type == "section") {
      var section = createSectionElement(response.question[i].content);
      selectedQuestionList.appendChild(section);
    }
    else if(response.question[i].type == "or") {
      var or = createOrElement();
      selectedQuestionList.appendChild(or);
    }
    else if(response.question[i].type == "question") {
      appendQuestionToSelectedQuestionList(response.question[i],response.question[i].positiveMarks);
    }
    else if(response.question[i].type == "pagebreak") {
      var pagebreak = createPageBreakElement();
      selectedQuestionList.appendChild(pagebreak);
    }
    else if(response.question[i].type == "line") {
      var line = createPageBreakElement();
      selectedQuestionList.appendChild(line);
    }
    else {
      alert("Unhandled Paper element type");
    }
  }
  updatePaperLogistics();

  for(var i=0; i<response.instructionsId.length; ++i) {
    addInstructionToSelectedList(response.instructionsId[i],response.instructions[i]);
    removeInstructionFromSimpleList(response.instructionsId);
  }
  updateTotalInstructions();

  updateLanguage();
  //MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("paperSpace")]);
}

window.onbeforeunload = function(evt) {
  //alert("Okay");
  if(false) {
    return false;
  }
}

/*window.onbeforeunload = function(evt) {
    var message = 'Did you remember to download your form?';
    if (typeof evt == 'undefined') {
        evt = window.event;
      alert("one");
    }
    if (evt) {
        evt.returnValue = message;
      alert("two");
    }

    return true;
}*/
