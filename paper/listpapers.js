var paperList;

var elementToView;
var limit=10;
var startPos=0;
var prevStartPos=0;
var stopDownloading=false;

var subjectTopicCategory;
var levelCategory;

function initListPapers() {
  paperList = document.getElementById("paperListDiv");
  paperList.className = REFRESH_PAPER_LIST;
}

$(window).scroll(function (event) {
  if(!varEmptyOrNull(elementToView) && !stopDownloading) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elementToView).offset().top;
    var elemBottom = elemTop + $(elementToView).height();

    if(((elemBottom <= docViewBottom) && (elemTop >= docViewTop)))
    {
      elementToView = null;
      getPaperList();
    }
  }
});

function refreshPaperList() {
  //if( $("#subject > option").length == 0 || $("#topic > option").length == 0 || $("#level > option").length == 0) {
  if( $("#subject > option").length == 0 || $("#topic > option").length == 0) {
    alert("One or more category fields has zero length");
    return;
  }
  $("#paperListDiv").empty();
  selectedSubject = $('#subject option:selected').text();
  selectedTopic = $('#topic option:selected').text();
  //selectedLevel = $('#level').val();
  selectedLevel = $('#okay').html();
  stopDownloading = false;
  startPos = 0;
  $("#noResultsFound").hide();
  getPaperList();
}

function getPaperList() {
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+startPos;
  postData += "&limit="+limit;
  $("#paperListDiv").empty();
  postRequest("paper/getpaperlist.php",postData,handleGetPaperList,100);
}

function handleGetPaperList(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  startPos = response.nextIndex;
  prevStartPos = response.prevIndex;

  for(var i=0;i < response.paper.length; ++i) {

    var paper = createPaperElement(response.paper[i]);
    paperList.appendChild(paper);

  }
  populatePageTrack(startPos,response.totalPapers);
  if(paperList.children.length == 0) { $("#noResultsFound").show(); }
}

function populatePageTrack(paperNum,totalPapers) {
  if(paperNum>limit) {
    $("#prevListLink").show();
  }
  if(paperNum<totalPapers) {
    $("#nextListLink").show();
  }
}

function previousPaperList() {
  $('html, body').animate({
    scrollTop: $(".selectDiv").offset().top
  },10);
  $("#prevListLink").hide();
  $("#nextListLink").hide();
  var postData = "subject="+selectedSubject;
  postData += "&topic="+selectedTopic;
  postData += "&level="+selectedLevel;
  postData += "&startPos="+prevStartPos;
  postData += "&limit="+limit;
  $("#paperListDiv").empty();
  postRequest("paper/getpaperlist.php",postData,handleGetPaperList,100);
}

function populateSelectSubject() {
  var prevSubject="";
  $("#subject").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject != prevSubject) {
      $("#subject").append("<option>" + subjectTopicCategory[i].subject + "</option>");
      prevSubject = subjectTopicCategory[i].subject;
    }
  }
}

function populateSelectTopic() {
  $("#topic").empty();
  $("#topic").append("<option>ALL</option");
  for(var i=0; i<subjectTopicCategory.length; ++i) {
    if(subjectTopicCategory[i].subject == $("#subject option:selected").text()) {
      $("#topic").append("<option>" + subjectTopicCategory[i].topic + "</option>");
    }
  }
}

/*function populateSelectLevel() {
  $("#level").append("<option>ALL</option");
  for(var i=0; i<levelCategory.length; ++i) {
    $("#level").append("<option>" + levelCategory[i].level + "</option>");
  }
}*/

function populateCategoryInListQuestions(jsonResponse) {
  var response = JSON.parse(jsonResponse);
  subjectTopicCategory = response.subjectTopic;
  levelCategory = response.level;
  populateSelectSubject();
  populateSelectTopic();
  populateSelectLevel();
}

$(function() {
  getRequest("categories/getallcategories.php",populateCategoryInListQuestions);
});

