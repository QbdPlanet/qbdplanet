function getQuestionDivA4(question) {
  var questionDiv = document.createElement("div");

  var statement = document.createElement("div");
  statement.className = "questionStatement prettybox";
  statement.innerHTML = question.question;
  for(var j=0; j<statement.getElementsByClassName("optionContainer").length; ++j) {
    var element = statement.getElementsByClassName("optionContainer")[j];
    var maxChar = 0;
    for(var k=0; k<element.getElementsByTagName("td").length; ++k) {
      var cellElement = element.getElementsByTagName("td")[k];
      if(maxChar < $(cellElement).text().length) { maxChar = $(cellElement).text().length; }
    }
    if(maxChar > 35) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
    else if(maxChar > 20) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
  }
  questionDiv.appendChild(statement);

  // Handle Image
  if( !varEmptyOrNull(question.imageUrl) ) {
    var image = document.createElement("IMG");
    image.className = "questionImage";
    questionDiv.appendChild(image);
    image.src = rootPath+question.imageUrl;
    image.style.cssText = "margin: 5.29mm; max-width: 132.25mm";
  }

  return questionDiv;
}

function getOrDivA4(language) {
  var secondColumn = document.createElement("div");
  $(secondColumn).css("vertical-align","top");
  $(secondColumn).css("text-align","center");
  $(secondColumn).css("font-size","4.5mm");
  if(language == "english") {
    secondColumn.innerHTML = "<u><i>OR</i></u>";
  }
  else if(language == "hindi") {
    secondColumn.innerHTML = "<u><i>&#x905;&#x925;&#x935;&#x93E;</i></u>";
  }
  return secondColumn;
}

function getQuestionDivA5(question) {
  var questionDiv = document.createElement("div");

  var statement = document.createElement("div");
  statement.className = "questionStatement prettybox";
  statement.innerHTML = question.question;
  for(var j=0; j<statement.getElementsByClassName("optionContainer").length; ++j) {
    var element = statement.getElementsByClassName("optionContainer")[j];
    var maxChar = 0;
    for(var k=0; k<element.getElementsByTagName("td").length; ++k) {
      var cellElement = element.getElementsByTagName("td")[k];
      if(maxChar < $(cellElement).text().length) { maxChar = $(cellElement).text().length; }
    }
    if(maxChar > 30) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
    else if(maxChar > 15) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
  }
  questionDiv.appendChild(statement);

  // Handle Image
  if( !varEmptyOrNull(question.imageUrl) ) {
    var image = document.createElement("IMG");
    image.className = "questionImage";
    questionDiv.appendChild(image);
    image.src = rootPath+question.imageUrl;
    image.style.cssText = "margin: 5.29mm; max-width: 132.25mm";
  }
  return questionDiv;
}

function getOrDivA5(language) {
  var secondColumn = document.createElement("div");
  $(secondColumn).css("vertical-align","top");
  $(secondColumn).css("text-align","center");
  $(secondColumn).css("font-size","5mm");
  if(language == "english") {
    secondColumn.innerHTML = "<u><i>OR</i></u>";
  }
  else if(language == "hindi") {
    secondColumn.innerHTML = "<u><i>&#x905;&#x925;&#x935;&#x93E;</i></u>";
  }
  return secondColumn;
}

function handleOptionContainer(statement) {
  for(var j=0; j<statement.getElementsByClassName("optionContainer").length; ++j) {
    var element = statement.getElementsByClassName("optionContainer")[j];
    var maxChar = 0;
    for(var k=0; k<element.getElementsByTagName("td").length; ++k) {
      var cellElement = element.getElementsByTagName("td")[k];
      if(maxChar < $(cellElement).text().length) { maxChar = $(cellElement).text().length; }
    }
    if(maxChar > 30) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
    else if(maxChar > 15) {
      var newElement = document.createElement("table");
      newElement.className = "optionContainer";
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[0].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[1].innerHTML + "</td></tr>");
      $(newElement).append("<tr><td>" + element.getElementsByTagName("td")[2].innerHTML + "</td><td>"
                           + element.getElementsByTagName("td")[3].innerHTML + "</td></tr>");
      element.parentNode.replaceChild(newElement,element);
    }
  }
}

function handlePrintPaperA4(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  var totalPrintContent = document.createElement("div");

  if(!varEmptyOrNull(response.paperSet)) {
    var rollNumAndPaperSetTable = document.createElement("table");
    $(rollNumAndPaperSetTable).css("width","100%");

    var rollNumAndPaperSetRow  = document.createElement("tr");

    var rollNum = document.createElement("div");
    rollNum.style.cssText = "font-size: 4.3mm; display: inline-block;";
    $(rollNum).attr("align","left");
    if(response.language == "english") {
      rollNum.innerHTML = "Roll No. ...............";
    }
    else if(response.language == "hindi") {
      rollNum.innerHTML = "&#x930;&#x94B;&#x932; &#x928;&#x902; ...............";
    }
    var rollNumColumn = document.createElement("td");
    $(rollNumColumn).attr("align","left");
    rollNumColumn.appendChild(rollNum);
    rollNumAndPaperSetRow.appendChild(rollNumColumn);

    var paperSet = document.createElement("div");
    paperSet.style.cssText = "font-size: 4.3mm; display: inline-block;";
    $(paperSet).attr("align","right");
    if(response.language == "english") {
      paperSet.innerHTML = "Set - " + (response.paperSet);
    }
    else if(response.language == "hindi") {
      paperSet.innerHTML = "&#x938;&#x947;&#x91F; - " + (response.paperSet);
    }
    var paperSetColumn = document.createElement("td");
    $(paperSetColumn).attr("align","right");
    paperSetColumn.appendChild(paperSet);
    rollNumAndPaperSetRow.appendChild(paperSetColumn);

    rollNumAndPaperSetTable.appendChild(rollNumAndPaperSetRow);
    totalPrintContent.appendChild(rollNumAndPaperSetTable);
  }

  if(!varEmptyOrNull(response.paperHeading)) {
    var paperHeading = document.createElement("div");
    paperHeading.className = "prettybox";
    paperHeading.style.cssText = "font-size: 6.4mm";
    $(paperHeading).attr("align","center");
    paperHeading.innerHTML = "<b>" + response.paperHeading + "</b>";
    totalPrintContent.appendChild(paperHeading);
  }

  /*var paperHeading = document.createElement("div");
  paperHeading.id = "paperHeading";
  paperHeading.className = "prettybox";
  paperHeading.style.cssText = "font-size: 6.4mm";
  //if(isFontAvailable('Bookman Old Style')) { paperHeading.style.fontFamily = "Bookman Old Style"; }
  $(paperHeading).attr("align","center");
  var linebreak = false;
  paperHeading.innerHTML = "";
  if(!varEmptyOrNull(response.instituteName)) {
    paperHeading.innerHTML += "<b>" + response.instituteName + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.examType)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.examType + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.paperSubject)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.paperSubject + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.paperLevel)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.paperLevel + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.heading)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.heading + "</b>";
  }
  if(!varEmptyOrNull(paperHeading.innerHTML)) {
    totalPrintContent.appendChild(paperHeading);
  }*/

  var timeAndMarksTable = document.createElement("table");
  $(timeAndMarksTable).css("width","100%");

  var timeAndMarksRow  = document.createElement("tr");

  var time= document.createElement("div");
  time.style.cssText = "font-size: 4.3mm; display: inline-block;";
  //if(isFontAvailable('Bookman Old Style')) { time.style.fontFamily = "Bookman Old Style"; }
  $(time).attr("align","left");
  if(response.language == "english") {
    time.innerHTML = "Time: " + response.time;
  }
  else if(response.language == "hindi") {
    time.innerHTML = "&#2360;&#2350;&#2351;: " + response.time;
  }
  var timeColumn = document.createElement("td");
  $(timeColumn).attr("align","left");
  timeColumn.appendChild(time);
  timeAndMarksRow.appendChild(timeColumn);

  var totalMarks = document.createElement("div");
  totalMarks.style.cssText = "font-size: 4.3mm; display: inline-block;";
  //if(isFontAvailable('Bookman Old Style')) { totalMarks.style.fontFamily = "Bookman Old Style"; }
  $(totalMarks).attr("align","right");
  if(response.language == "english") {
    totalMarks.innerHTML = "Total Marks: " + (response.totalMarks);
  }
  else if(response.language == "hindi") {
    totalMarks.innerHTML = "&#2346;&#2370;&#2352;&#2381;&#2339;&#2366;&#2306;&#2325;: " + (response.totalMarks);
  }
  var totalMarksColumn = document.createElement("td");
  $(totalMarksColumn).attr("align","right");
  totalMarksColumn.appendChild(totalMarks);
  timeAndMarksRow.appendChild(totalMarksColumn);

  timeAndMarksTable.appendChild(timeAndMarksRow);
  totalPrintContent.appendChild(timeAndMarksTable);

  if(response.instructions.length != 0) {
    var instructionsHeading = document.createElement("div");
    instructionsHeading.className = "webfont";
    //if(isFontAvailable('Bookman Old Style')) { instructionsHeading.style.fontFamily = "Bookman Old Style"; }
    if(response.language == "english") {
      instructionsHeading.innerHTML = "Instructions";
    }
    else if(response.language == "hindi") {
      instructionsHeading.innerHTML = "&#2344;&#2367;&#2352;&#2381;&#2342;&#2375;&#2358;";
    }
    instructionsHeading.style.cssText = "margin-bottom: 0mm; font-size: 5.5mm;";

    $(instructionsHeading).attr("align","center");
    totalPrintContent.appendChild(instructionsHeading);

    var instructionsList= document.createElement("ol");
    instructionsList.className = "webfont";
    //if(isFontAvailable('Bookman Old Style')) { instructionsList.style.fontFamily = "Bookman Old Style"; }
    for(var i=0;i<response.instructions.length;++i) {
      var instruction = document.createElement("li");
      instruction.style.cssText = "font-size: 4.5mm !important;";
      instruction.innerHTML = response.instructions[i];
      instructionsList.appendChild(instruction);
    }
    instructionsList.style.cssText = "margin-top: 1mm;";
    totalPrintContent.appendChild(instructionsList);
  }

  var questionsHeading = document.createElement("div");
  //questionsHeading.className = "webfont";
  //if(isFontAvailable('Bookman Old Style')) { questionsHeading.style.fontFamily = "Bookman Old Style"; }
  if(response.language == "english") {
    questionsHeading.innerHTML = "Questions";
  }
  else if(response.language == "hindi") {
    questionsHeading.innerHTML = "<span style='font-family: kruti'>iz'u</span>";
    //questionsHeading.innerHTML = "&#x92A;&#x94D;&#x930;&#x936;&#x94D;&#x928;";
  }
  questionsHeading.style.cssText = "margin-bottom: 2.5mm; margin-top: 2.5mm; font-size: 5.5mm";
  $(questionsHeading).attr("align","center");
  totalPrintContent.appendChild(questionsHeading);

  var questionCounter = 0;
  for(var i=0; i<response.question.length; ++i) {
    if(response.question[i].type == "question") {
      var questionsTable = document.createElement("table");
      questionsTable.className = "webfont";
      //if(isFontAvailable('Bookman Old Style')) { questionsTable.style.fontFamily = "Bookman Old Style"; }
      $(questionsTable).css("width","100%");
      $(questionsTable).css("font-size","4.5mm");
      //$(questionsTable).css("padding-left","5.29mm");
      //$(questionsTable).css("padding-left","2mm");

      var currRow = document.createElement("tr");
      currRow.className = "questionType";
      var questionDiv = getQuestionDivA4(response.question[i]);

      /*if(i!=0 && response.question[i-1].type == "section") {
        $(currRow).css("page-break-before","avoid");
      }*/

      var firstColumn = document.createElement("td");
      $(firstColumn).css("width","10mm");
      $(firstColumn).css("vertical-align","top");
      if(i==0 || response.question[i-1].type != "or") {
        if(response.language == "hindi") {
          firstColumn.innerHTML = "&#x92A;&#x94D;&#x930;.&nbsp;" + (++questionCounter);
        }
        else if(response.language == "english") {
          //firstColumn.innerHTML = "Q.&nbsp;" + (++questionCounter);
          firstColumn.innerHTML = "Q." + (++questionCounter);
        }
      }
      currRow.appendChild(firstColumn);

      var secondColumn = document.createElement("td");
      $(secondColumn).css("vertical-align","top");
      //$(secondColumn).css("width","100%");
      //$(secondColumn).css("width","190mm");
      secondColumn.appendChild(questionDiv);
      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        secondColumn.appendChild(getOrDivA4(response.language));
        secondColumn.appendChild(getQuestionDivA4(response.question[i+2]));
      }
      currRow.appendChild(secondColumn);

      var thirdColumn = document.createElement("td");
      $(thirdColumn).css("vertical-align","top");
      $(thirdColumn).css("text-align","right");
      $(thirdColumn).css("width","10mm");
      thirdColumn.innerHTML = "["+response.question[i].positiveMarks+"]";

      currRow.appendChild(thirdColumn);

      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        i+=2;
      }

      questionsTable.appendChild(currRow);
      totalPrintContent.appendChild(questionsTable);
    }
    else if(response.question[i].type == "section") {
      var currRow = document.createElement("div");
      //if(isFontAvailable('Bookman Old Style')) { currRow.style.fontFamily = "Bookman Old Style"; }
      currRow.className = "sectionType";
      $(currRow).css("margin-bottom","1mm");

      $(currRow).css("vertical-align","top");
      $(currRow).css("text-align","center");
      $(currRow).css("font-size","5mm");
      currRow.innerHTML = response.question[i].content;

      totalPrintContent.appendChild(currRow);

    }
    else if(response.question[i].type == "pagebreak") {
      var currRow = document.createElement("div");
      $(currRow).css("page-break-before","always");
      totalPrintContent.appendChild(currRow);
    }
    else if(response.question[i].type == "line") {
      var currRow = document.createElement("div");
      //if(isFontAvailable('Bookman Old Style')) { currRow.style.fontFamily = "Bookman Old Style"; }
      $(currRow).css("font-size","4.5mm");
      $(currRow).css("padding","0mm");
      $(currRow).css("margin","0mm");
      currRow.innerHTML = "&nbsp;";
      totalPrintContent.appendChild(currRow);
    }
    else {
      alert("unhandled paper element type");
    }
  }

  $(totalPrintContent).append("<div align='center'> ---------------- </div>");

  var cssStyleSheet = [];
  cssStyleSheet.push("../common/printstyleA4.css?v=54");

  console.log(totalPrintContent.innerHTML);
  printContent(totalPrintContent.innerHTML,"---",cssStyleSheet);
}

function handlePrintPaperA5(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  var totalPrintContent = document.createElement("div");

  if(!varEmptyOrNull(response.paperSet)) {
    var rollNumAndPaperSetTable = document.createElement("table");
    $(rollNumAndPaperSetTable).css("width","100%");

    var rollNumAndPaperSetRow  = document.createElement("tr");

    var rollNum = document.createElement("div");
    rollNum.style.cssText = "font-size: 5mm; display: inline-block;";
    $(rollNum).attr("align","left");
    if(response.language == "english") {
      rollNum.innerHTML = "Roll No. ...............";
    }
    else if(response.language == "hindi") {
      rollNum.innerHTML = "&#x930;&#x94B;&#x932; &#x928;&#x902; ...............";
    }
    var rollNumColumn = document.createElement("td");
    $(rollNumColumn).attr("align","left");
    rollNumColumn.appendChild(rollNum);
    rollNumAndPaperSetRow.appendChild(rollNumColumn);

    var paperSet = document.createElement("div");
    paperSet.style.cssText = "font-size: 5mm; display: inline-block;";
    $(paperSet).attr("align","right");
    if(response.language == "english") {
      paperSet.innerHTML = "Set - " + (response.paperSet);
    }
    else if(response.language == "hindi") {
      paperSet.innerHTML = "&#x938;&#x947;&#x91F; - " + (response.paperSet);
    }
    var paperSetColumn = document.createElement("td");
    $(paperSetColumn).attr("align","right");
    paperSetColumn.appendChild(paperSet);
    rollNumAndPaperSetRow.appendChild(paperSetColumn);

    rollNumAndPaperSetTable.appendChild(rollNumAndPaperSetRow);
    totalPrintContent.appendChild(rollNumAndPaperSetTable);
  }

  if(!varEmptyOrNull(response.paperHeading)) {
    var paperHeading = document.createElement("div");
    paperHeading.className = "prettybox";
    paperHeading.style.cssText = "font-size: 6.6125mm";
    $(paperHeading).attr("align","center");
    paperHeading.innerHTML = "<b>" + response.paperHeading + "</b>";
    totalPrintContent.appendChild(paperHeading);
  }

  /*var paperHeading = document.createElement("div");
  paperHeading.className = "prettybox";
  paperHeading.style.cssText = "font-size: 6.4mm";
  //if(isFontAvailable('Bookman Old Style')) { paperHeading.style.fontFamily = "Bookman Old Style"; }
  $(paperHeading).attr("align","center");
  paperHeading.innerHTML = "";
  var linebreak = false;
  if(!varEmptyOrNull(response.instituteName)) {
    paperHeading.innerHTML += "<b>" + response.instituteName + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.examType)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.examType + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.paperSubject)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.paperSubject + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.paperLevel)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.paperLevel + "</b>";
    linebreak = true;
  }
  if(!varEmptyOrNull(response.heading)) {
    if(linebreak) { paperHeading.innerHTML += "<br>";}
    paperHeading.innerHTML += "<b>" + response.heading + "</b>";
  }
  if(!varEmptyOrNull(paperHeading.innerHTML)) {
    totalPrintContent.appendChild(paperHeading);
  }*/

  var timeAndMarksTable = document.createElement("table");
  $(timeAndMarksTable).css("width","100%");

  var timeAndMarksRow  = document.createElement("tr");

  var time= document.createElement("div");
  time.style.cssText = "font-size: 5mm; display: inline-block;";
  //f(isFontAvailable('Bookman Old Style')) { time.style.fontFamily = "Bookman Old Style"; }
  $(time).attr("align","left");
  if(response.language == "english") {
    time.innerHTML = "Time: " + response.time;
  }
  else if(response.language == "hindi") {
    time.innerHTML = "&#2360;&#2350;&#2351;: " + response.time;
  }
  var timeColumn = document.createElement("td");
  $(timeColumn).attr("align","left");
  timeColumn.appendChild(time);
  timeAndMarksRow.appendChild(timeColumn);

  var totalMarks = document.createElement("div");
  totalMarks.style.cssText = "font-size: 5mm; display: inline-block;";
  //if(isFontAvailable('Bookman Old Style')) { totalMarks.style.fontFamily = "Bookman Old Style"; }
  $(totalMarks).attr("align","right");
  if(response.language == "english") {
    totalMarks.innerHTML = "Total Marks: " + (response.totalMarks);
  }
  else if(response.language == "hindi") {
    totalMarks.innerHTML = "&#2346;&#2370;&#2352;&#2381;&#2339;&#2366;&#2306;&#2325;: " + (response.totalMarks);
  }
  var totalMarksColumn = document.createElement("td");
  $(totalMarksColumn).attr("align","right");
  totalMarksColumn.appendChild(totalMarks);
  timeAndMarksRow.appendChild(totalMarksColumn);

  timeAndMarksTable.appendChild(timeAndMarksRow);
  totalPrintContent.appendChild(timeAndMarksTable);

  if(response.instructions.length != 0) {
    var instructionsHeading = document.createElement("div");
    instructionsHeading.className = "webfont";
    //if(isFontAvailable('Bookman Old Style')) { instructionsHeading.style.fontFamily = "Bookman Old Style"; }
    if(response.language == "english") {
      instructionsHeading.innerHTML = "Instructions";
    }
    else if(response.language == "hindi") {
      instructionsHeading.innerHTML = "&#2344;&#2367;&#2352;&#2381;&#2342;&#2375;&#2358;";
    }
    instructionsHeading.style.cssText = "margin-bottom: 3.9675mm; font-size: 6.4mm;";

    $(instructionsHeading).attr("align","center");
    totalPrintContent.appendChild(instructionsHeading);

    var instructionsList= document.createElement("ol");
    instructionsList.className = "webfont";
    //if(isFontAvailable('Bookman Old Style')) { instructionsList.style.fontFamily = "Bookman Old Style"; }
    for(var i=0;i<response.instructions.length;++i) {
      var instruction = document.createElement("li");
      instruction.innerHTML = response.instructions[i];
      instruction.style.cssText = "font-size: 5.5mm !important;";
      instructionsList.appendChild(instruction);
    }
    totalPrintContent.appendChild(instructionsList);
  }

  var questionsHeading = document.createElement("div");
  //questionsHeading.className = "webfont";
  //if(isFontAvailable('Bookman Old Style')) { questionsHeading.style.fontFamily = "Bookman Old Style"; }
  if(response.language == "english") {
    questionsHeading.innerHTML = "Questions";
  }
  else if(response.language == "hindi") {
    questionsHeading.innerHTML = "<span style='font-family: kruti'>iz'u</span>";
    //questionsHeading.innerHTML = "&#x92A;&#x94D;&#x930;&#x936;&#x94D;&#x928;";
  }
  questionsHeading.style.cssText = "margin-bottom: 3.9675mm; margin-top: 3.9675mm; font-size: 6.4mm";
  $(questionsHeading).attr("align","center");
  totalPrintContent.appendChild(questionsHeading);

  var questionCounter = 0;
  for(var i=0; i<response.question.length; ++i) {
    if(response.question[i].type == "question") {
      var questionsTable = document.createElement("table");
      questionsTable.className = "webfont";
      //if(isFontAvailable('Bookman Old Style')) { questionsTable.style.fontFamily = "Bookman Old Style"; }
      $(questionsTable).css("width","100%");
      $(questionsTable).css("font-size","5.5mm");
      //$(questionsTable).css("padding-left","5.29mm");
      //$(questionsTable).css("padding-left","2mm");

      var currRow = document.createElement("tr");
      currRow.className = "questionType";
      var questionDiv = getQuestionDivA5(response.question[i]);

      /*if(i!=0 && response.question[i-1].type == "section") {
        $(currRow).css("page-break-before","avoid");
      }*/

      var firstColumn = document.createElement("td");
      $(firstColumn).css("vertical-align","top");
      $(firstColumn).css("width","13mm");
      if(i==0 || response.question[i-1].type != "or") {
        if(response.language == "hindi") {
          firstColumn.innerHTML = "&#x92A;&#x94D;&#x930;.&nbsp;" + (++questionCounter);
        }
        else if(response.language == "english") {
          //firstColumn.innerHTML = "Q.&nbsp;" + (++questionCounter);
          firstColumn.innerHTML = "Q." + (++questionCounter);
          //firstColumn.innerHTML = (++questionCounter) + ".";
        }
      }
      currRow.appendChild(firstColumn);

      var secondColumn = document.createElement("td");
      $(secondColumn).css("vertical-align","top");
      //$(secondColumn).css("width","100%");
      secondColumn.appendChild(questionDiv);
      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        secondColumn.appendChild(getOrDivA5(response.language));
        secondColumn.appendChild(getQuestionDivA5(response.question[i+2]));
      }
      currRow.appendChild(secondColumn);

      var thirdColumn = document.createElement("td");
      $(thirdColumn).css("vertical-align","top");
      $(thirdColumn).css("text-align","right");
      $(thirdColumn).css("width","13mm");
      thirdColumn.innerHTML = "["+response.question[i].positiveMarks+"]";

      currRow.appendChild(thirdColumn);

      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        i+=2;
      }

      questionsTable.appendChild(currRow);
      totalPrintContent.appendChild(questionsTable);
    }
    else if(response.question[i].type == "section") {
      var currRow = document.createElement("div");
      //if(isFontAvailable('Bookman Old Style')) { currRow.style.fontFamily = "Bookman Old Style"; }
      currRow.className = "sectionType";
      $(currRow).css("margin-bottom","1mm");

      $(currRow).css("vertical-align","top");
      $(currRow).css("text-align","center");
      $(currRow).css("font-size","6mm");
      currRow.innerHTML = response.question[i].content;

      totalPrintContent.appendChild(currRow);
    }
    else if(response.question[i].type == "pagebreak") {
      var currRow = document.createElement("div");
      $(currRow).css("page-break-before","always");
      totalPrintContent.appendChild(currRow);
    }
    else if(response.question[i].type == "line") {
      var currRow = document.createElement("div");
      //if(isFontAvailable('Bookman Old Style')) { currRow.style.fontFamily = "Bookman Old Style"; }
      $(currRow).css("font-size","4.5mm");
      $(currRow).css("padding","0mm");
      $(currRow).css("margin","0mm");
      currRow.innerHTML = "&nbsp;";
      totalPrintContent.appendChild(currRow);
    }
    else {
      alert("unhandled paper element type");
    }
  }

  /*$(totalPrintContent).append("<div align='center'> ---------------- </div>");

  $("head").append('<link = rel="stylesheet" href="./common/printstyleA5.css?v=46">'); 
  document.getElementsByTagName("body")[0].innerHTML = totalPrintContent.outerHTML;*/
  var cssStyleSheet = [];
  cssStyleSheet.push("../common/printstyleA5.css?v=54");

  printContent(totalPrintContent.innerHTML,"---",cssStyleSheet);
  /*var formData = new FormData();
  formData.append("wordFile",totalPrintContent.innerHTML);
  formRequest("test.php",formData,dummyFunction,100);*/
}

/*function dummyFunction(jsonResponse) {
  var response;
}*/

function printContent(contents,title,cssStyleSheet) {
  var frame1 = document.createElement('iframe');
  frame1.name = "frame1";
  frame1.style.position = "absolute";
  frame1.style.top = "-264500mm";
  document.body.appendChild(frame1);
  var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
  frameDoc.document.open();
  frameDoc.document.write('<html><head>');
  if(!varEmptyOrNull(title)) {
    frameDoc.document.write('<title>' + title + '</title>');
  }
  frameDoc.document.write('<meta charset="UTF-8">');
  //frameDoc.document.write(' <script>window.MathJax = { MathML: { extensions: ["mml3.js", "content-mathml.js"]}};</script> <script type="text/x-mathjax-config"> MathJax.Hub.Config({ displayAlign: "left", }); </script> <script type="text/javascript" async src="../mathjax/MathJax.js?config=MML_HTMLorMML"></script>');
  frameDoc.document.write('<script type="text/x-mathjax-config"> MathJax.Hub.Config({ displayAlign: "left", skipStartupTypeset: false, zscale: "100%", showProcessingMessages: "none", messageStyle: "none", }); MathJax.Hub.processSectionDelay=0;</script><script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_CHTML"></script>');
  //frameDoc.document.write(' <script>function printIt() { window.print(); }</script>');
  frameDoc.document.write(' <script>function printIt() { '
                          + ' MathJax.Hub.Queue(["Typeset",MathJax.Hub],' 
                          + '[function() { setTimeout(function() {window.print();},500); }]); '
                          + '  }</script>');
  if(!varEmptyOrNull(cssStyleSheet)) {
    for(var i=0; i<cssStyleSheet.length; ++i) {
      frameDoc.document.write('<link rel="stylesheet" href="' + cssStyleSheet[i] + '">');
    }
  }
  frameDoc.document.write('</head><body onload="printIt();">');
  frameDoc.document.write(contents);
  frameDoc.document.write('</body></html>');
  frameDoc.document.close();
  /*MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
  setTimeout(function () {
    window.frames["frame1"].focus();
    window.frames["frame1"].print();
    document.body.removeChild(frame1);
  }, 1500);*/
  return false;
}

//assuming the paper is in english
function handleWordPaper(jsonResponse) {
  var response = JSON.parse(jsonResponse);

  var content = "";

  if(!varEmptyOrNull(response.paperSet)) {
    content += "Roll No. .............. \nSet - " + (response.paperSet) + "\n\n";
  }

  if(!varEmptyOrNull(response.paperHeading)) {
    var heading = document.createElement("div");
    heading.innerHTML = response.paperHeading;
    content += $(heading).text() + "\n\n";
  }

  content += "Time: " + response.time + "\n";
  content += "Total Marks: " + response.totalMarks + "\n\n";

  content += "Instructions\n";

  if(response.instructions.length != 0) {
    for(var i=0;i<response.instructions.length;++i) {
      content += (i+1) +". "+response.instructions[i]+"\n";
    }
  }
  content += "\n";

  content += "Questions\n\n";

  var questionCounter = 0;
  for(var i=0; i<response.question.length; ++i) {
    if(response.question[i].type == "question") {

      if(i==0 || response.question[i-1].type != "or") {
        content += "Q. " + (++questionCounter) + "\n";
      }

      var questionDiv = document.createElement("div");
      questionDiv.innerHTML = response.question[i].question;
      content += $(questionDiv).text() + "\n";

      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        content += "OR\n";
        
        var orQuestionDiv = document.createElement("div");
        orQuestionDiv.innerHTML = response.question[i+2].question;
        content += $(orQuestionDiv).text() + "\n";
      }

      content += "["+response.question[i].positiveMarks+"]\n\n";

      if(i!=response.question.length-1 && response.question[i+1].type == "or") {
        i+=2;
      }
    }
    else if(response.question[i].type == "section") {
      var section = document.createElement("div");
      section.innerHTML = response.question[i].content;
      content += $(section).text() + "\n";
    }
    else if(response.question[i].type == "pagebreak" || response.question[i].type == "line") { 
      //nothing
    }
    else {
      alert("unhandled paper element type: " + response.question[i].type);
    }
  }

  download(content,"qlib.docx","text/plain");
}

