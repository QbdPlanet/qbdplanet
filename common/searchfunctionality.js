$(function() {
  $("#searchStr").keydown(function(evt) {
    var e = event || evt;
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) {
      refreshQuestionList();
      e.preventDefault();
      return false;
    }
  });
});
