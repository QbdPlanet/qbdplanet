function getWidth(fontFamily) {
  var width;
  var body = document.body;

  var container = document.createElement('span');
  container.innerHTML = Array(100).join('wi');
  container.style.cssText = [
    'position:absolute',
    'width:auto',
    'font-size:128px',
    'left:-99999px'
  ].join(' !important;');

  container.style.fontFamily = fontFamily;

  $('body').append(container);
  width = container.clientWidth;
  $(container).remove();

  return width;
};

function isFontAvailable(font) {
  var monoWidth  = getWidth('monospace');
  var serifWidth = getWidth('serif');
  var sansWidth  = getWidth('sans-serif');
  return monoWidth !== getWidth(font + ',monospace') ||
    sansWidth !== getWidth(font + ',sans-serif') ||
    serifWidth !== getWidth(font + ',serif');
}
