<?php

function checkPostVariables($variables) {
  $varArray = explode(";",$variables);
  for($i=0;$i<count($varArray);++$i) {
    if( !isset( $_POST[$varArray[$i]] ) ) {
      $URL="http://" . $_SERVER['SERVER_NAME'];
      echo "<script>location.href='$URL'</script>";
      exit();
    }
  }
  return true;
}

function getImageName() {
  $files = scandir($_SERVER['DOCUMENT_ROOT'] . "/images/questions/",SCANDIR_SORT_ASCENDING);
  $largestNumber=0;
  for($loop=0;$loop<count($files);++$loop) {
    $number = pathinfo($files[$loop],PATHINFO_FILENAME);
    if($largestNumber < $number)  {
      $largestNumber = $number;
    }
  }
  return ++$largestNumber;
}

function handleImage($image) {

  if($image["size"] > 150000) {
    bye("Image File Size: " . $_image["size"] . " is too large");
  }

  $imageFileType = pathinfo(basename($image["name"]),PATHINFO_EXTENSION);
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) {
      bye("Image File Type: " . $imageFileType . ", only JPG, JPEG, PNG & GIF files are allowed.");
  }

  $check = getimagesize($image["tmp_name"]);
  if($check == false) {
    bye("File is not recognized as an image by server");
  }

  $imageName = "images/questions/" . getImageName() . "." . $imageFileType;

  if (!move_uploaded_file($image["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/" . $imageName)) {
    $response = array();
    $response["result"] = "failure";
    $response["message"] = "Error occured while uploading Image File";
    echo json_encode($response);
    exit();
  }

  return $imageName;

}

function getInstituteId($con,$instituteName,$userId) {
  $instituteId;
  $instituteName = mysqli_real_escape_string($con,$instituteName);
  $query = "SELECT InstituteId FROM Institute WHERE InstituteName='$instituteName' AND UserId='$userId'";
  $result = mysqli_query($con,$query);
  if($row = mysqli_fetch_array($result)) {
    $instituteId = $row[0];
  }
  else {
    $query = "INSERT INTO Institute (InstituteName,UserId) VALUES ('$instituteName','$userId')";
    $result = mysqli_query($con,$query);
    $instituteId = $con->insert_id;
  }
  return $instituteId;
}

function getExamId($con,$examName,$userId) {
  $examId;
  $examName = mysqli_real_escape_string($con,$examName);
  $query = "SELECT ExamId FROM Exam WHERE ExamName='$examName' AND UserId='$userId'";
  $result = mysqli_query($con,$query);
  if($row = mysqli_fetch_array($result)) {
    $examId = $row[0];
  }
  else {
    $query = "INSERT INTO Exam (ExamName,UserId) VALUES ('$examName','$userId')";
    $result = mysqli_query($con,$query);
    $examId = $con->insert_id;
  }
  return $examId;
}

function getSubjectTopicId($con,$subject,$topic,$userId) {
  $subjectTopicId;
  $subject = mysqli_real_escape_string($con,$subject);
  $topic = mysqli_real_escape_string($con,$topic);
  $query = "SELECT SubjectTopicId FROM SubjectTopic WHERE Subject='$subject' AND Topic='$topic' AND UserId = '$userId'";
  $result = mysqli_query($con,$query);
  if($row = mysqli_fetch_array($result)) {
    $subjectTopicId = $row[0];
  }
  else {
    $query = "INSERT INTO SubjectTopic (Subject,Topic,UserId) VALUES ('$subject','$topic','$userId')";
    $result = mysqli_query($con,$query);
    $subjectTopicId = $con->insert_id;
  }
  return $subjectTopicId;
}

function getDifficultyLevelId($con,$level,$userId) {
  $levelId;
  $level = mysqli_real_escape_string($con,$level);
  $query = "SELECT DifficultyLevelId FROM DifficultyLevel WHERE DifficultyLevel='$level' AND UserId = '$userId'";
  $result = mysqli_query($con,$query);
  if($row = mysqli_fetch_array($result)) {
    $levelId = $row[0];
  }
  else {
    $query = "INSERT INTO DifficultyLevel (DifficultyLevel,UserId) VALUES ('$level','$userId')";
    $result = mysqli_query($con,$query);
    $levelId = $con->insert_id;
  }
  return $levelId;
}

?>
