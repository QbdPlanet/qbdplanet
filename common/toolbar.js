$(function () {
  var toolbar = document.getElementById("toolbar");
  $(toolbar).append('<img src="../images/superscript.png" onclick="superscript();"></img>');
  $(toolbar).append('<img src="../images/subscript.png" onclick="subscript();"></img>');
  $(toolbar).append('<img src="../images/bold.png" onclick="bold();"></img>');
  $(toolbar).append('<img src="../images/underline.png" onclick="underline();"></img>');
  $(toolbar).append('<img src="../images/italic.png" onclick="italic();"></img>');
  $(toolbar).append('<img src="../images/justifyleft.png" onclick="justifyLeft();"></img>');
  $(toolbar).append('<img src="../images/justifycenter.png" onclick="justifyCenter();"></img>');
  $(toolbar).append('<img src="../images/justifyright.png" onclick="justifyRight();"></img>');
  $(toolbar).append('<img src="../images/justifyfull.png" onclick="justifyFull();"></img>');
  initOrderedListInToolbar(toolbar);
  initUnorderedListInToolbar(toolbar);
  $(toolbar).append('<label style="margin: 0mm 0mm 0mm 2mm">Match the Column: </label>');
  $(toolbar).append('<button type="button" onclick="insertTableRow();" accesskey="t">+</button>');
  $(toolbar).append('<button type="button" onclick="deleteTableRow();" accesskey="r">-</button>');
  $(toolbar).append('<label style="margin: 0mm 0mm 0mm 2mm">Options: </label>');
  //$(toolbar).append('<button type="button" onclick="insertOption();" accesskey="o">+</button>');
  $(toolbar).append('<button type="button" onclick="insertOptionOneCrossFour();" accesskey="o">1&times;4</button>');
  $(toolbar).append('<button type="button" onclick="insertOptionTwoCrossTwo();" >2&times;2</button>');
  $(toolbar).append('<button type="button" onclick="insertOptionFourCrossOne();" >4&times;1</button>');
  $(toolbar).append('<button type="button" onclick="deleteOption();" accesskey="p">-</button>');
  $(toolbar).append('<label style="margin: 0mm 0mm 0mm 2mm">SubQuestion: </label>');
  $(toolbar).append('<button type="button" onclick="insertSubQuestion();" accesskey="n">+</button>');
  $(toolbar).append('<button type="button" onclick="deleteSubQuestion();" accesskey="m">-</button>');
  $(toolbar).append('<button type="button" onclick="openMathEditor();">Math</button>');
  //$(toolbar).append('<button type="button" onclick="insertFraction();">frac</button>');
});

function openMathEditor() {
  var width = "600";
  var height = "350";
  var left = (screen.availWidth-width)/2;
  var top = (screen.availHeight-height)/2;
  var sizeParam = "width="+width+",height="+height+",left="+left+",top="+top;
  var myWindow=window.open(rootPath+"matheditor.html",'MyWindow',sizeParam);
  //window.open(rootPath+"matheditor.html");
}

function initUnorderedListInToolbar(toolbar) {
  var unorderedListContainer = document.createElement("div");
  unorderedListContainer.style.display = "inline-block";
  $(unorderedListContainer).append('<img src="../images/unorderedlist.png"></img>');
  var dropdowncontent = document.createElement("div");
  dropdowncontent.className = "toolbar-dropdown-content";
  var types = ["disc","circle","square"];
  $(dropdowncontent).append('<button type="button" onclick="insertUnorderedList(\'disc\');">&bull;</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertUnorderedList(\'square\');">&#9642;</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertUnorderedList(\'circle\');">&#9675;</button>');
  $(unorderedListContainer).append(dropdowncontent);
  $(toolbar).append(unorderedListContainer);
  var timeoutId;
  $(unorderedListContainer).hover(function() {
      if (!timeoutId) {
        timeoutId = window.setTimeout(
          function() {
            timeoutId = null;
            $(dropdowncontent).show();
          }, 100);
      }
  },
  function () {
      if (timeoutId) {
          window.clearTimeout(timeoutId);
          timeoutId = null;
      }
      else {
         $(dropdowncontent).slideUp(200);
      }
  });
}

function initOrderedListInToolbar(toolbar) {
  var orderedListContainer = document.createElement("div");
  orderedListContainer.style.display = "inline-block";
  $(orderedListContainer).append('<img id="orderedlist" src="../images/orderedlist.png"></img>');
  var dropdowncontent = document.createElement("div");
  dropdowncontent.className = "toolbar-dropdown-content";
  var types = ["1","a","A","i","I"];
  $(dropdowncontent).append('<button type="button" onclick="insertOrderedList(\'1\');">1.</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertOrderedList(\'a\');">a.</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertOrderedList(\'A\');">A.</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertOrderedList(\'i\');">i.</button>');
  $(dropdowncontent).append('<button type="button" onclick="insertOrderedList(\'I\');">I.</button>');
  $(orderedListContainer).append(dropdowncontent);
  $(toolbar).append(orderedListContainer);
  var timeoutId;
  $(orderedListContainer).hover(function() {
      if (!timeoutId) {
        timeoutId = window.setTimeout(
          function() {
            timeoutId = null;
            $(dropdowncontent).show();
          }, 100);
      }
  },
  function () {
      if (timeoutId) {
          window.clearTimeout(timeoutId);
          timeoutId = null;
      }
      else {
         $(dropdowncontent).slideUp(200);
      }
  });
}

function insertFraction() {
  if(!inFormatBox()) { alert("can only be inserted in edit box"); return; }
  var BigDiv = document.createElement("div");
  /*var fracTable = document.createElement("table");
  fracTable.style.display = "inline";
  $(fracTable).append("<tr><td></td></tr><tr><td></td></tr>");
  BigDiv.appendChild(fracTable);
  $(BigDiv).append("<span></span>");*/
  $(BigDiv).append("<nobr><table style='display: inline'><tr><td></td></tr><tr><td></td></tr></table></nobr>");
  pasteHtmlAtCaret(BigDiv.innerHTML);
}

function insertSubQuestion() {
  if(!inFormatBox()) { alert("can only be inserted in edit box"); return; }
  var tableElem = document.getElementsByClassName("formatbox")[0].getElementsByClassName("subQuestionContainer")[0];
  if(varEmptyOrNull(tableElem)) {
    tableElem = document.createElement("table");
    tableElem.className = "subQuestionContainer";
    $(".formatbox").append(tableElem);
    var rowElem = document.createElement("tr");
    rowElem.className = "subQuestionRow";
    $(rowElem).append("<td style='width: 5mm;'></td>");
    $(rowElem).append("<td></td>");
    $(rowElem).append("<td style='width: 5mm;'></td>");
    tableElem.appendChild(rowElem);
    placeCursorAtStart(rowElem);
  }
  else {
    var rowElem = document.createElement("tr");
    rowElem.className = "subQuestionRow";
    $(rowElem).append("<td style='width: 5mm;'></td>");
    $(rowElem).append("<td></td>");
    $(rowElem).append("<td style='width: 5mm;'></td>");
    tableElem.appendChild(rowElem);
    placeCursorAtStart(rowElem);
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function deleteSubQuestion() {
  var focusNode = window.getSelection().focusNode;
  var deleted = false;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("subQuestionRow")) {
      $(focusNode).remove();
      deleted = true;
      var tableElem = document.getElementsByClassName("formatbox")[0].getElementsByClassName("subQuestionContainer")[0];
      if(!varEmptyOrNull(tableElem)) {
        if(tableElem.rows.length == 0) {
          $(tableElem).remove();
        }
        placeCursorAtStart(document.getElementsByClassName("formatbox")[0]);
      }
      if($("#questionStatementFinalCut").length) {
        populateQuestionCard('#questionStatementFinalCut','#question','Question');
      }
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if(!deleted) {
    alert("Place cursor at a sub question to delete");
  }
}

/*function insertOption() {
  if(!inFormatBox()) { return; }
  var focusNode = window.getSelection().focusNode;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("formatbox") ) {
      var optionContainer = document.createElement("table");
      optionContainer.className = "optionContainer";
      $(focusNode).append(optionContainer);
      var rowElem = document.createElement("tr");
      $(rowElem).append("<td></td><td></td><td></td><td></td>");
      optionContainer.appendChild(rowElem);
      placeCursorAtStart(rowElem);
      break;
    }
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("subQuestionRow")) {
      if($(focusNode)[0].getElementsByClassName("optionContainer").length !=0) {
        alert("options already exist in this subquestion");
        return;
      }
      var optionContainer = document.createElement("table");
      optionContainer.className = "optionContainer";
      var secondColumn = focusNode.getElementsByTagName("td")[1];
      $(secondColumn).append(optionContainer);
      var rowElem = document.createElement("tr");
      $(rowElem).append("<td></td><td></td><td></td><td></td>");
      optionContainer.appendChild(rowElem);
      placeCursorAtStart(rowElem);
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}*/

function insertOptionOneCrossFour() {
  if(!inFormatBox()) { return; }
  var focusNode = window.getSelection().focusNode;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("formatbox") ) {
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder oneCrossFour";
      $(focusNode).append(optionHolder);
      var rowElem = document.createElement("tr");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      optionHolder.appendChild(rowElem);
      placeCursorAtStart(rowElem);
      break;
    }
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("subQuestionRow")) {
      if($(focusNode)[0].getElementsByClassName("optionHolder").length !=0) {
        alert("options already exist in this subquestion");
        return;
      }
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder onCrossFour";
      var secondColumn = focusNode.getElementsByTagName("td")[1];
      $(secondColumn).append(optionHolder);
      var rowElem = document.createElement("tr");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      $(rowElem).append("<td class='optionIndex'></td><td></td>");
      optionHolder.appendChild(rowElem);
      placeCursorAtStart(rowElem);
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function insertOptionTwoCrossTwo() {
  if(!inFormatBox()) { return; }
  var focusNode = window.getSelection().focusNode;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("formatbox") ) {
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder oneCrossFour";
      $(focusNode).append(optionHolder);
      var firstRowElem = document.createElement("tr");
      var secondRowElem = document.createElement("tr");
      $(firstRowElem).append("<td class='optionIndex'></td><td></td>");
      $(firstRowElem).append("<td class='optionIndex'></td><td></td>");
      $(secondRowElem).append("<td class='optionIndex'></td><td></td>");
      $(secondRowElem).append("<td class='optionIndex'></td><td></td>");
      optionHolder.appendChild(firstRowElem);
      optionHolder.appendChild(secondRowElem);
      placeCursorAtStart(firstRowElem);
      break;
    }
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("subQuestionRow")) {
      if($(focusNode)[0].getElementsByClassName("optionHolder").length !=0) {
        alert("options already exist in this subquestion");
        return;
      }
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder onCrossFour";
      var secondColumn = focusNode.getElementsByTagName("td")[1];
      $(secondColumn).append(optionHolder);
      var firstRowElem = document.createElement("tr");
      var secondRowElem = document.createElement("tr");
      $(firstRowElem).append("<td class='optionIndex'></td><td></td>");
      $(firstRowElem).append("<td class='optionIndex'></td><td></td>");
      $(secondRowElem).append("<td class='optionIndex'></td><td></td>");
      $(secondRowElem).append("<td class='optionIndex'></td><td></td>");
      optionHolder.appendChild(firstRowElem);
      optionHolder.appendChild(secondRowElem);
      placeCursorAtStart(firstRowElem);
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function insertOptionFourCrossOne() {
  if(!inFormatBox()) { return; }
  var focusNode = window.getSelection().focusNode;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("formatbox") ) {
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder oneCrossFour";
      $(focusNode).append(optionHolder);
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      placeCursorAtStart(optionHolder);
      break;
    }
    if(focusNode.nodeName != "#text" && $(focusNode).hasClass("subQuestionRow")) {
      if($(focusNode)[0].getElementsByClassName("optionHolder").length !=0) {
        alert("options already exist in this subquestion");
        return;
      }
      var optionHolder = document.createElement("table");
      optionHolder.className = "optionHolder onCrossFour";
      var secondColumn = focusNode.getElementsByTagName("td")[1];
      $(secondColumn).append(optionHolder);
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      $(optionHolder).append("<tr><td class='optionIndex'></td><td></td></tr>");
      placeCursorAtStart(optionHolder);
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function deleteOption() {
  var focusNode = window.getSelection().focusNode;
  var deleted = false;
  while(focusNode != null) {
    if(focusNode.nodeName != "#text" && ($(focusNode).hasClass("optionContainer") || $(focusNode).hasClass("optionHolder")) ) {
      $(focusNode).remove();
      deleted = true;
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if(!deleted) {
    alert("Place cursor at a group of options to delete");
  }
}

function insertTableRow() {
  //var tableElem = document.getElementById("question").getElementsByClassName("matchTheCol")[0];
  var tableElem = document.getElementsByClassName("formatbox")[0].getElementsByClassName("matchTheCol")[0];
  if(varEmptyOrNull(tableElem)) {
    tableElem = document.createElement("table");
    tableElem.className = "matchTheCol";
    //$("#question").append(tableElem);
    $(".formatbox").append(tableElem);
    var rowHeadElem = document.createElement("tr");
    $(rowHeadElem).append("<td></td>");
    $(rowHeadElem).append("<td>&nbsp;-&nbsp;</td>");
    $(rowHeadElem).append("<td></td>");
    tableElem.appendChild(rowHeadElem);
    /*var rowFirstElem = document.createElement("tr");
    $(rowFirstElem).append("<td></td>");
    $(rowFirstElem).append("<td>&nbsp;-&nbsp;</td>");
    $(rowFirstElem).append("<td></td>");
    tableElem.appendChild(rowFirstElem);
    var rowSecondElem = document.createElement("tr");
    $(rowSecondElem).append("<td></td>");
    $(rowSecondElem).append("<td>&nbsp;-&nbsp;</td>");
    $(rowSecondElem).append("<td></td>");
    tableElem.appendChild(rowSecondElem);
    var rowElem = document.createElement("tr");
    $(rowElem).append("<td></td>");
    $(rowElem).append("<td>&nbsp;-&nbsp;</td>");
    $(rowElem).append("<td></td>");
    tableElem.appendChild(rowElem);*/
    placeCursorAtStart(rowHeadElem);
  }
  else {
    var rowElem = document.createElement("tr");
    $(rowElem).append("<td></td>");
    $(rowElem).append("<td>&nbsp-&nbsp;</td>");
    $(rowElem).append("<td></td>");
    tableElem.appendChild(rowElem);
    placeCursorAtStart(rowElem);
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function deleteTableRow() {
  //var tableElem = document.getElementById("question").getElementsByClassName("matchTheCol")[0];
  var tableElem = document.getElementsByClassName("formatbox")[0].getElementsByClassName("matchTheCol")[0];
  if(!varEmptyOrNull(tableElem)) {
    tableElem.deleteRow(tableElem.rows.length-1);
    if(tableElem.rows.length == 0) {
      //document.getElementById("question").removeChild(tableElem);
      $(tableElem).remove();
    }
    //placeCursorAtStart(document.getElementById("question"));
    placeCursorAtStart(document.getElementsByClassName("formatbox")[0]);
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function superscript() {
  if(!inEditBox()) { return; }
  var finalFocusNode = window.getSelection().focusNode;
  /*while(finalFocusNode != null) {
    if($(finalFocusNode).hasClass("editbox") ){
      break;
    }
    finalFocusNode = finalFocusNode.parentNode;
  }
  if(finalFocusNode == null) { return; }*/
  var parentEditBox = finalFocusNode;

  if(window.getSelection().focusNode.parentNode.nodeName == "SUP") {
    document.execCommand('superscript',false,null);
    document.execCommand('superscript',false,null);
    $(parentEditBox).focus();
  }
  else {
    document.execCommand('superscript',false,null);
    $(parentEditBox).focus();
  }
}

/*function insertSuperScript() {
  if(!inEditBox()) { return; }
  var sel, range, html;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.getRangeAt && sel.rangeCount) {
      range = sel.getRangeAt(0);
      range.deleteContents();
      var span = document.createElement("div");
      span.innerHTML = "<qbd class='emptyBorder'>a</qbd><sup><qbd class='emptyBorder'>b</qbd></sup> ";
      range.insertNode(span);
      range.setStartAfter(span);
      range.setEndAfter(span);
      sel.removeAllRanges();
      sel.addRange(range);
      //range.insertNode( document.createTextNode(text) );
    }
  }
}*/

function subscript() {
  var finalFocusNode = window.getSelection().focusNode;
  while(finalFocusNode != null) {
    if($(finalFocusNode).hasClass("editbox") ){
      break;
    }
    finalFocusNode = finalFocusNode.parentNode;
  }
  if(finalFocusNode == null) { return; }
  var parentEditBox = finalFocusNode;

  if(window.getSelection().focusNode.parentNode.nodeName == "SUB") {
    document.execCommand('subscript',false,null);
    document.execCommand('subscript',false,null);
    $(parentEditBox).focus();
  }
  else {
    document.execCommand('subscript',false,null);
    $(parentEditBox).focus();
  }
}

function indent() {
  document.execCommand('indent',false,null);
}

function outdent() {
  document.execCommand('outdent',false,null);
}

function inFormatBox() {
  var focusNode = window.getSelection().focusNode;
  while(focusNode!=null) {
    if($(focusNode).hasClass("formatbox")) {
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if(focusNode == null) { return false; } else { return true; }
}

function inEditBox() {
  var focusNode = window.getSelection().focusNode;
  while(focusNode!=null) {
    if($(focusNode).hasClass("editbox")) {
      break;
    }
    focusNode = focusNode.parentNode;
  }
  if(focusNode == null) { return false; } else { return true; }
}

function insertOrderedList(type) {
  if(!inEditBox()) { return ;}
  var focusNode = window.getSelection().focusNode;
  if(focusNode == null) { return; }
  if(focusNode.nodeName == "LI") {
    var listNode;
    listNode = focusNode.parentNode;
    if(listNode.tagName == "UL") {
      var newListNode = document.createElement("ol");
      newListNode.innerHTML = listNode.innerHTML;
      newListNode.type = type;
      listNode.parentNode.replaceChild(newListNode,listNode);
      placeCursorAtEnd(newListNode);
    }
    else {
      listNode.type = type;
      placeCursorAtEnd(focusNode);
    }
  }
  else if (focusNode.parentNode != null && focusNode.parentNode.nodeName == "LI") {
    var listNode;
    listNode = focusNode.parentNode.parentNode;
    if(listNode.tagName == "UL") {
      var newListNode = document.createElement("ol");
      newListNode.innerHTML = listNode.innerHTML;
      newListNode.type = type;
      listNode.parentNode.replaceChild(newListNode,listNode);
      placeCursorAtEnd(newListNode);
    }
    else {
      listNode.type = type;
      placeCursorAtEnd(focusNode.parentNode);
    }
  }
  else if($(focusNode).hasClass("editbox")) {
    document.execCommand('insertOrderedList',false,null);
    focusNode = $(focusNode).find("OL")[0];
    focusNode.type = type;
  }
  else if(focusNode.nodeName == "#text") {
    document.execCommand('insertOrderedList',false,null);
    var finalFocusNode = window.getSelection().focusNode;
    while(finalFocusNode != null) {
      if($(finalFocusNode).hasClass("editbox")) {
        finalFocusNode = $(finalFocusNode).find("OL")[0];
        finalFocusNode.type = type;
        break;
      }
      else if(finalFocusNode.nodeName == "OL") {
        finalFocusNode.type = type;
        break;
      }
      finalFocusNode = finalFocusNode.parentNode;
    }
  }
  else {
    document.execCommand('insertOrderedList',false,null);
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }

  var finalFocusNode = window.getSelection().focusNode;
  console.log("Focus is on");
  while(finalFocusNode != null) {
    console.log(finalFocusNode.nodeName);
    finalFocusNode = finalFocusNode.parentNode;
  }
}

function insertUnorderedList(type) {
  if(!inEditBox()) { return ;}
  var focusNode = window.getSelection().focusNode;
  if(focusNode == null) { return; }
  if(focusNode.nodeName == "LI") {
    var listNode;
    listNode = focusNode.parentNode;
    if(listNode.tagName == "OL") {
      var newListNode = document.createElement("ul");
      newListNode.innerHTML = listNode.innerHTML;
      newListNode.style.listStyleType = type;
      listNode.parentNode.replaceChild(newListNode,listNode);
      placeCursorAtEnd(newListNode);
    }
    else {
      listNode.style.listStyleType = type;
      placeCursorAtEnd(focusNode);
    }
  }
  else if (focusNode.parentNode != null && focusNode.parentNode.nodeName == "LI") {
    var listNode;
    listNode = focusNode.parentNode.parentNode;
    if(listNode.tagName == "OL") {
      var newListNode = document.createElement("ul");
      newListNode.innerHTML = listNode.innerHTML;
      newListNode.style.listStyleType = type;
      listNode.parentNode.replaceChild(newListNode,listNode);
      placeCursorAtEnd(newListNode);
    }
    else {
      listNode.style.listStyleType = type;
      placeCursorAtEnd(focusNode.parentNode);
    }
  }
  else if($(focusNode).hasClass("editbox")) {
    document.execCommand('insertUnorderedList',false,null);
    focusNode = $(focusNode).find("UL")[0];
    focusNode.style.listStyleType = type;
  }
  else if(focusNode.nodeName == "#text") {
    document.execCommand('insertUnorderedList',false,null);
    var finalFocusNode = window.getSelection().focusNode;
    while(finalFocusNode != null) {
      if($(finalFocusNode).hasClass("editbox")) {
        finalFocusNode = $(finalFocusNode).find("UL")[0];
        finalFocusNode.style.listStyleType = type;
        break;
      }
      else if(finalFocusNode.nodeName == "UL") {
        finalFocusNode.style.listStyleType = type;
        break;
      }
      finalFocusNode = finalFocusNode.parentNode;
    }
  }
  else {
    document.execCommand('insertUnorderedList',false,null);
  }
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function justifyRight() {
  document.execCommand('justifyRight',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function justifyFull() {
  document.execCommand('justifyFull',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function justifyCenter() {
  document.execCommand('justifyCenter',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function justifyLeft() { 
  document.execCommand('justifyLeft',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
}

function bold() {
  var finalFocusNode = window.getSelection().focusNode;
  while(finalFocusNode != null) {
    if($(finalFocusNode).hasClass("editbox") ){
      break;
    }
    finalFocusNode = finalFocusNode.parentNode;
  }
  if(finalFocusNode == null) { return; }
  var parentEditBox = finalFocusNode;

  document.execCommand('bold',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
  $(parentEditBox).focus();
}

function underline() {
  var finalFocusNode = window.getSelection().focusNode;
  while(finalFocusNode != null) {
    if($(finalFocusNode).hasClass("editbox") ){
      break;
    }
    finalFocusNode = finalFocusNode.parentNode;
  }
  if(finalFocusNode == null) { return; }
  var parentEditBox = finalFocusNode;

  document.execCommand('underline',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
  $(parentEditBox).focus();
}

function italic() {
  var finalFocusNode = window.getSelection().focusNode;
  while(finalFocusNode != null) {
    if($(finalFocusNode).hasClass("editbox") ){
      break;
    }
    finalFocusNode = finalFocusNode.parentNode;
  }
  if(finalFocusNode == null) { return; }
  var parentEditBox = finalFocusNode;

  document.execCommand('italic',false,null);
  if($("#questionStatementFinalCut").length) {
    populateQuestionCard('#questionStatementFinalCut','#question','Question');
  }
  $(parentEditBox).focus();
}

function placeCursorAtEnd(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
          && typeof document.createRange != "undefined") {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
  } else if (typeof document.body.createTextRange != "undefined") {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
  }
}

function placeCursorAtStart(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
          && typeof document.createRange != "undefined") {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(true);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
  } else if (typeof document.body.createTextRange != "undefined") {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(true);
      textRange.select();
  }
}
