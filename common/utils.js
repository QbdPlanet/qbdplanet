// ----- To check if variable is empty or null
function varEmptyOrNull(x) {
  if (x == null || x == "") {
    return true;
  }
  return false;
}


function handleResponseTextInUtils(responseText, customFunction) {
  if(rootPath == "http://localhost/") {
    console.log(responseText);
  }
  var response = JSON.parse(responseText);
  if( "loggedOut" in response) {
    var width = "500";
    var height = "350";
    var left = (screen.availWidth-width)/2;
    var top = (screen.availHeight-height)/2;
    var sizeParam = "width="+width+",height="+height+",left="+left+",top="+top;
    var myWindow=window.open(rootPath+"authentication/login.html?popup=",'MyWindow',sizeParam);
  }
  else {
    customFunction(responseText);
  }
}

// ----- For post XMLHTTPRequest
function postRequestHandleError( filename, postData, customFunction , errorFunction, errorVar, progress) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    releaseClicksOfBody(progress);
    if (xhttp.readyState == 4) {
      if(!varEmptyOrNull(progress)) {
        setProgress(progress);
      }
      if( xhttp.status == 200) {
        handleResponseTextInUtils(xhttp.responseText,customFunction);
      }
      else {
        errorFunction(errorVar);
      }
    }
  };
  xhttp.open("POST", rootPath+filename, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  holdClicksOnBody(progress);
  xhttp.send(postData);
}

// ----- For post XMLHTTPRequest
function postRequest( filename, postData, customFunction , progress) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    releaseClicksOfBody(progress);
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      if(!varEmptyOrNull(progress)) {
        setProgress(progress);
      }
      handleResponseTextInUtils(xhttp.responseText,customFunction);
    }
  };
  xhttp.open("POST", rootPath+filename, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  holdClicksOnBody(progress);
  xhttp.send(postData);
}

function formRequest( filename, formData, customFunction , progress) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    releaseClicksOfBody(progress);
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      if(!varEmptyOrNull(progress)) {
        setProgress(progress);
      }
      handleResponseTextInUtils(xhttp.responseText,customFunction);
    }
  };
  xhttp.open("POST",rootPath+filename);
  holdClicksOnBody(progress);
  xhttp.send(formData);
}

function holdClicksOnBody(progress) {
  if(!varEmptyOrNull(progress)) {
    $("#bar").show();
  }
  $("body").append("<div id='toHoldClicksTill'></div>");
}

function releaseClicksOfBody(progress) {
  var toHoldClicksTill = document.getElementById("toHoldClicksTill");
  if(!varEmptyOrNull(toHoldClicksTill)) {
    $("body")[0].removeChild(toHoldClicksTill);
  }
}

// ----- For get XMLHTTPRequest
function getRequest( filename, customFunction ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      handleResponseTextInUtils(xhttp.responseText,customFunction);
    }
  };
  xhttp.open("GET", rootPath+filename, true);
  xhttp.send();
}

// ----- To Remove given child from it's parent
function removeChildFromParent( childId ) {
  var child = document.getElementById(childId);
  if( child ) {
    child.parentNode.removeChild(child);
  }
}

// ----- To Get Url Variables
function getUrlVars() {
  var url = window.location.href,
  vars = {};
  url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
    key = decodeURIComponent(key);
    value = decodeURIComponent(value);
    vars[key] = value;
  });
  return vars;
}

// ----- To set cookies with expired time
function setCookie(cname, cvalue, exMins) {
  var d = new Date();
  d.setTime(d.getTime() + (exMins*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

// ----- To set cookies with expired time in seconds
function setCookieInSeconds(cname, cvalue, exSeconds) {
  var d = new Date();
  d.setTime(d.getTime() + (exSeconds*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

// ----- To get cookies
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
  document.cookie = cname + "=; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/";
}

function focusGone() {
  alert("This is a warning message. No other window or activity should be done when the test is running.");
}

function addDefaultValueToSelect(selectTag,selectDefaultValue) {
  var option = document.createElement('option');
  option.innerHTML = selectDefaultValue;
  option.value = 0;
  selectTag.appendChild(option);
}

function containerMinimumHeight() {
  $('.container').css("min-height",($(window).height()+50) + 'px');
}

function logout() {
  document.location.href = rootPath+"authentication/logout.php";
}

function home() {
  document.location.href = rootPath+"index.html";
}

function displayProfile() {
  document.location.href = rootPath+"common/profile.html";
}

function initHeadBar(headline) {
  var tempDiv = document.createElement("div");
  tempDiv.className = "mainHeading";

  var tempHeading = '<div id="headline" style="margin: 0mm 0mm 0mm 2.645mm; display: inline-block;"><b>';
  tempHeading += headline+'</b></div>';

  var headRightSection = document.createElement("div");
  headRightSection.className = "headSection headSectionRight";
  headRightSection.style.cssFloat = "right";
  //$(headRightSection).append("<span id='headDisplayName'>"+loggedInDisplayName+"</span><br />");
  $(headRightSection).append("<a id='headDisplayName' href='#' onclick='displayProfile();'>"+loggedInDisplayName+"</a><br />");
  $(headRightSection).append("<a href='"+rootPath+"authentication/logout.php'>Logout</a>");

  var headLeftSection = document.createElement("div");
  headLeftSection.className = "headSection";
  headLeftSection.style.display = "inline-block";
  headLeftSection.style.cssFloat = "left";
  var tempHomeButton = '<button class="headButton" onclick="home();">QBD</button>';
  $(headLeftSection).append(tempHomeButton);


  $(tempDiv).append(headRightSection);
  $(tempDiv).append(headLeftSection);
  $(tempDiv).append(tempHeading);

  $('body').prepend(tempDiv);
}

function implementDropDown(mainTopic,dropDownContent) {
$(function() {
    $(dropDownContent).hide();
    var timeoutId;
    $(mainTopic).hover(function() {
        if (!timeoutId) {
          timeoutId = window.setTimeout(
            function() {
              timeoutId = null;
              $(dropDownContent).slideDown(120);
            }, 100);
        }
    },
    function () {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timeoutId = null;
        }
        else {
           $(dropDownContent).slideUp(120);
        }
    });
});
}

function createMainTopic(topicVariables) {
  var topicVarArray = topicVariables.split(";");
  var mainTopic = document.createElement("li");
  mainTopic.id = "mainTopic";
  mainTopic.className = "dropdown";

  var mainTopicA = document.createElement("a");
  mainTopicA.innerHTML = topicVarArray[0];
  mainTopicA.className = "dropbtn";
  $(mainTopicA).css("text-decoration","none");
  $(mainTopicA).attr("href",rootPath+topicVarArray[1]);
  $(mainTopic).append(mainTopicA);

  if(topicVarArray.length>2) {
    var dropDownContent = document.createElement("div");
    dropDownContent.className = "dropdown-content";
    for(var loop=2;loop<topicVarArray.length;loop+=2) {
      var subTopic = document.createElement("a");
      subTopic.innerHTML = topicVarArray[loop];
      $(subTopic).css("text-decoration","none");
      $(subTopic).attr("href",rootPath+topicVarArray[loop+1]);
      $(dropDownContent).append(subTopic);
    }
    implementDropDown(mainTopic,dropDownContent);
    $(mainTopic).append(dropDownContent);
  }

  return mainTopic;
}

function initTopicList() {
  var topicList = document.createElement("ul");
  topicList.id = "topicList";

  $(topicList).append(createMainTopic("Class;categories/listcategories.html"));
  $(topicList).append(createMainTopic("Question;questions/submitquestions.html;New;questions/submitquestions.html;List;questions/listquestions.html"));
  $(topicList).append(createMainTopic("Paper;index.html;New;index.html;List;paper/listpapers.html"));
  $(topicList).append(createMainTopic("Contact Us;contactus.html"));

  $('body').prepend(topicList);
}

function initProgressBar() {
  var bar = document.createElement("div");
  bar.id = "bar";

  var progress = document.createElement("div");
  progress.id = "progress";
  bar.appendChild(progress);

  $('body').prepend(bar);
}

function setProgress(progress) {
  var width = ($("#progress").width()*100)/($("#bar").width());
  var id = setInterval(progressing,5);
  function progressing() {
    if(width >= 100) {
      clearInterval(id);
      $("#bar").hide();
      $("#progress").css("width","40%");
    }
    else {
      ++width;
      $("#progress").css("width",width+"%");
    }
  }
}

function initCommon(headline) {
  $("head").append("<title>"+headline+"</title>");
  $("head").append("<link rel='icon' href='"+rootPath+"images/qbd_icon.png' sizes='16x16' type='image/png'>");
  $('.container').css("min-height",($(window).height()+50) + 'px');
  initTopicList();
  initHeadBar(headline);
  initProgressBar();
}

function getParent(element) {
  return element.parentNode;
}

function getGrandParent(element) {
  return element.parentNode.parentNode;
}

function getGreatGrandParent(element) {
  return element.parentNode.parentNode.parentNode;
}
