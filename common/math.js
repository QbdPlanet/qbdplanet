String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function processMathTags(text) {
  var finalText = text;
  finalText = processFractionTag(finalText);
  finalText = processMixedFractionTag(finalText);
  return finalText;
}

function convertToFraction(text) {
  var slashIndex = text.indexOf("//");
  if(slashIndex == -1) { return text; }
  var firstTerm = text.substring(0,slashIndex);
  var secondTerm = text.substring(slashIndex+2);
  /*var processedText = "";
  processedText+='<table class="fraction" align="center" cellpadding="0" cellspacing="0">';
  processedText+='  <tr>';
  processedText+='      <td nowrap="nowrap">';
  processedText+=firstTerm;
  processedText+='      </td>';
  processedText+='  </tr>';
  processedText+='  <tr>';
  processedText+='      <td style="border-top: solid 0.2645mm black; -webkit-print-color-adjust: exact;">';
  processedText+=secondTerm;
  processedText+='      </td>';
  processedText+='  </tr>';
  processedText+='</table>';*/
  var processedText = document.createElement("div");
  var fractionTable = document.createElement("table");
  $(fractionTable).css("text-align","center");
  $(fractionTable).css("vertical-align","middle");
  $(fractionTable).css("margin-top","0mm");
  $(fractionTable).css("margin-bottom","0mm");
  $(fractionTable).css("margin-left","0.7935mm");
  $(fractionTable).css("display","inline");
  $(fractionTable).attr("align","center");
  $(fractionTable).attr("cellpadding","0");
  $(fractionTable).attr("cellspacing","0");

  var firstRow = '<tr><td nowrap="nowrap">' + firstTerm + '</td></tr>';
  var secondRow = '<tr><td style="border-top: solid 0.2645mm black; -webkit-print-color-adjust: exact;">' + secondTerm + '</td></tr>';

  $(fractionTable).append(firstRow);
  $(fractionTable).append(secondRow);
  $(processedText).append(fractionTable);

  return processedText.innerHTML;
}

function processFractionTag(text) {
  var temptext = text.replaceAll("/","//");
  while(true) {
    var tagend = temptext.indexOf("<//fr>");
    if(tagend == -1) { break; }
    if(tagend == 0) { return "error: text shouldn't start with </fr> tag"; }
    var tagstart = temptext.substring(0,tagend-1).lastIndexOf("<fr>");
    if(tagstart == -1) { return "error: no starting <fr> tag for ending </fr> tag"; }
    temptext = temptext.substring(0,tagstart)
               + convertToFraction(temptext.substring(tagstart+4,tagend))
               + temptext.substring(tagend+6);
  }
  if(temptext.indexOf("<fr>") != -1) { return "error: starting <fr> tag with no ending </fr> tag"; }
  return temptext.replaceAll("//","/");
}

function convertToMixedFraction(text) {
  var terms = text.split(",");
  if(terms.length != 3) { return text;}

  var processedText = document.createElement("div");
  var fractionTable = document.createElement("table");
  $(fractionTable).css("text-align","center");
  $(fractionTable).css("vertical-align","middle");
  $(fractionTable).css("margin-top","0mm");
  $(fractionTable).css("margin-bottom","0mm");
  $(fractionTable).css("margin-left","0.7935mm");
  $(fractionTable).css("display","inline");
  $(fractionTable).attr("align","center");
  $(fractionTable).attr("cellpadding","0");
  $(fractionTable).attr("cellspacing","0");

  var firstRow = '<tr><td rowspan=2>' + terms[0] + '</td><td>' + terms[1] + '</td></tr>';
  //var firstRow = '<tr><td rowspan=2>&nbsp;</td><td>' + terms[1] + '</td></tr>';
  var secondRow = '<tr><td style="border-top: solid 0.2645mm black; -webkit-print-color-adjust: exact;">' + terms[2] + '</td></tr>';

  $(fractionTable).append(firstRow);
  $(fractionTable).append(secondRow);
  $(processedText).append(fractionTable);

  return processedText.innerHTML;
}

function processMixedFractionTag(text) {
  var temptext = text.replaceAll("/","//");
  while(true) {
    var tagend = temptext.indexOf("<//mfr>");
    if(tagend == -1) { break; }
    if(tagend == 0) { return "error: text shouldn't start with </mfr> tag"; }
    var tagstart = temptext.substring(0,tagend-1).lastIndexOf("<mfr>");
    if(tagstart == -1) { return "error: no starting <mfr> tag for ending </mfr> tag"; }
    temptext = temptext.substring(0,tagstart)
               + convertToMixedFraction(temptext.substring(tagstart+5,tagend))
               + temptext.substring(tagend+7);
  }
  if(temptext.indexOf("<mfr>") != -1) { return "error: starting <mfr> tag with no ending </mfr> tag"; }
  return temptext.replaceAll("//","/");
}

function processIndentTags(text) {
  var temptext = text.replaceAll("<ind>","<div style='display: inline-block; margin-left: 5.819mm'>").replaceAll("</ind>","</div>");
  return temptext;
}
