<?php

include php("common/config.php");

$con=mysqli_connect($dbsource,$username,$password,$dbname);

if (mysqli_connect_errno($con))
{
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
}

?>
