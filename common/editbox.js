/*$(function() {
  $(".editbox").keydown(function (evt) {
  //$(".formatbox").keydown(function (evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if(charCode == 9) { // recognizing tabs
      //alert("tab recognized");
      var parentNode = window.getSelection().focusNode.parentNode;
      var parentNodeName = window.getSelection().focusNode.parentNode.nodeName;
      //console.log(parentNodeName);
      if(parentNodeName == "OL" || parentNodeName == "UL"  || parentNodeName == "LI") {
        document.execCommand("indent",false,null);
        return false;
      }
      else if(parentNodeName == "TD") {
        if(!varEmptyOrNull(parentNode.nextSibling)) {
          placeCursorAtEnd(parentNode.nextSibling);
        }
        else {
          placeCursorAtEnd(parentNode.parentNode);
        }
        return false;
      }
      return true;
    }
    $(".prettybox").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".matchTheCol *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".optionContainer *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".optionHolder *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    return true;
  });
});*/

function handleTabs(element) {
  $(element).keydown(function (evt) {
    if(!$(element).hasClass("formatbox")) { return true; }
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if(charCode == 9) { // recognizing tabs
      var focusNode = window.getSelection().focusNode;
      while(focusNode) {
        var nodeName = focusNode.nodeName;
        //console.log(nodeName);
        if(nodeName!="#text" && $(focusNode).hasClass("formatbox") ){
          break;
        }
        if(nodeName == "OL" || nodeName == "UL"  || nodeName == "LI") {
          document.execCommand("indent",false,null);
          return false;
        }
        else if(nodeName == "TD") {
          if(!varEmptyOrNull(focusNode.nextSibling)) {
            placeCursorAtEnd(focusNode.nextSibling);
          }
          else {
            placeCursorAtEnd(focusNode.parentNode);
          }
          return false;
        }
        focusNode = focusNode.parentNode;
      }
      return true;
    }
    $(".prettybox").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".matchTheCol *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".optionContainer *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    $(".optionHolder *").each(function() {
      if(this.innerHTML == "<br>") { this.innerHTML = ""; }
    });
    return true;
  });
}

function pasteHtmlAtCaret(html, selectPastedContent) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // only relatively recently standardized and is not supported in
            // some browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            var firstNode = frag.firstChild;
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                if (selectPastedContent) {
                    range.setStartBefore(firstNode);
                } else {
                    range.collapse(true);
                }
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if ( (sel = document.selection) && sel.type != "Control") {
        // IE < 9
        var originalRange = sel.createRange();
        originalRange.collapse(true);
        sel.createRange().pasteHTML(html);
        if (selectPastedContent) {
            range = sel.createRange();
            range.setEndPoint("StartToStart", originalRange);
            range.select();
        }
    }
}

function getCaretPosition(editableDiv) {
  var caretPos = 0,
    sel, range;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.rangeCount) {
      range = sel.getRangeAt(0);
      if (range.commonAncestorContainer.parentNode == editableDiv) {
        caretPos = range.endOffset;
      }
    }
  } else if (document.selection && document.selection.createRange) {
    range = document.selection.createRange();
    if (range.parentElement() == editableDiv) {
      var tempEl = document.createElement("span");
      editableDiv.insertBefore(tempEl, editableDiv.firstChild);
      var tempRange = range.duplicate();
      tempRange.moveToElementText(tempEl);
      tempRange.setEndPoint("EndToEnd", range);
      caretPos = tempRange.text.length;
    }
  }
  return caretPos;
}
