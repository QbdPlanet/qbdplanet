var validForm = true;
var validFormAlert = "";

function initValidForm() {
  validForm = true;
  validFormAlert = "";
}

function appendValidFormAlert(alertStatement) {
  validFormAlert = validFormAlert + alertStatement + "\n";
}

function checkTextDiv(elementId,alertStatement) {
  if(varEmptyOrNull($(elementId).html())) {
    validForm = false;
    appendValidFormAlert(alertStatement);
    $(elementId).css("border-color","red");
  }
}

function checkText(elementId,alertStatement) {
  var element = document.getElementById(elementId);
  if(varEmptyOrNull(element.value)) {
    validForm = false;
    appendValidFormAlert(alertStatement);
    element.style.borderColor = "red";
  }
}

function checkTextArea(elementId,alertElementId,alertStatement) {
  var element = document.getElementById(elementId);
  if(varEmptyOrNull(element.value)) {
    validForm = false;
    appendValidFormAlert(alertStatement + " should be filled.");
    document.getElementById(alertElementId).style.color = "red";
  }
}

function checkSelect(elementId,alertElementId,alertStatement) {
  var element = document.getElementById(elementId);
  if(element.options.length == 0
       || element.selectedIndex == 0)
  {
    validForm = false;
    appendValidFormAlert(alertStatement);
    document.getElementById(alertElementId).style.color = "red";
  }
}

function checkPositiveNumber(elementId,alertElementId,alertStatement) {
  var element = document.getElementById(elementId);
  var alertElement = document.getElementById(alertElementId);
  if(varEmptyOrNull(element.value)) {
    validForm = false;
    appendValidFormAlert(alertStatement + " should be filled.");
    alertElement.style.color = "red";
    return;
  }
  else if( element.value <=0 ) {
    validForm = false;
    appendValidFormAlert(alertStatement + " should be greater than 0.");
    alertElement.style.color = "red";
  }
}

function checkNonPositiveNumber(elementId,alertElementId,alertStatement) {
  var element = document.getElementById(elementId);
  var alertElement = document.getElementById(alertElementId);
  if(varEmptyOrNull(element.value)) {
    validForm = false;
    appendValidFormAlert(alertStatement + " should be filled.");
    alertElement.style.color = "red";
    return;
  }
  else if( element.value > 0 ) {
    validForm = false;
    appendValidFormAlert(alertStatement + " should be less than or equals to 0.");
    alertElement.style.color = "red";
  }
}

function isFormValid() {
  if(!validForm) {
    alert(validFormAlert);
    return false;
  }
  return true;
}

