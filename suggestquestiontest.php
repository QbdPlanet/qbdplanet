<?php

include php("common/utils.php");
include php("common/databaseconnection.php");

//checkPostVariables("subject;topic;level;startPos;limit");

//$subject = $_POST["subject"];
//$topic = $_POST["topic"];
//$level = $_POST["level"];
//$startPos = $_POST["startPos"];
//$limit = $_POST["limit"];

$searchStr = "";
if(isset($_POST["searchStr"])) {
  $searchStr = $_POST["searchStr"];
}

$userId = $_SESSION["userId"];

$subject = "English";
$topic = "ALL";
$level = "ALL";
$startPos = 0;
$limit = 10;

$defaultValue = "ALL";

/*$query = "DELIMITER $$
CREATE FUNCTION `levenshtein`( s1 text, s2 text) RETURNS int(11)
    DETERMINISTIC
BEGIN 
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT; 
    DECLARE s1_char CHAR; 
    DECLARE cv0, cv1 text; 
    SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0; 
    IF s1 = s2 THEN 
      RETURN 0; 
    ELSEIF s1_len = 0 THEN 
      RETURN s2_len; 
    ELSEIF s2_len = 0 THEN 
      RETURN s1_len; 
    ELSE 
      WHILE j <= s2_len DO 
        SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1; 
      END WHILE; 
      WHILE i <= s1_len DO 
        SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1; 
        WHILE j <= s2_len DO 
          SET c = c + 1; 
          IF s1_char = SUBSTRING(s2, j, 1) THEN  
            SET cost = 0; ELSE SET cost = 1; 
          END IF; 
          SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost; 
          IF c > c_temp THEN SET c = c_temp; END IF; 
            SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1; 
            IF c > c_temp THEN  
              SET c = c_temp;  
            END IF; 
            SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1; 
        END WHILE; 
        SET cv1 = cv0, i = i + 1; 
      END WHILE; 
    END IF; 
    RETURN c; 
  END $$
CREATE FUNCTION `levenshtein_ratio`( s1 text, s2 text ) RETURNS int(11)
    DETERMINISTIC
BEGIN 
    DECLARE s1_len, s2_len, max_len INT; 
    SET s1_len = LENGTH(s1), s2_len = LENGTH(s2); 
    IF s1_len > s2_len THEN  
      SET max_len = s1_len;  
    ELSE  
      SET max_len = s2_len;  
    END IF; 
    RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100); 
  END $$
DELIMITER ;";*/

$query = "SELECT Question.QuestionId,Question.Question,Question.ImageUrl," 
                   . "SubjectTopic.Subject,SubjectTopic.Topic,DifficultyLevel.DifficultyLevel" 
                   . " FROM Question JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
                   . " JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId"
                   . " WHERE Question.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (SubjectTopic.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (SubjectTopic.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (DifficultyLevel.DifficultyLevel='$level') )"
                   . " AND Question.Question LIKE '%$searchStr%'"
                   . " ORDER BY Question.QuestionId"
                   . " LIMIT $startPos, $limit";
$result = mysqli_query($con,$query);

$response = array();
$questionResponse = array();
/*$response["questionId"] = array();
$response["question"] = array();
$response["imageUrl"] = array();*/

while($row = mysqli_fetch_array($result) ) {
  $question = array();
  $question["questionId"] = $row[0];
  $question["question"] = $row[1];
  $question["imageUrl"] = $row[2];
  $question["subject"] = $row[3];
  $question["topic"] = $row[4];
  $question["level"] = $row[5];
  array_push($questionResponse,$question);
}
$response["question"] = $questionResponse;

$query = "SELECT COUNT(Question.QuestionId) AS TotalQuestions"
                   . " FROM Question JOIN SubjectTopic ON Question.SubjectTopicId=SubjectTopic.SubjectTopicId"
                   . " JOIN DifficultyLevel ON Question.DifficultyLevelId=DifficultyLevel.DifficultyLevelId"
                   . " WHERE Question.UserId='$userId'"
                   . " AND ( ('$subject' = '$defaultValue') OR (SubjectTopic.Subject='$subject') )"
                   . " AND ( ('$topic' = '$defaultValue') OR (SubjectTopic.Topic='$topic') )"
                   . " AND ( ('$level' = '$defaultValue') OR (DifficultyLevel.DifficultyLevel='$level') )"
                   . " AND Question.Question LIKE '%$searchStr%'";
$result = mysqli_query($con,$query);

if($row = mysqli_fetch_array($result) ) {
  $response["totalQuestions"] = $row['TotalQuestions'];
}

$response["nextIndex"] = $startPos+$limit;
$response["prevIndex"] = $startPos-$limit;

echo json_encode($response);

mysqli_close($con);

exit();

?>
