<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

checkPostVariables("folderId");

$folderId = $_POST["folderId"];

$userId = $_SESSION["userId"];

$response = array();

$query = "SELECT QuestionId FROM Question WHERE FolderId='$folderId' AND UserId='$userId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Question/s exist in this stream.";
  echo json_encode($response);
  exit();
}

$query = "SELECT PaperId FROM Paper WHERE FolderId='$folderId' AND UserId='$userId' LIMIT 1";
$result=mysqli_query($con,$query);

if($row = mysqli_fetch_array($result)) {
  $response["result"] = "failure";
  $response["message"] = "Unable to Delete. Paper/s exist in this stream.";
  echo json_encode($response);
  exit();
}

$query = "SELECT f1.FolderId,f2.FolderId,f1.FolderName,f2.FolderName,f3.FolderName"
          . " FROM Folder f1 JOIN Folder f2 ON f1.FolderId=f2.ParentId"
          . " JOIN Folder f3 ON f2.FolderId=f3.ParentId"
          . " WHERE f1.ParentId=0 AND f3.FolderId='$folderId' AND f3.UserId='$userId'";
$result = mysqli_query($con,$query);

$topicId = $folderId;
$topic = "";
$subjectId = "";
$subject = "";
$levelId = "";
$level = "";

if($row = mysqli_fetch_array($result)) {
  $levelId = $row[0];
  $subjectId = $row[1];
  $level = $row[2];
  $subject = $row[3];
  $topic = $row[4];
}
else {
  $response["result"] = "failure";
  $response["message"] = "Data Corrupt! Contact Admin";
  echo json_encode($response);
  exit();
}

$query = "DELETE FROM Folder WHERE FolderId='$topicId'";
$result = mysqli_query($con,$query);

$query = "SELECT f3.FolderId FROM Folder f2 JOIN Folder f3 ON f2.FolderId=f3.ParentId WHERE f2.FolderId='$subjectId' LIMIT 1";
$result = mysqli_query($con,$query);

if(!($row = mysqli_fetch_array($result))) {
  $query = "DELETE FROM Folder WHERE FolderId='$subjectId'";
  $result = mysqli_query($con,$query);
}

$query = "SELECT f2.FolderId FROM Folder f1 JOIN Folder f2 ON f1.FolderId=f2.ParentId WHERE f1.FolderId='$levelId' LIMIT 1";
$result = mysqli_query($con,$query);

if(!($row = mysqli_fetch_array($result))) {
  $query = "DELETE FROM Folder WHERE FolderId='$levelId'";
  $result = mysqli_query($con,$query);
}

$response["result"] = "success";
$response["message"] = "'$level' -> '$subject' -> '$topic', stream deleted successfully";
$response["folderId"] = $folderId;

echo json_encode($response);

mysqli_close($con);

exit();

?>
