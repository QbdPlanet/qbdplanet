<?php

include php("common/databaseconnection.php");
include php("common/utils.php");

$response = array();

$userId = $_SESSION["userId"];

$query = "SELECT f1.FolderName,f2.FolderName,f3.FolderName,f1.FolderId,f2.FolderId,f3.FolderId"
          . " FROM Folder f1 JOIN Folder f2 ON f1.FolderId=f2.ParentId"
          . " JOIN Folder f3 ON f2.FolderId=f3.ParentId"
          . " WHERE f1.UserId='$userId' AND f1.ParentId=0"
          . " ORDER BY f1.FolderId, f2.FolderId, f3.FolderId";
$result = mysqli_query($con,$query);

$lastLevelName="";
while($row = mysqli_fetch_array($result)) {
  $temp = array();
  $temp["level"] = $row[0];
  $temp["subject"] = $row[1];
  $temp["topic"] = $row[2];
  $temp["levelId"] = $row[3];
  $temp["subjectId"] = $row[4];
  $temp["topicId"] = $row[5];
  array_push($response,$temp);
}

echo json_encode($response);

mysqli_close($con);

exit();

?>
